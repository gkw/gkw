#!/bin/bash
## This script auto generates GKW html documentation from in code comments using doxygen.
## and uploads to gkw webspace.  
# Runs as Sunday night cron job (on fileserver machine) as user btpp301007.
# Example line for crontab:
# 30 23 * * 0 /home/btpp/bt301007/Programme/gkw/scripts/bayreuth/gkw_doc_update > /dev/null
#
# \note Installing a new version of the OS might delete the crontab and thus
#       making a reseting of the cronjob necessary.

## F.J.Casson 2010

## Webspace location: http://www.gkw.org.uk/doxygen
## For more info on doxygen See http://www.stack.nl/~dimitri/doxygen/
## See http://code.google.com/p/gkw/wiki/Doxygen for GKW comment format.

# PATH TO doxygen binary, works with version 1.6+ of doxygen
DOXYBIN="/usr/bin/doxygen"

# Location already containing a clean working copy of GKW.
TMP_LOC=/home/btpp/bt301007/Programme/gkw-read-only

# Update GKW
cd $TMP_LOC
git pull > /dev/null

# Modify the Doxyfile
export GKW_VERSION=`git describe --always --tags`
cd $TMP_LOC/doc
mv Doxyfile Doxyfile_orig
sed s/REVISION_NUMBER/${GKW_VERSION}-B/ Doxyfile_orig >Doxyfile

# Remove any old documentation files.
rm -rf html latex doxygen*

# Build the new documentation
$DOXYBIN > doxygen.log 2>&1

# Cleanup
mv Doxyfile_orig Doxyfile

# Send GKW doxygen to web host.  Use a public key already uploaded.
# Asssumes wilcards are allowed for put in sftp client used.
# Use option -r for 'recursive', as there is the subfolder 'search'.
sftp -q -r -o "IdentityFile=~/.ssh/id_rsa_npp" -b /dev/fd/0 gkworgu1@gkw.org.uk <<EOF
put $TMP_LOC/doc/html/* ./public_html/doxygen
put $TMP_LOC/doc/html/search/* ./public_html/doxygen/search
exit
EOF
