## --- MPCDF Viper ---

OPTFLAGS = on
DEBUG = off
PERF = perf

REQUIREDMODULES = intel/2024.0 impi/2021.11 fftw-mpi/3.3.10 hdf5-mpi/1.14.1

# Uncomment if you need the HDF5 format
IO_LIB = HAVE_HDF5
LDFLAGS_H5 = -L/$(HDF5_HOME)/lib -lhdf5hl_fortran -lhdf5_hl -lhdf5_fortran -lhdf5 -L/$(HDF5_HOME)/lib -lz -ldl -lm -Wl,-rpath=$(HDF5_HOME)/lib
FFLAGS_INC_H5 = -I/$(HDF5_HOME)/include

# Inlcude FFTW3 library
FFTLIB = FFT_FFTW3
FFLAGS_INC_FFT= -I/$(FFTW_HOME)/include 
LDFLAGS_FFT=-L/$(FFTW_HOME)/lib -lfftw3 -lfftw3f -lm -Wl,-rpath=$(FFTW_HOME)/lib

# Compiler
CC = cc
FC = mpiifort
LD = $(FC)

# Mpi
MPI = mpi2

IMPLICIT=umfpack

FFLAGS_DOUBLE = -r8

# the latter flag is suggested by the Viper user guide
FFLAGS_OPT    = -O3 -no-prec-div -march=skylake-avx512
FFLAGS_DEBUG  = -g -O0 -traceback -ftrapuv -check pointers,stack,uninit,bounds -fpe0
FFLAGS_OTHER  = -qopt-report=3 -warn all

FFLAGS_INC = -I. \
             $(FFLAGS_INC_SLEPC) $(FFLAGS_INC_H5) $(FFLAGS_INC_FFT) $(FFLAGS_INC_MKL) $(FFLAGS_INC_LIBRSB)
LDFLAGS    = $(LDFLAGS_SLEPC) $(LDFLAGS_FFT) $(LDFLAGS_H5) $(LDFLAGS_MKL) $(LDFLAGS_LIBRSB) \
             -L../../libs/UMFPACK/Lib/ -lumfpack_gkw64_NB -L../../libs/AMD/Lib/ -lamd


