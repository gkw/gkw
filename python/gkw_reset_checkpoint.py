#!/usr/bin/env python3

# ----------------------------------------------------------------------
# This module can be used to reset a simulation performed with GKW to a
# given checkpoint dump file (DM[1/2]).
# It requires io_format = 'hdf5' or 'ascii+hdf5', i.~e., it requires an
# hdf5-file 'gkwdata.h5' that contains the control namelist.
import h5py
import os
import pandas as pd
import numpy as np
from shutil import copyfile
import fileinput
import sys

description_text = """

====================== DESCRIPTION =====================================

FEATURES:

 - This script resets a simulation performed with GKW to the most recent 
   checkpoint DM[1/2].
 - It has to be called within the simulation directory.
 - WARNING: If the simulation has been restarted from another simulation 
            (using the FDS-file of another simulation), then this script
            might fail unless the parameters NT_COMPLETE and FILE_COUNT 
            in FDS.dat have been set to zero in the beginning!!!
======================= ARGUMENTS ======================================
"""

# ----------------------------------------------------------------------
# Functions

# Function that delets all data in interval [nt_reset:nt_broke].
# ncol considers, if data series is ordered by a multiple interger of
# ntime.
def reset_time_trace(indata, dim, ncol, nt_reset):
    
  # Shift dimension dim to 0.
  data_shifted = np.moveaxis(indata, dim, 0)
  
  # Reset data.
  data_shifted_trimmed = data_shifted[0:int(nt_reset*ncol),]
    
  # Shift dimension back.
  out = np.moveaxis(data_shifted_trimmed, 0, dim)
  
  # free memory
  del data_shifted
  del data_shifted_trimmed
  
  return out
  
  
# Checks if file has binary format.
def is_binary(filename):
  try:
    with open(filename, 'tr') as check_file:
      check_file.read()
      return False
  except:
    return True
  
  
  
# Check for specific files that are no ordinary data files.
def is_file_exception(filename):
  
  # substrings that have to be checked
  check_list = ['geom.dat', 'DM1.dat', 'DM2.dat', 'FDS.dat', '.o', 'FDS', 
                'input.dat', 'perform_first.dat', 'perform.dat', 'output.dat', 
                'gkwdata.meta', 'gkw_hdf5_errors.txt', 'kx_connect.dat', 
                'jobscript', 'Poincare1.mat', 'perfloop_first.dat', 'par.dat', 
                'input_init.dat', 'sgrid', 'gkw', '.out', 'status.txt',
                'D_theta_theta', 'Coll_params.dat', 'CoulombLog', 'D_nu_nu',
                'CollFreqs', 'job.env']
  for key in check_list:
    if key in filename:
      return True
  
  return False
  
  
# Check if given file is a PBS or SLURM jobscript.
def is_jobscript(filename):
  with open(filename,'r') as file:                                                                                                                                                                                                                                             
    for line in file:
      if '#PBS -l' in line:
        return True
      if '#SBATCH' in line:
        return True
        
  return False



# ----------------------------------------------------------------------
# Check for most recent checkpoint dump file and determine various
# reset parameters
# ----------------------------------------------------------------------
def reset_simulation(SIM_DIR, NTIME=None, verbose=True, use_ntime=False):
  
  print(use_ntime)
        
  # ----------------------------------------------------------------------
  # Generic variables
  
  HDF5_FILENAME = "gkwdata.h5"
  DUMPFILE1 = "DM1"
  DUMPFILE2 = "DM2"
  RESTARTFILE = "FDS"
  DM1_EXISTS = False
  DM2_EXISTS = False
  FDS_EXISTS = False
  NT_REMAIN1 = None
  NT_REMAIN2 = None
  NT_REMAIN  = None
  NT_COMPLETE1 = None
  NT_COMPLETE2 = None
  NT_COMPLETE = None
  
  # Change to simulation directory.
  if(not os.path.isdir(SIM_DIR)):
    print('**** Abort **** Given directory does not exist!')
    sys.exit()
  else:
    os.chdir(SIM_DIR)
  
  if verbose:
    print('\n -------- Start reset to last checkpoint of simulation -------- \n')
    print('\t'+SIM_DIR+'\n')
  
  # Check if hdf5-file exists.
  if(not os.path.isfile(HDF5_FILENAME)):
    print('**** Abort **** HDF5 file does not exist! \n')
    sys.exit()
  
  # First, read hdf5 file and determine the number of big time steps NTIME, 
  # requested in the input.dat file.
  f = h5py.File(HDF5_FILENAME, "r+")
    
  
  # Get requested big time steps from the /control group in the hdf5-file.
  if(NTIME==None):
      NTIME = int(f['input/control/ntime'][:])
  
  # Get number of big time steps after which simulation broke.
  # If time.dat exists read this file to obtain number of time steps after
  # which simulation broke.
  if(os.path.isfile('time.dat')):
    tim = pd.read_csv(SIM_DIR+'/'+'time.dat', header=None, sep='\s+').values
    NT_BROKE = tim.shape[0]
  # Else, get time from hdf5-file.
  else:
    NT_BROKE = f['diagnostic/diagnos_growth_freq/time'].shape[1]
  
  
  # Set NT_BROKE for output files holding temporal derivates and therefore 
  # one timestep less.
  NT_BROKE_DERIV = NT_BROKE-1
  
  # Close the hdf5-file again.
  f.close()
    
  # Get the number of remaining big time steps NT_REMAIN from checkpoint 
  # files FDS.dat. This is used lateron to determine the most recent 
  # checkpoint file.
  if(os.path.isfile(SIM_DIR+'/'+RESTARTFILE+'.dat')):
    FDS_EXISTS = True
    with open(SIM_DIR+'/'+RESTARTFILE+'.dat','r') as file:                                                                                                                                                                                                                                             
      for line in file:
        if 'NT_REMAIN' in line:
          expr = line.replace(' ','')
          expr = expr.replace(',','')
          expr = expr.replace('\n','')
          NT_REMAIN = int(expr.split('=')[1])
        if 'NT_COMPLETE' in line:
          expr = line.replace(' ','')
          expr = expr.replace(',','')
          expr = expr.replace('\n','')
          NT_COMPLETE = int(expr.split('=')[1])
    
  # Get the number of remaining big time steps NT_REMAIN[1/2] from checkpoint 
  # files DM[1/2]. This is used lateron to determine the most recent 
  # checkpoint file.
  if(os.path.isfile(SIM_DIR+'/'+DUMPFILE1+'.dat')):
    DM1_EXISTS = True
    with open(SIM_DIR+'/'+DUMPFILE1+'.dat','r') as file:                                                                                                                                                                                                                                             
      for line in file:
        if 'NT_REMAIN' in line:
          expr = line.replace(' ','')
          expr = expr.replace(',','')
          expr = expr.replace('\n','')
          NT_REMAIN1 = int(expr.split('=')[1])
        if 'NT_COMPLETE' in line:
          expr = line.replace(' ','')
          expr = expr.replace(',','')
          expr = expr.replace('\n','')
          NT_COMPLETE1 = int(expr.split('=')[1])
  
  if(os.path.isfile(SIM_DIR+'/'+DUMPFILE2+'.dat')):
    DM2_EXISTS = True
    with open(SIM_DIR+'/'+DUMPFILE2+'.dat','r') as file:                                                                                                                                                                                                                                             
      for line in file:
        if 'NT_REMAIN' in line:
          expr = line.replace(' ','')
          expr = expr.replace(',','')
          expr = expr.replace('\n','')
          NT_REMAIN2 = int(expr.split('=')[1])
        if 'NT_COMPLETE' in line:
          expr = line.replace(' ','')
          expr = expr.replace(',','')
          expr = expr.replace('\n','')
          NT_COMPLETE2 = int(expr.split('=')[1])
          
          
  # Check if FDS is the most recent checkpoint file. In this case
  # resetting the simulation makes no sense.
  if(FDS_EXISTS):
    DM1_OLD = False
    DM2_OLD = False
    if(DM1_EXISTS):
      if(NT_COMPLETE1 < NT_COMPLETE):
        DM1_OLD = True
    if(DM2_EXISTS):
      if(NT_COMPLETE2 < NT_COMPLETE):
        DM2_OLD = True
    if(DM1_OLD and DM2_OLD):
      print('**** Abort **** FDS is most recent checkpoint file. Reset not necessary!')
      sys.exit()
          
          
  # Now determine which checkpoint file is the recent one.
  if(DM1_EXISTS and DM2_EXISTS):
    
    if(NT_REMAIN1 > NT_REMAIN2):
      NT_REMAIN = NT_REMAIN2
      NT_COMPLETE = NT_COMPLETE2
      DUMPFILE = DUMPFILE2
    else:
      NT_REMAIN = NT_REMAIN1
      NT_COMPLETE = NT_COMPLETE1
      DUMPFILE = DUMPFILE1
  
  elif(DM1_EXISTS and not DM2_EXISTS):
    
    NT_REMAIN = NT_REMAIN1
    NT_COMPLETE = NT_COMPLETE1
    DUMPFILE = DUMPFILE1
    
  elif(DM2_EXISTS and not DM1_EXISTS):
  
    NT_REMAIN = NT_REMAIN2
    NT_COMPLETE = NT_COMPLETE2
    DUMPFILE = DUMPFILE2
    
  else:
    print('**** Abort **** Cannot find any checkpoint dump file!')
    sys.exit()
    
    
  if(not use_ntime):
    # Find the total number ob big time steps the simulation time trace should have, 
    # when considering the big time steps already completed as well as the big time 
    # steps that remain. Can be different from NTIME, since the simulation could 
    # have been restarted several times such that NTIME > NT_COMPLETE.
    NTOT = NT_COMPLETE + NT_REMAIN
    N_REQUEST = NTOT
    
    # Determine the time steps to which the time trace files have to be reset.
    # Use NTOT here, since NTIME could have been changed at some point, or NT_COMPLETE
    # could be larger than NTIME.
    NT_RESET = NTOT-NT_REMAIN
  else:
    # Determine the time steps to which the time trace files have to be reset.
    NT_RESET = NTIME-NT_REMAIN
    N_REQUEST = NTIME
    
  # Same for files holding time derivatives.
  NT_RESET_DERIV = NT_RESET-1
  
  # Write information to screen.
  if verbose:
    print('\n -------- Checkpoint reset report -------- \n')
    print('  Requested number of big time steps: \t'+str(N_REQUEST))
    print('  Number of time steps after simulation broke: \t'+str(NT_BROKE))
    print('  Most recent checkpoint dump file: \t' + DUMPFILE)
    print('  Remaining time steps after last checkpoint file: \t'+str(NT_REMAIN))
    print('  Number of big time steps the simulation is reset to: \t'+str(NT_RESET))
    print('\n -------- End of Checkpoint reset report -------- \n')
  
  
  # ----------------------------------------------------------------------
  # Cycle over all nodes of hdf5-file and reset time trace datasets.
  
  # Check if hdf5-file exists.
  if(os.path.isfile(HDF5_FILENAME)):
      
    if verbose:
        print('\n -------- Reset hdf5-datasets -------- \n')
    
    # Find all possible keys items, i.e. both groups and datasets
    f = h5py.File(HDF5_FILENAME, "a")
    h5_keys = []
    f.visit(h5_keys.append)
  
    # Cycle over all keys items and check, if any dimension has size NT_BROKE, 
    # i.e., it is a time trace file.
    for n, item in enumerate(h5_keys):
      
      data = f.get(item)
          
      # Consider datasets only.
      if(isinstance(data, h5py.Dataset)):
        
        #Check if any dimension is an integer multiple of NT_BROKE, by checking
        # the residual of the division.
        res = [None]*len(data.shape)
        for i in range(len(data.shape)):
          res[i] = data.shape[i]/NT_BROKE-np.floor(data.shape[i]/NT_BROKE)
          
        if 0.0 in res:
        
          # Check which dimension is integer multiple of NT_BROKE
          # and save dimension as well as integer.
          new_shape = data.shape
          for i in range(len(data.shape)):
            ncol = data.shape[i]/NT_BROKE
            res = ncol - np.floor(ncol)
            if(res == 0):
              dim = i
              ncol = int(ncol)
              # adjust new shape to ncol*NT_RESET
              y = list(new_shape)
              y[dim] = int(ncol*NT_RESET)
              new_shape = tuple(y)
              break
              
          # Print name of dataset that is to be repaired to screen.
          if verbose:
            print('\n Reset hdf5-dataset: '+item)
          
          # Reset dataset (.resize discards data with indices larger than 
          # ncol*NT_RESET along dimension dim).
          dset = f[item]
          dset.resize(int(ncol*NT_RESET),dim)
  
  
    # After having repaired all datasets, close the hdf5-file again.
    f.close()
    
    
  # ----------------------------------------------------------------------
  # Cycle over all csv-files and reset time trace.
  if verbose:
    print('\n\n -------- Reset csv-files -------- \n')
  
  for filename in os.listdir(SIM_DIR):
      
    # First perform some checks on files; cycle if file is binary, an exception
    # or a jobscript.
    if(is_binary(filename)):
      continue
    
    if(is_file_exception(filename)):
      continue
      
    if(is_jobscript(filename)):
      continue

    # check for empty file
    try:
      pd.read_csv(SIM_DIR+'/'+filename)
    except pd.errors.EmptyDataError:
      continue
    
    # no file
    if(not os.path.isfile(filename)):
      continue
    
    # Load csv file.
    data =  pd.read_csv(SIM_DIR+'/'+filename, header=None, sep='\s+').values
    
    #Check if any dimension is an integer multiple of NT_BROKE, by checking
    #the residual of the division.
    res = [None]*len(data.shape)
    
    # residul for output that holds time derivatives and therefore nt-1 datapoints
    res_deriv = [None]*len(data.shape)
    for i in range(len(data.shape)):
      res[i] = data.shape[i]/NT_BROKE-np.floor(data.shape[i]/NT_BROKE)
      res_deriv[i] = data.shape[i]/(NT_BROKE_DERIV)-np.floor(data.shape[i]/(NT_BROKE_DERIV))
      
    # ordinary files
    if 0.0 in res:

      #Check which dimension is integer multiple of NT_BROKE
      #and save dimension as well as integer.
      for i in range(len(data.shape)):
        ncol = data.shape[i]/NT_BROKE
        res = ncol - np.floor(ncol)
        if(res == 0):
          dim = i
          ncol = int(ncol)
          break
          
      # Print filename that is to be repaired to screen.
      if verbose:
        print('\n Reset csv-file: '+filename)
              
      # Load original dataset.
      original_data = data
      # print('\t Original shape: \t'+str(original_data.shape))
      
      # Reset time trace.
      reset_data = reset_time_trace(original_data,dim,ncol,NT_RESET)
      # print('\t Reset shape: \t' +str(reset_data.shape))
    
      # Save resetted data.
      pd.DataFrame(reset_data).to_csv(filename, sep='\t', header=None, index=None)
      
      
    # files holding time derivatives
    if 0.0 in res_deriv:
    
      # Check which dimension is integer multiple of NT_BROKE
      # and save dimension as well as integer.
      for i in range(len(data.shape)):
        ncol = data.shape[i]/NT_BROKE_DERIV
        res = ncol - np.floor(ncol)
        if(res == 0):
          dim = i
          ncol = int(ncol)
          break
          
      # Print filename that is to be repaired to screen.     
      if verbose:
        print('\n Reset csv-file: '+filename)
              
      # Load original dataset.
      original_data = data
      
      # Reset time trace.
      reset_data = reset_time_trace(original_data,dim,ncol,NT_RESET_DERIV)
    
      # Save resetted data.
      pd.DataFrame(reset_data).to_csv(filename, sep='\t', header=None, index=None)
      
  
  # ----------------------------------------------------------------------
  # Finally, copy most recent dump file to FDS[/.dat].
  
  if verbose:
    print('\n -------- Copy most recent dump file to FDS[/.dat]-------- \n')
  
  # Copy the most recent dump file to FDS[/.dat].
  copyfile(SIM_DIR+'/'+DUMPFILE, SIM_DIR+'/'+'FDS')
  copyfile(SIM_DIR+'/'+DUMPFILE+'.dat', SIM_DIR+'/'+'FDS.dat')
  
  # First line of the so produced FDS.dat has to be modified. 
  old_text = '!Dump filename: '+DUMPFILE
  new_text = '!Dump filename: '+'FDS'
  
  # Replace first line in FDS.dat to set the correct file name.
  with fileinput.input(SIM_DIR+'/'+'FDS.dat',inplace=True) as f:
    for line in f:
      newline = line.replace(old_text, new_text)
      print(newline, end='')
      
  # Write information to screen.
  if verbose:
    print('\n  Most recent dump file: \t'+DUMPFILE+'[/.dat]')
    print('  is copied to: \t' + 'FDS[/.dat]')
          
    print('\n -------- Finished copying most recent dump file to FDS[/.dat] -------- \n')



if __name__ == '__main__':
  
  from argparse import ArgumentParser
  
  parser = ArgumentParser()
  
  # The simulation directory.
  parser.add_argument("-sd", "--sim_dir", dest="sim_dir", default=os.getcwd())
  
  # The number of big time steps, if it has to be set manually.
  parser.add_argument("-nt", "--ntime", dest="ntime", default=None)
  
  # The number of big time steps, if it has to be set manually.
  parser.add_argument("--verbose", dest="verbose", default=True, action="store_true")
  parser.add_argument("--no-verbose", dest="verbose", action="store_false")
  
  # Use ntime to determine nreset
  parser.add_argument("--use-ntime", dest="use_ntime", default=False, action="store_true")
  
  # Parse arguments to namescape args.
  args = parser.parse_args()
    
  # reset the simulation
  reset_simulation(args.sim_dir, NTIME=args.ntime, verbose=args.verbose, use_ntime=args.use_ntime)
