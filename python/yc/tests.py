# Various tests to check the behaviour of the GKW python tools
import os
os.chdir('/home/yann/codes/gkw/python/yc')
import gkwrun
os.chdir('/home/yann/codes/gkw/python/yc')
import gkwimas
import idspy_toolkit as idspy
from idspy_dictionaries import ids_gyrokinetics_local
import matplotlib.pyplot as plt
import numpy as np

########## Test GKW->IMAS->GKW conversion (with reference GKW input file provided)
gkw_home='/home/yann/runs1/gkw/'

# generate reference IDS -> tt.ids
proj='WEST_1'
flnm='55797b_kth02_2_2'   # linear run, fourier, kykxs, phi+apar+bpar
tt=gkwrun.GKWrun()
tt.set_storage(gkw_home+proj,'multiple_folders',flnm)
tt.settings['verbose']=False
tt.read()
tt.ids=gkwimas.gkw2ids(tt,[],21)


# generate reference GKW input file -> ref.input
proj_ref='GKDB_TEST'
flnm_ref='ref_fourier'     # linear run, fourier, phi only, no kykxs diagnostics
ref=gkwrun.GKWrun()
ref.set_storage(gkw_home+proj,'multiple_folders',flnm)
ref.get_inputs()

# convert tt.ids to a GKW input and write files (input.dat and input.out)
dum=gkwimas.ids2gkw(tt.ids,'linear',params_source='gkwref',gkwref=ref)
dum.write_input('/home/yann/tmp/input.dat')
dum.write_input('/home/yann/tmp/input.out')

# update the input part of tt.ids using the newly written GKW input files
tt.set_storage('/home/yann/tmp/','single_folder')
tt.get_inputs()

# convert again to an IDS and write again the input files
tt.ids=gkwimas.gkw2ids(tt,[],21)
dum=gkwimas.ids2gkw(tt.ids,'linear',params_source='gkwref',gkwref=ref)
dum.write_input('/home/yann/tmp/input2.dat')

# diff input.dat input2.dat
# 29.02.2024 -> ok! input.dat and input2.dat only have small differences in non significant digits


########## Test GKW->IMAS->GKW conversion (build GKW input from the parameters field in the IDS)

gkw_home='/home/yann/runs1/gkw/'

# generate  IDS from GKW run
proj='WEST_1'
flnm='55797b_kth02_2_2'   # linear run, fourier, kykxs, phi+apar+bpar
tt=gkwrun.GKWrun()
tt.set_storage(gkw_home+proj,'multiple_folders',flnm)
tt.settings['verbose']=False
tt.read()
tt.ids=gkwimas.gkw2ids(tt,[],21)

# convert tt.ids to a GKW input and write files (input.dat and input.out)
dum=gkwimas.ids2gkw(tt.ids,'linear',params_source='ids')
dum.write_input('/home/yann/tmp/input.dat')
dum.write_input('/home/yann/tmp/input.out')

# update the input part of tt.ids using the newly written GKW input files
tt.set_storage('/home/yann/tmp/','single_folder')
tt.get_inputs()

# convert again to an IDS and write again the input files
tt.ids=gkwimas.gkw2ids(tt,[],21)

# write to the IDS to file and read it again
if 0: # temporary fix until the bug is corrected in the IMAS DD
  tt.ids.linear.wavevector[0].eigenmode[0].linear_weights.momentum_tor_parallel_a_field_parallel=0.0
  tt.ids.linear.wavevector[0].eigenmode[0].linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel=0.0
g,k=idspy.ids_to_hdf5(tt.ids,'/home/yann/tmp/gkw.hdf5',overwrite=True) 
newids=ids_gyrokinetics_local.GyrokineticsLocal()
idspy.hdf5_to_ids('/home/yann/tmp/gkw.hdf5',newids,todict=False,fill=True)
tt.ids=newids

dum=gkwimas.ids2gkw(tt.ids,'linear',params_source='ids')
dum.write_input('/home/yann/tmp/input2.dat')

# diff input.dat input2.dat
# 29.02.2024 -> ok, input.dat and input2.dat only have small differences in non significant digits



########### Test read/write hdf5 files with idspy_toolkit for the GK IDS ###########
# ok - 29.02.2024

os.chdir('/home/yann/projects/imasgk_git/gkids/tools')
import gkids

gkw_home='/home/yann/runs1/gkw/'

# generate  IDS from GKW run
proj='WEST_1'
flnm='55797b_kth02_2_2'   # linear run, fourier, kykxs, phi+apar+bpar
tt=gkwrun.GKWrun()
tt.set_storage(gkw_home+proj,'multiple_folders',flnm)
tt.read()
tt.ids=gkwimas.gkw2ids(tt,[],21)

if 0: # temporary fix until the bug is corrected in the IMAS DD
  tt.ids.linear.wavevector[0].eigenmode[0].linear_weights.momentum_tor_parallel_a_field_parallel=0.0
  tt.ids.linear.wavevector[0].eigenmode[0].linear_weights_rotating_frame.momentum_tor_parallel_a_field_parallel=0.0


g,k=idspy.ids_to_hdf5(tt.ids,'/home/yann/tmp/gkw.hdf5',overwrite=True) 

newids=ids_gyrokinetics_local.GyrokineticsLocal()
idspy.hdf5_to_ids('/home/yann/tmp/gkw.hdf5',newids,todict=True,fill=True)

# compare the two ids
check,log=gkids.ids_diff(tt.ids,newids)

########## Compare fluxes computed from moments to fluxes computed in GKW ########
# 21.03.2024 -> ok 

# NL run
gkw_home='/home/yann/runs1/gkw/'
proj='TCV_RAMPUP_1'
flnm='nl64965r05t1_nom'
tint=[150,152]

gkw_home='/home/yann/runs3/gkw/'
proj='TCV_RAMPUP_2'
flnm='nl64965r09t1_nom'
tint=[30,31]


NLrun=gkwrun.GKWrun()
NLrun.set_storage(gkw_home+proj,'multiple_folders',flnm)
NLrun.read(kykxs=False)
NLrun.get_kykxs(time_interval=tint,prec='sp')
NLrun.get_kykxs_spectra()
NLrun.get_transport_fluxes()

# check
t=NLrun.grids['time'][NLrun.kykxs['It']]
for field in ('phi','apar','bpar'):
  for flux in ('particles','heat','momentum_parallel'):
    f1=np.trapz(NLrun.fluxes[field][flux][NLrun.kykxs['It'],:],t,axis=0)/(t[-1]-t[0])
    plt.plot(t,NLrun.fluxes[field][flux][NLrun.kykxs['It'],:])
    if flux in NLrun.kykxs_spectra[field].keys():
      f2=np.sum(NLrun.kykxs_spectra[field][flux]['ky'],axis=0)
      plt.plot(t,np.sum(NLrun.kykxs_spectra[field][flux]['kyt'],axis=0),'--')
    else:
      f2=np.nan 
    print(field+", "+flux+": "+str(f1/f2))
    plt.show()


# linear run
gkw_home='/home/yann/runs1/gkw/'
proj='WEST_1'
flnm='55797b_kth02_2_2'   # linear run, fourier, kykxs, phi+apar+bpar
#proj='TEST'
#flnm='test_j1_moments'

tt=gkwrun.GKWrun()
tt.set_storage(gkw_home+proj,'multiple_folders',flnm)
tt.read()

for field in ('phi','apar','bpar'):
  for flux in ('particles','heat','momentum_parallel'):
     print(field+", "+flux+": "+str(tt.fluxes[field][flux][-1]/np.sum(tt.kykxs_spectra[field][flux]['ky'],axis=0)))


##### Test writing "big" hdf5 for NL run ##########
# 

# NL run
gkw_home='/home/yann/runs1/gkw/'
proj='TCV_RAMPUP_1'
flnm='nl64965r05t1_nom'
tint=[150,300]
tint=[150,152]

# test first conversion without the kykxs spectra
NLrun=gkwrun.GKWrun()
NLrun.set_storage(gkw_home+proj,'multiple_folders',flnm)
NLrun.read(kykxs=False)
gkids=gkwimas.gkw2ids(NLrun,tint,21)

g,k=idspy.ids_to_hdf5(gkids,'/home/yann/tmp/'+flnm+'_nokykxs.hdf5',overwrite=True) 



newids=ids_gyrokinetics_local.GyrokineticsLocal()
idspy.hdf5_to_ids('/home/yann/tmp/'+flnm+'_nokykxs.hdf5',newids,todict=True,fill=True)

# and then with the kykxs spectra
NLrun=gkwrun.GKWrun()
NLrun.set_storage(gkw_home+proj,'multiple_folders',flnm)
NLrun.read(kykxs=False)
NLrun.get_kykxs_spectra_lowmem(tint)
NLrun.get_transport_fluxes()
gkids=gkwimas.gkw2ids(NLrun,tint,21)

g,k=idspy.ids_to_hdf5(gkids,'/home/yann/tmp/'+flnm+'.hdf5',overwrite=True) 
# with imaspy
if 0:
  import imaspy
  hdf5_dbentry=imaspy.DBEntry("imas:hdf5?path=/home/yann/tmp/test","w")
  hdf5_dbentry.put(gkids)

newids=ids_gyrokinetics_local.GyrokineticsLocal()
idspy.hdf5_to_ids('/home/yann/tmp/'+flnm+'.hdf5',newids)
# with imaspy
if 0:
  hdf5_dbentry=imaspy.DBEntry("imas:hdf5?path=/home/yann/tmp/test","r")
  newids=hdf5_dbentry.get("gyrokinetics_local")

