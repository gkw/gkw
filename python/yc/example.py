import importlib #to reload modules by doing importlib.reload(gkwrun)
import os
os.chdir('/home/yann/codes/gkw/python/yc')
import gkwrun
os.chdir('/home/yann/codes/gkw/python/yc')
import gkwimas


# specify the type of file storage and get the corresponding paths
gkw_home='/home/yann/runs1/gkw/'
proj='PFS_Samuele'
flnm='Waltz_disp_1_1_1'  # linear run, phi+apar, no collisions, no parallel.dat, no kykxs for apar

proj='TCVREV_3'
flnm='p2LINhigh_2_2'    # linear run, not normalised

proj='JET_KBMbis'
flnm='j15_NLfiW_rlte_exb_1'     # non-linear run

proj='GKDB_TEST'
flnm='ref_fourier'     # linear run, fourier, phi only, no kykxs diagnostics

proj='GKDB_2'
flnm='JDW352_3'     # linear run, chease, no kykxs diags, input_out not formatted properly

proj='WEST_1'
flnm='55797b_kth02_2_2'   # linear run, fourier, kykxs, phi+apar+bpar

proj='TCVREV_2'
flnm='p1upr1_5_5'

proj='TCV_RAMPUP_1'
flnm='nl64965r07t1_nom'



# create an instance of the run class
tt=gkwrun.GKWrun()

tt.set_storage(gkw_home+proj,'multiple_folders',flnm)

# load input files (input.dat and input_out.dat)
tt.get_inputs()

# load geom
tt.get_geom()

# load grids 
tt.get_grids()
tt.get_s0()

# load output file
tt.get_info()

# load eigenvalues
tt.get_eigenvalues()

# load collisions
tt.get_collisions()

# load background density
tt.get_cfdens()

# load fluxes
tt.get_fluxes()

# load fields and moments from parallel.dat
tt.get_parallel()

# load kykxs fields and moments 
tt.get_kykxs({})
tt.get_kykxs({'phi','apar','bpar'},[],prec='sp')
tt.get_kykxs({'dens','vpar','Tperp','Tperp_J0'},[])

# make a GK IDS from GKW inputs and outputs
tt.gkw2ids([])




# for NL runs, one needs to compute the fluxes per (ky,kx) from the kykxs moments to fill the fluxes_norm_gyrocenter table
# self.ids['wavevector'][ii_wav]['eigenmode'][ii_eiv]['fluxes_moments'][ii_sp]['fluxes_norm_gyrocenter']

if 0:

  # to do: put all IDS related routines in a separate class

  # checks
  ne=tt.ids['wavevector'][0]['eigenmode'][0]['fluxes_moments'][1]['moments_norm_gyrocenter']['density']
  ni=tt.ids['wavevector'][0]['eigenmode'][0]['fluxes_moments'][0]['moments_norm_gyrocenter']['density']
  nz=tt.ids['wavevector'][0]['eigenmode'][0]['fluxes_moments'][2]['moments_norm_gyrocenter']['density']
  theta=tt.ids['wavevector'][0]['eigenmode'][0]['poloidal_angle']
  plt.plot(theta,ne)
  plt.plot(theta,ni)

  plt.plot(theta,np.real(tt.ids['wavevector'][0]['eigenmode'][0]['phi_potential_perturbed_norm']))
  plt.plot(theta,np.imag(tt.ids['wavevector'][0]['eigenmode'][0]['phi_potential_perturbed_norm']))


  # temporary, look how a JSON file is loaded in python
  import json
  with open('/home/yann/projects/gkdb/GENE_GKW/GKW-case-1-1.json') as f:
   ids=json.load(f)


  # structure represented as a combination of dicts and lists of dicts
  type(ids['wavevector']) # list
  type(ids['wavevector'][0]) # list


      # ids properties (to fill later)
  #    self.ids['ids_properties']['comment']=""
  #    self.ids['ids_properties']['provider']=""
  #    self.ids['ids_properties']['creation_date']=""

