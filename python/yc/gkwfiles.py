# Module containing functions to handle native GKW input and output files

import os
import numpy as np
import glob
import gkwrun

def binary_conversion(root_path,project=None):
  """
  Convert GKW binary files in spectra_3D (kykxs diagnostics) from double to single precision
  New single precision binary files will be created with the suffix '_sp'
  Old double precision binary files will be deleted after checking that the content of the new files matches that of the old ones.

  Inputs  
    root_path     path of the folder where the GKW projects are stored (multi-folders file storage)
    project       project name, if None applies to all projects in the root_path folder
  """

  threshold=np.finfo('f').eps
  dir_list=('fields','j0_moments','moments','j1_moments')


  if project is None:
    proj_list=[proj.name for proj in os.scandir(root_path)]
  else:
    proj_list=[project]
    
  for proj in proj_list:
    for base_dir in dir_list:
      if os.path.isdir(root_path+proj+'/spectra_3D/'+base_dir):
        for run in os.scandir(root_path+proj+'/spectra_3D/'+base_dir):
          endianness=''
          if run.is_dir():
            print(run.path)
            for f in os.scandir(root_path+proj+'/spectra_3D/'+base_dir+'/'+run.name):
              if os.path.isfile(f.path) and (not f.path[-3:]=='_sp'):
                with open(f.path,'r') as f_in:  # read file
                  if endianness!='<' and endianness!='>': # if not known, attempts automatic detection
                    endianness='<' # test little endian first
                    dum=np.fromfile(f_in,dtype=endianness+'d') # 'd' is double precision
                    f_in.seek(0) #rewind file 
                    if np.any(dum>1e50):
                      endianness='>' # then big endian
                  dum=np.fromfile(f_in,dtype=endianness+'d')
                with open(f.path+'_sp','wb') as f_out:  # write file in single precision
                  dum.astype('float32').tofile(f_out)
                with open(f.path+'_sp','r') as f_in:  # read file and compare with double precision results
                  dum2=np.fromfile(f_in,dtype='<f')
                dum3=dum2.astype('float64')
                dum3[np.abs(dum3)<np.finfo('d').eps]=np.finfo('d').eps
                check=(dum-dum2)/dum3
                if any(check>threshold):
                  print("Problem for file "+f.path)   
                  plt.figure()
                  plt.plot(check)
                  raise ValueError('Bad thing happened')
                else:
                  os.remove(f.path)
           

def binary_to_hdf5(root_path,proj,run=None,precision='sp',delete=False):
  """
  Convert GKW binary files in spectra_3D (kykxs diagnostics) into an hdf5 file (single precision)
  One hdf5 file per field/moment will be created.
  Only applicable to non-linear runs (could in principle work for linear runs, but not tested)

  Inputs
    root_path     path of the folder where the GKW projects are stored (multi-folders file storage)
    proj          project name
    run           run name. Optional, will loop over all runs if not given
    precision     'sp' if single precision is used for the binary files (suffix '_sp'). 'dp' for double precision
    delete        if True, delete the original binary files (use with caution)
  """

  dir_list=('fields','j0_moments','moments','j1_moments')
  mom_list={'phi','apar','bpar','dens','vpar','Tpar','Tperp','Qpar','dens_J0','vpar_J0','Tpar_J0','Tperp_J0','Qpar_J0','Tperp_J1','M12_J1','M24_J1'}
  suffix=''
  if precision=='sp':
    suffix='_sp'

  if run is None:
    for flnm in os.scandir(root_path+proj+'/input_out/'):
      tt=gkwrun.GKWrun()
      tt.set_storage(root_path+proj,'multiple_folders',flnm.name)
      tt.get_inputs()
      if tt.input_out['CONTROL']['non_linear']==True: 
        tt.read(kykxs=False) 
        # load data and write hdf5 files
        print("Start conversion of "+flnm.name)      
        tt.settings['verbose']=False
        for mom in mom_list:
          print("  "+mom)
          tt.get_kykxs(to_load=mom,prec=precision,from_hdf5=False,to_hdf5=True)
          del tt.kykxs[mom]
        if delete==True:
          # remove original binary files
          print("  Delete original files")
          for base_dir in dir_list:
            flpth=tt.storage['path'][base_dir]
            for f in os.scandir(flpth+'hdf5'):
              if os.path.isfile(f.path) and f.name[-3:]=='.h5':
                for ff in glob.glob(flpth+f.name[0:3]+'*l'+suffix):
                  os.remove(ff)
                for ff in glob.glob(flpth+f.name[0:3]+'*g'+suffix):
                  os.remove(ff)
  else:
    tt=gkwrun.GKWrun()
    tt.set_storage(root_path+proj,'multiple_folders',run)
    tt.get_inputs()
    if tt.input_out['CONTROL']['non_linear']==True: 
      tt.read(kykxs=False) 
      # load data and write hdf5 files
      print("Start conversion of "+run)      
      tt.settings['verbose']=False
      for mom in mom_list:
        print("  "+mom)
        tt.get_kykxs(to_load=mom,prec=precision,from_hdf5=False,to_hdf5=True)
        del tt.kykxs[mom]
      if delete==True:
        # remove original binary files
        print("  Delete original files")
        for base_dir in dir_list:
          flpth=tt.storage['path'][base_dir]
          for f in os.scandir(flpth+'hdf5'):
            if os.path.isfile(f.path) and f.name[-3:]=='.h5':
              for ff in glob.glob(flpth+f.name[0:3]+'*l'+suffix):
                os.remove(ff)
              for ff in glob.glob(flpth+f.name[0:3]+'*g'+suffix):
                os.remove(ff)
