### required modules
import os
import f90nml
import re
import datetime
import copy
import numpy as np
from scipy.interpolate import CubicSpline
from scipy.special import i0e
from scipy.special import i1e
import h5py
import inspect
import subprocess


class GKWrun:
  """ Load and manipulate data from a GKW run

      GKWrun.storage          -> path of GKW input and outputs files
                                 filled by set_storage
            .settings         -> parameters controlling the behaviour of the GKWrun class
            .input            -> content of GKW input files input.dat and input_out.dat
            .input_out           filled by get_inputs
            .geom             -> content of geom.dat output file
                                 filled by get_geom
            .grids            -> following grids: time, s, kthrho, krrho, keps, kzeta, file_count
                                 filled by get_grids
            .info             -> content of GKW 'out' file (as a string)
                                 filled by get_info
            .eigenvalues      -> mode growth rate and real frequency
                                 filled by get_eigenvalues
            .collisions       -> collision frequencies for all species
                                 filled by get_collisions
            .cfdens           -> background density and density gradient when poloidal asymmetry
                                 filled by get_cfdens
            .fluxes           -> fluxes in co-moving and laboratory frames
            .fluxes_lab          read directly from GKW outputs
                                 gyrocenter fluxes, parallel momentum only, no CF energy contribution to heat flux
                                 filled by get_fluxes
            .fluxes_transport -> physical fluxes to be used in 1D transport equations
            .fluxes_transport_lab    follows IMAS GK DD definitions (from Sugama/Belli)
                                     fluxes given in laboratory frame, includes pol/mag effect for momentum flux, CF energy for heat flux 
                                 filled by get_transport_fluxes, needs kykxs_spectra
            .parallel         -> parallel structure of fields and moments from parallel.dat
                                 filled by get_parallel
            .spectra          -> intensity and fluxes spectra computed internally in GKW
                                 filled by get_spectra
            .kykxs            -> fields and moments from kykxs diagnostics
                                 filled by get_kykxs
            .kykxs_spectra    -> various summed/averaged intensity and fluxes spectra computed from kykxs fields/moments
                                 filled by get_kykxs_spectra
            .log              -> dict containing warning/error messages generated when calling the class methods

     If data is missing, empty dict, strings or arrays are returned

     Other methods:
            .write_input      -> write a GKW input file using the data in .input or .input_out
            .get_kperp        -> computes k_perp*rho_ref and adds it to the geom grid
            .get_s0           -> computes ballooning angle shift (in s coord.) and adds it to the geom grid
            .read             -> load available inputs and outputs
 
  """

  def set_storage(self,flpth,file_style='multiple_folders',flnm=None):
    """ Get the path of GKW input and output files 
    Inputs
      file_style   either 'single_folder' or 'multiple_folders' file storage
      flpth        path of the project main folder (multi-folders file storage) 
                   or path to the run folder (single folder file storage))
      flnm         name of the run (used only for multi-folders file storage)
    Outputs
      GKWrun.storage['path']    dict containing the path to the various GKW inputs and outputs
    """
    # checks
    if flpth[-1]!='/':
      flpth=flpth+'/'    
    assert (file_style in ('single_folder','multiple_folders')),\
           "Accepted values for 'file_style' are 'single_folder' or 'multiple_folders'"
    assert not((file_style=='multiple_folders') & (flnm==None)),\
           "flnm required for file_style='multiple_folders'"
    # store input values
    self.storage={}
    self.storage['style']=file_style
    self.storage['flpth']=flpth
    self.storage['flnm']=flnm
    # store path to GKW inputs and outputs
    self.storage['path']={}
    if self.storage['style']=='single_folder':
      self.storage['path']['input']=flpth+'input.dat'
      self.storage['path']['input_out']=flpth+'input.out'
      # to be completed
      self.storage['path']['file_count']=flpth+'file_count'
      self.storage['path']['kthrho']=flpth+'krho'
      self.storage['path']['keps']=flpth+'kxrh'
      self.storage['path']['geom']=flpth+'geom.dat'
      self.storage['path']['time']=flpth+'time.dat'
      self.storage['path']['out']=flpth+'out'
      self.storage['path']['CollFreqs']=flpth+'other/CollFreqs'
      self.storage['path']['cfdens']=flpth+'cfdens.dat'
      self.storage['path']['parallel']=flpth+'parallel.dat'
      self.storage['path']['fluxes_phi']=flpth+'fluxes.dat'
      self.storage['path']['fluxes_apar']=flpth+'fluxes_em.dat'
      self.storage['path']['fluxes_bpar']=flpth+'fluxes_bpar.dat'
      self.storage['path']['fluxes_lab_phi']=flpth+'fluxes_lab.dat'
      self.storage['path']['fluxes_lab_apar']=flpth+'fluxes_em_lab.dat'
      self.storage['path']['fluxes_lab_bpar']=flpth+'fluxes_bpar_lab.dat'
      self.storage['path']['fields']=flpth
      self.storage['path']['moments']=flpth
      self.storage['path']['j0_moments']=flpth
      self.storage['path']['j1_moments']=flpth
      self.storage['path']['kyspec_phi']=flpth +'kyspec'
      self.storage['path']['kyspec_apar']=flpth+'kyspec_em'
      self.storage['path']['kyspec_pflux_phi']=flpth+'spectrum/pflux_spectra.dat'
      self.storage['path']['kyspec_pflux_apar']=flpth+'spectrum/pflux_em_spectra.dat'
      self.storage['path']['kyspec_eflux_phi']=flpth+'spectrum/eflux_spectra.dat'
      self.storage['path']['kyspec_eflux_apar']=flpth+'spectrum/eflux_em_spectra.dat'
      self.storage['path']['kyspec_vflux_phi']=flpth+'spectrum/vflux_spectra.dat'
      self.storage['path']['kyspec_vflux_apar']=flpth+'spectrum/vflux_em_spectra.dat'
    elif self.storage['style']=='multiple_folders':      
      self.storage['path']['input']=flpth+'input/'+flnm
      self.storage['path']['input_out']=flpth+'input_out/'+flnm
      self.storage['path']['file_count']=flpth+'grids/file_count/'+flnm
      self.storage['path']['kthrho']=flpth+'grids/krho/'+flnm
      self.storage['path']['keps']=flpth+'grids/kxrh/'+flnm
      self.storage['path']['geom']=flpth+'geom/'+flnm
      self.storage['path']['time']=flpth+'time/'+flnm
      self.storage['path']['out']=flpth+'out/'+flnm
      self.storage['path']['CollFreqs']=flpth+'other/'+flnm +'/CollFreqs'
      self.storage['path']['cfdens']=flpth+'cfdens/'+flnm
      self.storage['path']['parallel']=flpth+'parallel/'+flnm
      self.storage['path']['fluxes_phi']=flpth+'fluxes/'+flnm
      self.storage['path']['fluxes_apar']=flpth+'fluxes/em/'+flnm
      self.storage['path']['fluxes_bpar']=flpth+'fluxes/bpar/'+flnm
      self.storage['path']['fluxes_lab_phi']=flpth+'fluxes_lab/'+flnm
      self.storage['path']['fluxes_lab_apar']=flpth+'fluxes_lab/em/'+flnm
      self.storage['path']['fluxes_lab_bpar']=flpth+'fluxes_lab/bpar/'+flnm
      self.storage['path']['fields']=flpth+'spectra_3D/fields/'+flnm+'/'
      self.storage['path']['moments']=flpth+'spectra_3D/moments/'+flnm+'/'
      self.storage['path']['j0_moments']=flpth+'spectra_3D/j0_moments/'+flnm+'/'
      self.storage['path']['j1_moments']=flpth+'spectra_3D/j1_moments/'+flnm+'/'
      self.storage['path']['kyspec_phi']=flpth+'spectrum/kyspec/'+flnm
      self.storage['path']['kyspec_apar']=flpth+'spectrum/kyspec_em/'+flnm
      self.storage['path']['kyspec_pflux_phi']=flpth+'spectrum/pflux/'+flnm
      self.storage['path']['kyspec_pflux_apar']=flpth+'spectrum/pflux_em/'+flnm
      self.storage['path']['kyspec_eflux_phi']=flpth+'spectrum/eflux/'+flnm
      self.storage['path']['kyspec_eflux_apar']=flpth+'spectrum/eflux_em/'+flnm
      self.storage['path']['kyspec_vflux_phi']=flpth+'spectrum/vflux/'+flnm
      self.storage['path']['kyspec_vflux_apar']=flpth+'spectrum/vflux_em/'+flnm

  def msg_log(self,parent,msg,verbose): 
    """ Generic routine to store and optionally display warning/error messages 
    Inputs    
      parent      name of the method for which a message needs to be stored/displayed 
      msg         message to be stored/displayed
      verbose     if True, displays the message on screen
    """
    if parent not in self.log.keys():
      self.log[parent]=[]
    self.log[parent].append(msg)
    if verbose is True:
      print(msg)

  def load_file(flname,fltype,endianness=""): #to be called only from the class
    """ Generic routine to load GKW I/O files depending of their type 
    Inputs    
      flname      path+name of the file to open
      fltype      one among nml       -> fortran namelist, loaded with f90nml.read(f)
                            ftext     -> formated text (e.g. geom.dat), loaded with f.read
                            array     -> array of floats (e.g. fluxes.dat), loaded with np.loadtxt(f)
                            binary    -> array of floats in binary format (double precision)
                            binary_sp -> array of floats in binary format (single precision)
      endianness  can be '>'(big) or '<' (little). If empty attempts automatic detection.
                  only used for binary format

    Outputs
      Depending on fltype: nml   ->  dict with the content of the namelist
                           ftext ->  string
                           array ->  numpy array 
                           binary -> numpy array
      err                  True if File Not Found Error, False otherwise
      msg                  Error message with the name of the file not found
    """
    assert (fltype in ('nml','ftext','array','binary','binary_sp')), \
           "Accepted values for 'fltype' are 'nml', 'ftext', 'array', 'binary' or 'binary_sp'"
    if fltype=='binary':
      prec='d'
    elif fltype=='binary_sp':
      prec='f'
    msg=""
    err=False
    try:
      with open(flname,'r') as f:
        if fltype=='nml': 
          dum=f90nml.read(f)
          for x in dum.keys(): # remove potential trailing spaces in strings
            if type(dum[x])==list:
              for ii in range(len(dum[x])):
                for y in dum[x][ii].keys():
                  if type(dum[x][ii][y])==str:
                    dum[x][ii][y]=dum[x][ii][y].strip()
            else:
              for y in dum[x].keys():
                if type(dum[x][y])==str:
                  dum[x][y]=dum[x][y].strip()
          return err,msg,dum
        if fltype=='ftext':
          return err,msg,f.read()
        if fltype=='array':
          return err,msg,np.loadtxt(f,dtype=float)
        if fltype=='binary' or fltype=='binary_sp':
          if endianness!='<' and endianness!='>': # if not known, attempts automatic detection
            endianness='<' # test little endian first
            dum=np.fromfile(f,dtype=endianness+prec)
            f.seek(0) #rewind file 
            if np.any(dum>1e50):
              endianness='>'
          return err, msg, np.fromfile(f,dtype=endianness+prec), endianness
    except FileNotFoundError as err:
      msg="Warning: file "+err.filename+" not found."
      err=True
      if fltype=='nml': 
        return err, msg, dict()
      if fltype=='ftext':
        return err, msg, ""
      if fltype=='array':
        return err, msg, np.array([])
      if fltype=='binary' or fltype=='binary_sp':
        return err, msg, np.array([]), endianness


  def get_inputs(self):
    """ Load GKW input files (both input.dat and input_out.dat) in the following dicts:
          GKWrun.input
          GKWrun.input_out
    """
    (err,msg,self.input)=GKWrun.load_file(self.storage['path']['input'],'nml')
    if err:
      self.msg_log('get_inputs',msg,self.settings['verbose'])
    err,msg,self.input_out=GKWrun.load_file(self.storage['path']['input_out'],'nml')
    if err:
      self.msg_log('get_inputs',msg,self.settings['verbose'])
    self.nsp=self.input_out['gridsize']['number_of_species']
    self.ns=self.input_out['gridsize']['n_s_grid']
    self.ns_per_turn=int(self.input_out['gridsize']['n_s_grid']/(2*self.input_out['gridsize']['nperiod']-1))
    self.nkx=self.input_out['gridsize']['nx']
    self.nky=self.input_out['gridsize']['nmod']
   

  def get_geom(self):
    """ Load GKW geom file in the following dict:
          GKWrun.geom
    """
    err,msg,str=GKWrun.load_file(self.storage['path']['geom'],'ftext')
    if err:
      self.msg_log('get_geom',msg,self.settings['verbose'])
    p=re.compile(r'^(\S+)\n',flags = re.MULTILINE) # finds text starting from the beginning of the line
    m=p.split(str)
    self.geom={}
    for i in range(1,len(m),2):
      try:
        self.geom[m[i]]=int(m[i+1])
      except ValueError:
        try:
          self.geom[m[i]]=float(m[i+1])
        except ValueError:
          self.geom[m[i]]=np.fromstring(m[i+1],dtype=float,sep=' ')
    # compute krnorm manually if missing from geom.dat file
    if 'krnorm' not in self.geom:
      msg="'krnorm' not available in geom.dat, computed from 'g_eps_eps'"
      self.msg_log('get_geom',msg,self.settings['verbose'])
      f=CubicSpline(self.geom['s_grid'],np.sqrt(self.geom['g_eps_eps']),bc_type='natural')
      self.geom['krnorm']=f(0)


  def get_eigenvalues(self):
    """ Load mode growth rate and real frequency in
          GKWrun.eigenvalues['gamma']
          GKWrun.eigenvalues['freq']
    """
    err,msg,dum=GKWrun.load_file(self.storage['path']['time'],'array')
    if err:
      self.msg_log('get_eigenvalues',msg,self.settings['verbose'])
    self.eigenvalues={}
    # this works for initial value runs, need to treat eigenvalue runs differently
    if dum.size!=0 and dum.ndim==2:
      self.eigenvalues['gamma']=dum[:,1]
      self.eigenvalues['freq']=dum[:,2]
    else:
      self.eigenvalues['gamma']=np.array([])
      self.eigenvalues['freq']=np.array([])

 
  def get_grids(self,clean_time=True):
    """ Load the various grids: time, s, kthrho, krrho, keps, kzeta, file_count in the dict
          GKWrun.grids
    
        Inputs
          clean_time    if True, automatically detect messed up time base and fix it (for non-linear runs only)
    """
    self.grids={}
    # time grid
    # this works for initial value runs, need to treat eigenvalue runs differently?
    # if not, each "time slice" corresponds to a different eigenvalue, maybe ok but not very straigtforward
    err,msg,dum=GKWrun.load_file(self.storage['path']['time'],'array')
    if err:
      self.msg_log('get_grids',msg,self.settings['verbose'])
    nt=dum.shape[0]
    if dum.ndim<2: # NL runs, only one column
      if clean_time is True:
        It_clean=np.asarray(range(nt))
        smallest_time=dum[-1]
        for ii_t in range(nt-1,0,-1):
          if dum[ii_t-1]<dum[ii_t]:
            smallest_time=dum[ii_t-1]
          else:
            dum=np.delete(dum,ii_t-1)
            It_clean=np.delete(It_clean,ii_t-1)
        self.__It_clean=It_clean
        msg="Removed "+str(nt-It_clean.shape[0])+" time slices"
        self.msg_log('clean_time',msg,self.settings['verbose'])
        if np.any(np.diff(dum)<0):
          msg="Error when cleaning time base"
          self.msg_log('clean_time',msg,True)
          return
      else:
        self.__It_clean=np.asarray(range(nt))
      self.grids['time']=dum 
    else:
      self.grids['time']=dum[:,0]
      self.__It_clean=np.asarray(range(nt))
    self.nt=dum.shape[0]

    # s grid (from geom outputs)
    self.grids['s']=self.geom['s_grid']

    # kx and ky grids
    err,msg,dum=GKWrun.load_file(self.storage['path']['keps'],'array')
    if err:
      self.msg_log('get_grids',msg,self.settings['verbose'])
    if dum.ndim==2:
      self.grids['keps']=dum[0,:]
    else:
      self.grids['keps']=dum
    self.grids['krrho']=np.array(self.grids['keps']*self.geom['krnorm'])

    err,msg,dum=GKWrun.load_file(self.storage['path']['kthrho'],'array')
    if err:
      self.msg_log('get_grids',msg,self.settings['verbose'])
    if dum.ndim==2:
      self.grids['kthrho']=dum[:,0]
    else:
      self.grids['kthrho']=dum
    self.grids['kzeta']=np.array(self.grids['kthrho']/self.geom['kthnorm'])

    # file counter for kykxs diagnostics
    err,msg,dum=GKWrun.load_file(self.storage['path']['file_count'],'array')
    if err:
      self.msg_log('get_grids',msg,self.settings['verbose'])
    if dum.size==0 and not self.input_out['CONTROL']['non_linear']: # missing file_count, attempts to built it for linear runs
      self.grids['file_counter']=np.arange(1,len(self.grids['time'])+1) 
      msg="Warning, file_count missing: build it from the time grid (may fail)"
      self.msg_log('get_grids',msg,self.settings['verbose'])
    else:
      self.grids['file_counter']=dum[self.__It_clean].astype('int')


  def get_s0(self):
    """ Compute the ballooning angle shift s0 (in GKW s coordinate) corresponding to krrho
          Stored in GKWrun.grids['s0']
    """
    if self.grids['keps'].size==1 and self.grids['kzeta'].size==1:
      dum=self.grids['keps']*self.geom['g_eps_eps']+self.grids['kzeta']*self.geom['g_eps_zeta']
      f=CubicSpline(self.geom['s_grid'],dum,bc_type='natural')
      s_fine=np.linspace(-0.55,0.55,self.ns_per_turn*100)
      I_s0=np.argmin(np.abs(f(s_fine)))
      self.grids['s0']=s_fine[I_s0]
    else:      
      msg="Warning, s0 calculation only implemented for a single (keps,kzeta). Value not computed"
      self.msg_log('get_s0',msg,self.settings['verbose'])
      self.grids['s0']=None


  def get_kperp(self):
    """ Compute k_perp*rho_ref and (k_perp.grad_eps)*rho_ref and store it in the geom dict:
          GKWrun.geom['kperp']  (nky*nkx*ns)
          GKWrun.geom['kperp_eps']  (nky*nkx*ns)
       Requires data obtained with get_geom and get_grids
    """
    kzeta=np.moveaxis(np.tile(self.grids['kzeta'],[self.nkx,self.ns,1]),2,0)
    keps =np.moveaxis(np.tile(self.grids['keps'],[self.nky,self.ns,1]),2,1)
    gepseps=np.tile(self.geom['g_eps_eps'],[self.nky,self.nkx,1])
    gepszeta=np.tile(self.geom['g_eps_zeta'],[self.nky,self.nkx,1])
    gzetazeta=np.tile(self.geom['g_zeta_zeta'],[self.nky,self.nkx,1])
    self.geom['kperp']=np.sqrt(gepseps*keps**2+2*gepszeta*keps*kzeta+gzetazeta*kzeta**2)
    self.geom['kperp_eps']=gepseps*keps+gepszeta*kzeta


  def get_info(self):
    """ Load the content of the GKW 'out.dat' file as a string and parse some if its content
          GKWrun.info
    """
    self.info={}
    err,msg,self.info['out']=GKWrun.load_file(self.storage['path']['out'],'ftext')
    if err:
      self.msg_log('get_info',msg,self.settings['verbose'])
    # get commit version
    err,msg,dum=GKWrun.load_file(self.storage['path']['input_out'],'ftext')
    if err:
      self.msg_log('get_info',msg,self.settings['verbose'])
    self.info['commit']=re.search(r'(?<=-g)\w*-*\w*',dum).group(0)
    date_raw=re.search(r'!\s+(\d{1,2}\s+\S+\s+\d{4}+\s\s+\d{2}:\d{2}:\d{2})',dum).group(1)
    date=datetime.datetime.strptime(date_raw,'%d %B %Y  %H:%M:%S')
    self.info['date']=date.strftime('%Y-%m-%dT%H:%M:%SZ')
    shear=re.search(r'shear\_rate used \:(.*)',self.info['out'])
    if shear is None:
      self.info['shearing_rate']=0
    else:
      self.info['shearing_rate']=float(shear.group(1))
    rotate_factor=re.search(r'parallel.dat:\ normalized\ \(rotated\ in\ complex\ plane\)\ by:\ \n\s*\((.*),(.*)\)',self.info['out'])
    if rotate_factor is None:
      self.info['rotate_factor']=1+0j
    else:
      self.info['rotate_factor']=float(rotate_factor.group(1))+1j*float(rotate_factor.group(2))

  def get_collisions(self):
    """ Load the collision frequency for all species (i.e. gammab in GKW)
        nu_i_j [vth_i/Rref] with i lines, j columns in the array
          GKWrun.collisions
    """
    err,msg,dum=GKWrun.load_file(self.storage['path']['CollFreqs'],'array')
    if err:
      self.msg_log('get_collisions',msg,self.settings['verbose'])
    if dum.size!=0:
      dum=dum[:,2]
      self.collisions=np.reshape(dum,(self.nsp,self.nsp))
    else:
      self.collisions=np.array([])


  def get_cfdens(self):
    """ Load the poloidally varying background density and density gradient on the s grid
          GKWrun.cfdens['n_pol']  [nspecies] with n_pol=n(s)/n(s=0)
          GKWrun.cfdens['rln_pol']    [nspecies]
    """
    self.cfdens={}
    err,msg,dum=GKWrun.load_file(self.storage['path']['cfdens'],'array')    
    if err:
      self.msg_log('get_cfdens',msg,self.settings['verbose'])
    if dum.size!=0:
      self.cfdens['rln_pol']=dum[:,1:1+self.nsp]
      self.cfdens['n_pol']=dum[:,1+self.nsp:-1]

  def get_fluxes(self):
    """ Load the fluxes (GKW units) in 
          GKWrun.fluxes['phi']   [ntime x nspecies]
          GKWrun.fluxes['apar']  [ntime x nspecies]
          GKWrun.fluxes['bpar']  [ntime x nspecies]
          GKWrun.fluxes_lab['phi']   [ntime x nspecies]
          GKWrun.fluxes_lab['apar']  [ntime x nspecies]
          GKWrun.fluxes_lab['bpar']  [ntime x nspecies]
    """
    fl_list1=['fluxes_phi','fluxes_apar','fluxes_bpar']
    fl_list2=['fluxes_lab_phi','fluxes_lab_apar','fluxes_lab_bpar']
    key_list=['phi','apar','bpar']
    self.fluxes={}
    self.fluxes_lab={}
    for fl1,fl2,k in zip(fl_list1,fl_list2,key_list):
      err,msg,dum=GKWrun.load_file(self.storage['path'][fl1],'array')
      if err:
        self.msg_log('get_fluxes',msg,self.settings['verbose'])
      self.fluxes[k]={}
      if dum.size!=0:
        dum=np.reshape(dum[self.__It_clean,:],(self.nt,self.nsp*3))
        self.fluxes[k]['particles']=dum[:,0::3]
        self.fluxes[k]['heat']=dum[:,1::3]
        self.fluxes[k]['momentum_parallel']=dum[:,2::3]
      err,msg,dum=GKWrun.load_file(self.storage['path'][fl2],'array')
      if err:
        self.msg_log('get_fluxes',msg,self.settings['verbose'])
      self.fluxes_lab[k]={}
      if dum.size!=0:
        dum=np.reshape(dum[self.__It_clean,:],(self.nt,self.nsp*3))
        self.fluxes_lab[k]['particles']=dum[:,0::3]
        self.fluxes_lab[k]['heat']=dum[:,1::3]
        self.fluxes_lab[k]['momentum_parallel']=dum[:,2::3]


  def get_parallel(self):
    """ Load the parallel structure of fields and moments from parallel.dat in
          GKWrun.parallel['phi']      [ns]
          GKWrun.parallel['apar']     [ns]
          GKWrun.parallel['bpar']     [ns]
          GKWrun.parallel['dens']        [ns x nspecies]
          GKWrun.parallel['vpar']     [ns x nspecies]
          GKWrun.parallel['Tpar']     [ns x nspecies]
          GKWrun.parallel['Tperp']    [ns x nspecies]
    """
    err,msg,dum=GKWrun.load_file(self.storage['path']['parallel'],'array')
    if err:
      self.msg_log('get_parallel',msg,self.settings['verbose'])
    self.parallel={}
    if dum.size!=0:
      self.parallel['phi']=dum[0:self.ns,1]+1j*dum[0:self.ns,2]
      self.parallel['apar']=dum[0:self.ns,3]+1j*dum[0:self.ns,4]
      self.parallel['bpar']=dum[0:self.ns,13]+1j*dum[0:self.ns,14]
      self.parallel['dens']=dum[0:self.ns,5]+1j*dum[0:self.ns,6]
      self.parallel['vpar']=dum[0:self.ns,11]+1j*dum[0:self.ns,12]
      self.parallel['Tpar']=dum[0:self.ns,7]+1j*dum[0:self.ns,8]
      self.parallel['Tperp']=dum[0:self.ns,9]+1j*dum[0:self.ns,10]

  def get_spectra(self):
    """ Load the fields intensity and fluxes ky spectra computed internally in GKW
          GKWrun.spectra['phi']['intensity']
                               ['particles']
                               ['heat']
                               ['momentum_parallel']
                         ['apar'][...]
    """
    spectra_list={'phi':{'intensity':'kyspec_phi',
                         'particles':'kyspec_pflux_phi',
                         'heat':'kyspec_eflux_phi',
                         'momentum_parallel':'kyspec_vflux_phi'},
                  'apar':{'intensity':'kyspec_apar',
                         'particles':'kyspec_pflux_apar',
                         'heat':'kyspec_eflux_apar',
                         'momentum_parallel':'kyspec_vflux_apar'}}
    self.spectra={}
    for key1 in spectra_list:
      self.spectra[key1]={}
      for key2 in spectra_list[key1]:        
        err,msg,dum=GKWrun.load_file(self.storage['path'][spectra_list[key1][key2]],'array')
        if err:
          self.msg_log('get_spectra',msg,self.settings['verbose'])
        if key2=='intensity':
          dum=dum[self.__It_clean,:]
        else:
          dum=np.reshape(dum[self.__It_clean,:],(self.nt,self.nky,self.nsp),order='F')
        self.spectra[key1][key2]=dum

  def get_kykxs(self,to_load=None,time_interval=np.array([]),spc_indx=np.array([]),prec='sp',from_hdf5=True,to_hdf5=False):
    """ Load the kykxs fields/moments specified in the to_load list for a given time interval and kx interval
        Available fields/moments are:
          phi, apar, bpar
          dens, vpar, Tpar, Tperp, Qpar,
          dens_J0, vpar_J0, Tpar_J0, Tperp_J0, Qpar_J0,
          Tperp_J1, M12_J1, M24_J1 

        Inputs:
          to_load          list of fields/moments to load (string or set of strings, e.g. {'phi','apar'})
                           if unspecified, load all field/moments
          time_interval    time interval for which the files will be loaded [t_start, t_end] (numpy.ndarray)
                           if empty, load all available files
                           not used if xy_estep=F (fields/moments output at last time step only)
                           Note that a unique time_interval is used for all fields/moments. 
          spc_indx         index of species for which the moments will be loaded [0,1] (numpy.ndarray)
                           if empty, load data for all species
          prec             precision used for the binary files, available 'dp' (double) and 'sp' (single)
          from_hdf5        if True, reads the spectra from hdf5 files (10 to 20 times faster)
          to_hdf5          if True, write the loaded data into hdf5 files (single precision)

        Outputs
         GKWrun.kykxs['time_interval']   effective time interval for which the data is loaded
                     ['It']              corresponding indexes in the time grid
                     ['phi']             loaded data, complex array of size nky,nkx,ns,nt,(nsp) 
                     ['apar']
                     etc.             
    """
    # keys: internal name, values: (GKW file prefix , field or moments)
    kykxs_list={'phi': ('Phi','fields'), 'apar': ('Apa','fields'), 'bpar': ('Bpa','fields'), 
                'dens': ('dens','moments'), 'vpar': ('vpar','moments'), 'Tpar': ('Tpar','moments'),
                'Tperp': ('Tperp','moments'), 'Qpar': ('Qpar','moments'), 
                # 'M12': ('M12','moments'),'M24': ('M24','moments'),
                'dens_J0': ('dens_ga','j0_moments'), 'vpar_J0': ('vpar_ga','j0_moments'),
                'Tpar_J0': ('Tpar_ga','j0_moments'), 'Tperp_J0': ('Tperp_ga','j0_moments'),
                'Qpar_J0': ('Qpar_ga','j0_moments'), 
                # 'M12_J0': ('M12_ga','j0_moments'), 'M24_J0': ('M24_ga','j0_moments'), 
                'Tperp_J1': ('Tperp_J1','j1_moments'),
                'M12_J1': ('M12_J1','j1_moments'), 'M24_J1': ('M24_J1','j1_moments')
               }
    # checks
    if to_load is None:
      to_load=list(kykxs_list.keys())
    if type(to_load)==str:
      to_load={to_load}
    time_interval=np.asarray(time_interval)
    spc_indx=np.asarray(spc_indx)
    if spc_indx.size==0:
      spc_indx=range(0,self.nsp)
    # initialise kykxs dict if needed
    if not hasattr(self,'kykxs'):
      self.kykxs={}
    # find time indexes (check if output was requested at each time step)
    if self.input_out['diagnostic']['xy_estep']:
      if time_interval.size==0:
        dum=self.grids['time'][[0,-1]]
        It=range(self.nt)
      else:
        I1=np.abs(self.grids['time']-time_interval[0]).argmin()
        I2=np.abs(self.grids['time']-time_interval[1]).argmin()
        dum=self.grids['time'][[I1, I2]]
        It=range(I1,min(I2+1,self.nt))
    else:
      dum=self.grids['time'][[-1,-1]]
      It=range(self.nt-1,self.nt)
      if time_interval.size!=0:
        msg="Warning, only last time step available for kykxs diagnostic"
        self.msg_log('get_kykxs',msg,self.settings['verbose'])
    # store time interval and local time grid
    if ('time_interval' in self.kykxs) and np.any(self.kykxs['time_interval']!=dum):
      msg="Warning: 'time_interval' already defined for kykxs fields/moments and different from requested. No data loaded."
      self.msg_log('get_kykxs',msg,self.settings['verbose'])
      return
    else:
      self.kykxs['time_interval']=dum
    self.kykxs['It']=It
    # find kx indexes (to implement later if needed)
    # either by specifying the kx interval or with a switch so that kx_max=3*ky_max
    Ikx=[]
    self.kykxs['Ikx']=Ikx

    endianness=''
    for kk in to_load:
      if kk in kykxs_list:
        flpth=self.storage['path'][kykxs_list[kk][1]]
        hdf5_loaded=False
        if from_hdf5 and os.path.exists(flpth+'hdf5'+'/'+kykxs_list[kk][0]+'.h5'):
          with h5py.File(flpth+'hdf5'+'/'+kykxs_list[kk][0]+'.h5','r') as f:
            h5_file_counter=f['file_counter'][()]
            if np.all(np.isin(self.grids['file_counter'][It],h5_file_counter)):
              It_h5_slice=[np.argwhere(dd==h5_file_counter)[0][0] for dd in self.grids['file_counter'][It]]
              dum_nt_first=f[kykxs_list[kk][0]][It_h5_slice,...]
              hdf5_loaded=True
            else:
              msg="Warning: hdf5 file for " +kk+ " does not have required file counter, switch to binary reading"
              self.msg_log('get_kykxs',msg,self.settings['verbose'])
        if not hdf5_loaded: #attempts reading from binary files
          if kykxs_list[kk][1]=='fields':  # fields
            dum_nt_first=np.full((len(It),self.nky,self.nkx,self.ns),np.nan+1j*np.nan,np.csingle)
            for ii in range(len(It)):
              flroot=kykxs_list[kk][0]+'_kykxs'+str(self.grids['file_counter'][It[ii]]).zfill(8)
              if prec=='sp': # file stored in single precision
                err,msg,dum_r,endianness=GKWrun.load_file(flpth+flroot+'_real_sp','binary_sp',endianness)
                if err:
                  self.msg_log('get_kykxs',msg,self.settings['verbose'])
                err,msg,dum_i,endianness=GKWrun.load_file(flpth+flroot+'_imag_sp','binary_sp',endianness)
                if err:
                  self.msg_log('get_kykxs',msg,self.settings['verbose'])
              else:          # assume double precision
                err,msg,dum_r,endianness=GKWrun.load_file(flpth+flroot+'_real','binary',endianness)
                if err:
                  self.msg_log('get_kykxs',msg,self.settings['verbose'])
                err,msg,dum_i,endianness=GKWrun.load_file(flpth+flroot+'_imag','binary',endianness)
                if err:
                  self.msg_log('get_kykxs',msg,self.settings['verbose'])
              if dum_r.size==self.nky*self.nkx*self.ns and dum_i.size==self.nky*self.nkx*self.ns:
                dum_r=dum_r.astype('float32')
                dum_r=np.reshape(dum_r,(self.nky,self.nkx,self.ns),order='F')
                dum_i=dum_i.astype('float32')
                dum_i=np.reshape(dum_i,(self.nky,self.nkx,self.ns),order='F')
                dum_nt_first[ii,:,:,:]=dum_r+1j*dum_i
              elif dum_r.size>0 and dum_i.size>0:
                msg="Warning: wrong file size for "+flroot+". No data loaded"
                self.msg_log('get_kykxs',msg,self.settings['verbose'])
          else:                            # moments
            dum_nt_first=np.full((len(It),self.nky,self.nkx,self.ns,self.nsp),np.nan+1j*np.nan,np.csingle)
            for ii in range(len(It)):
              for jj in spc_indx:
                flroot=kykxs_list[kk][0]+'_kykxs'+str(jj+1).zfill(2)+'_'+str(self.grids['file_counter'][It[ii]]).zfill(6)
                if prec=='sp': # file stored in single precision
                  err,msg,dum_r,endianness=GKWrun.load_file(flpth+flroot+'_real_sp','binary_sp',endianness)
                  if err:
                    self.msg_log('get_kykxs',msg,self.settings['verbose'])
                  err,msg,dum_i,endianness=GKWrun.load_file(flpth+flroot+'_imag_sp','binary_sp',endianness)
                  if err:
                    self.msg_log('get_kykxs',msg,self.settings['verbose'])
                else:          # assume double precision
                  err,msg,dum_r,endianness=GKWrun.load_file(flpth+flroot+'_real','binary',endianness)
                  if err:
                    self.msg_log('get_kykxs',msg,self.settings['verbose'])
                  err,msg,dum_i,endianness=GKWrun.load_file(flpth+flroot+'_imag','binary',endianness)
                  if err:
                    self.msg_log('get_kykxs',msg,self.settings['verbose'])
                if dum_r.size==self.nky*self.nkx*self.ns and dum_i.size==self.nky*self.nkx*self.ns:
                  dum_r=dum_r.astype('float32')
                  dum_r=np.reshape(dum_r,(self.nky,self.nkx,self.ns),order='F')
                  dum_i=dum_i.astype('float32')
                  dum_i=np.reshape(dum_i,(self.nky,self.nkx,self.ns),order='F')
                  dum_nt_first[ii,:,:,:,jj]=dum_r+1j*dum_i
                elif dum_r.size>0 and dum_i.size>0:
                  msg="Warning: wrong file size for "+flroot+". No data loaded"
                  self.msg_log('get_kykxs',msg,self.settings['verbose'])
        if np.all(np.isnan(dum_nt_first)):
          self.kykxs[kk]=np.array([])
        else:
          self.kykxs[kk]=np.moveaxis(dum_nt_first,0,3)
          if to_hdf5 and not hdf5_loaded: # write the kykxs spectra in hdf5 files for future use
            # create new folder if needed
            if not os.path.isdir(flpth+'hdf5'):
              os.mkdir(flpth+'hdf5')
            if os.path.exists(flpth+'hdf5'+'/'+kykxs_list[kk][0]+'.h5'):
              msg="Warning: File "+flpth+'hdf5'+'/'+kykxs_list[kk][0]+'.h5'+" already exists. No data saved"
              self.msg_log('get_kykxs',msg,self.settings['verbose'])
              return             
            with h5py.File(flpth+'hdf5'+'/'+kykxs_list[kk][0]+'.h5','w') as f: 
              # time is stored as the 1st dimension for efficient slicing nt,nky,nkx,ns(,nsp)
              f.create_dataset(kykxs_list[kk][0],data=dum_nt_first,dtype='complex64')
              f.create_dataset('file_counter',data=self.grids['file_counter'][It])
      else:
        msg="Warning: Field/moments "+kk+" not defined. Skipped."
        self.msg_log('get_kykxs',msg,self.settings['verbose'])

  def get_kykxs_spectra(self,to_load=None,time_avg=np.array([]),spc_indx=np.array([])):
    """ Compute the time averaged intensity and fluxes spectra from kykxs fields/moments
    Available fields/fluxes are:
      phi, apar, bpar
      particles, heat, momentum_parallel, momentum_parallel_polmag, momentum_perp, momentum_perp_polmag
    The fluxes in kykxs_spectra are GKW fluxes in the rotating frame 
    The cf energy contribution to heat flux and pol/mag contribution to momentum flux are given separately:
      -> the total heat flux is 'heat'+'heat_cf'
      -> the total parallel contribution to the toroidal momentum flux is 'momentum_parallel'+'momentum_parallel_polmag'
      -> the total perpendicular contribution to the toroidal momentum flux is 'momentum_perp'+'momentum_perp_polmag'

    Inputs:
      to_load          dict of field intensity/fluxes to compute, e.g.: 
                         {'phi':('intensity','particles','heat','momentum_parallel','momentum_perp','momentum_perp_polmag'),
                          'apar':('intensity','particles','heat','momentum_parallel','momentum_parallel_polmag','momentum_perp'),
                          'bpar':('intensity','particles','heat','momentum_parallel','momentum_perp','momentum_perp_polmag')}
                       if unspecified, compute all field intensity/fluxes
      time_avg         time interval used to average the fluxes and fields intensity (NL runs only)
                          If empty, uses all available values
      spc_indx         index of species for which the fluxes will be computed, e.g. [0,1] (numpy.ndarray)
                       if empty, compute fluxes for all species
    Outputs:
      GKWrun.kykxs_spectra['phi']['intensity']['kykxs'] (nky*nkx*ns)
                                              ['kykx']
                                              ['ky']
                                 ['zonal'] (nkx*nt)
                                 ['particles']['kykxs'] (nky*nkx*ns*nsp)
                                              ['kykx']
                                              ['ky']
                                              ['kyt'] (nky*nt*nsp)
                                 ['heat'][...]
                                 ['momentum_parallel'][...]
                                 ['momentum_parallel_polmag'][...]
                                 ['momentum_perp'][...]
                                 ['momentum_perp_polmag'][...]
                           ['apar'][...]
                           ['bpar'][...]
    To add: shearing rate (kx,t), see Ajay paper
    """
    compute_list={'phi':('intensity','particles','heat','momentum_parallel','momentum_perp','momentum_perp_polmag'),
                  'apar':('intensity','particles','heat','momentum_parallel','momentum_parallel_polmag','momentum_perp'),
                  'bpar':('intensity','particles','heat','momentum_parallel','momentum_perp','momentum_perp_polmag')}
    kykxs_list={'phi':{'particles': ('dens_J0',),'heat': ('Tpar_J0','Tperp_J0'),'momentum_parallel': ('vpar_J0',),
                       'momentum_perp': ('Tperp_J1',),'momentum_perp_polmag': ()},
                  'apar':{'particles': ('vpar_J0',),'heat': ('Qpar_J0',),'momentum_parallel': ('Tpar_J0',),
                       'momentum_parallel_polmag': (),'momentum_perp': ('M12_J1',)},
                  'bpar':{'particles': ('Tperp_J1',),'heat': ('M24_J1',),'momentum_parallel': ('M12_J1',),
                       'momentum_perp': ('Tperp_J1','Tperp_J0'),'momentum_perp_polmag': ('phi',)}}

    # checks
    if to_load is None:
      to_load=compute_list
    if type(to_load)==str:
      to_load={to_load}
    spc_indx=np.asarray(spc_indx)
    if spc_indx.size==0:
      spc_indx=range(0,self.nsp)
    time_avg=np.asarray(time_avg)
    # when moments unavailable, remove item from to_load list
    for field in copy.copy(to_load):
      if field not in self.kykxs.keys() or self.kykxs[field].size==0:
        to_load.pop(field)
        msg="Warning: Field "+field+" not available. Fluxes "+field+" not computed."
        self.msg_log('get_kykxs_spectra',msg,self.settings['verbose'])
      else:
        for kk in to_load[field]:
          if not kk=='intensity':
            for mom in  kykxs_list[field][kk]:
              if mom not in self.kykxs.keys() or self.kykxs[mom].size==0:
                dum=list(to_load[field])
                dum.remove(kk)
                to_load[field]=tuple(dum)
                msg="Warning: Field/moments "+mom+" not available in kykxs. Flux "+field+" "+kk+" not computed."
                self.msg_log('get_kykxs_spectra',msg,self.settings['verbose'])
        if 'particles' in to_load[field]:
          dum=list(to_load[field])
          dum.append('heat_cf')
          dum.append('tolab_momentum_parallel')
          dum.append('tolab_momentum_perp')
          dum.append('tolab_heat_part')
          to_load[field]=tuple(dum)

    # initialise dicts if needed
    if not hasattr(self,'kykxs_spectra'):
      self.kykxs_spectra={}
    # time base and various quantities required for fluxes calculation
    nt_kykxs=len(self.kykxs['It'])
    self.kykxs_spectra['It']=self.kykxs['It']
    if nt_kykxs>1:
      time_average=True
      if time_avg.size==0:
        It_avg=self.kykxs['It']
      else:
        I1_avg=np.max([np.abs(self.grids['time']-time_avg[0]).argmin(),self.kykxs['It'][0]])
        I2_avg=np.min([np.abs(self.grids['time']-time_avg[1]).argmin(),self.kykxs['It'][-1]])
        if I2_avg<I1_avg:
          msg="Warning: time interval given for average does not intersect available data in self.kykxs"
          self.msg_log('get_kykxs_spectra',msg,True)
          return
        It_avg=range(I1_avg,I2_avg+1)
      t=self.grids['time'][It_avg]
      self.kykxs_spectra['time_interval_avg']=np.asarray([t[0],t[-1]])
      self.kykxs_spectra['It_avg']=It_avg
      I_trapz=range(It_avg[0]-self.kykxs['It'][0],It_avg[-1]-self.kykxs['It'][0]+1)
    else:
      time_average=False
    ints=(self.grids['s'][1]-self.grids['s'][0])/(2*self.input_out['GRIDSIZE']['nperiod']-1)
    fac_kzeta=np.tile(-2*self.geom['E_eps_zeta'].astype('float32'),(self.nky,self.nkx,1)
                     )*np.moveaxis(np.tile(self.grids['kzeta'].astype('float32'),[self.nkx,self.ns,1]),2,0)
    bn=np.tile(self.geom['bn'].astype('float32'),(self.nky,self.nkx,1))
    RBt=np.tile((self.geom['R']*self.geom['Bt_frac']).astype('float32'),(self.nky,self.nkx,1))
    kperp=self.geom['kperp'].astype('float32')
    fac_mom_perp=np.moveaxis(np.tile(self.grids['kzeta'].astype('float32'),[self.nkx,self.ns,1]),2,0)*self.geom['kperp_eps'].astype('float32')/np.pi
    if 'n_pol' in self.cfdens.keys():
      ns_cf=self.cfdens['n_pol'].astype('float32')
    else:
      if self.input_out['ROTATION']['cf_trap']==True:
        msg="Error: cfdens data required when centrifugal effects are included"
        self.msg_log('get_kykxs_spectra',msg,True)
        return        
      else:
        ns_cf=np.ones((self.ns,self.nsp)).astype('float32')
    
    for field in to_load:
      if field not in self.kykxs_spectra.keys():
        self.kykxs_spectra[field]={}
      for kk in to_load[field]:
        if kk not in self.kykxs_spectra[field].keys():
          self.kykxs_spectra[field][kk]={}
        if kk=='intensity': 
          if time_average:
            self.kykxs_spectra[field][kk]['kykxs']=np.trapz(np.abs(self.kykxs[field][:,:,:,I_trapz])**2,t)/(t[-1]-t[0])
          else:
            self.kykxs_spectra[field][kk]['kykxs']=np.abs(self.kykxs[field][:,:,:,0])**2
          self.kykxs_spectra[field][kk]['kykx']=np.sum(self.kykxs_spectra[field][kk]['kykxs'],axis=2)*ints # FS average (done as in GKW)          
          self.kykxs_spectra[field][kk]['ky']=np.sum(self.kykxs_spectra[field][kk]['kykx'],axis=1)
          self.kykxs_spectra[field]['zonal']=np.sum(self.kykxs[field][0,:,:,:],axis=1)*ints
        else:
          if 'kykxs' not in self.kykxs_spectra[field][kk]:
            self.kykxs_spectra[field][kk]['kykxs']=np.full((self.nky,self.nkx,self.ns,self.nsp),np.nan,np.single)
          if 'kykx' not in self.kykxs_spectra[field][kk]:
            self.kykxs_spectra[field][kk]['kykx']=np.full((self.nky,self.nkx,self.nsp),np.nan,np.single)
          if 'ky' not in self.kykxs_spectra[field][kk]:
            self.kykxs_spectra[field][kk]['ky']=np.full((self.nky,self.nsp),np.nan,np.single)
          if 'kyt' not in self.kykxs_spectra[field][kk]:
            self.kykxs_spectra[field][kk]['kyt']=np.full((self.nky,nt_kykxs,self.nsp),np.nan,np.single)
          for ii_sp in spc_indx:
            vthrat=np.sqrt(self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['mass']).astype('float32')
            if ns_cf.size==1:
              ns_rat=1
            else:
              ns_rat=np.tile(ns_cf,(self.nky,self.nkx,1,1))
              ns_rat=ns_rat[:,:,:,ii_sp]
            fac_heat_cf=np.tile(np.log(ns_cf[:,ii_sp]),(self.nky,self.nkx,1))
            fac_tolab_mompar=np.tile((self.input_out['GEOM']['signb']*self.input_out['ROTATION']['vcor']/vthrat
                                     *(self.geom['R']*self.geom['Bt_frac'])**2).astype('float32'),(self.nky,self.nkx,1))
            fac_tolab_momperp=np.tile((self.input_out['GEOM']['signb']*self.input_out['ROTATION']['vcor']/vthrat
                                     *self.geom['R']**2*(1-self.geom['Bt_frac'])**2).astype('float32'),(self.nky,self.nkx,1))
            fac_tolab_Q=np.tile((0.5*self.input_out['SPECIES'][ii_sp]['mass']*self.input_out['ROTATION']['vcor']**2
                                /self.input_out['SPECIES'][ii_sp]['temp']*self.geom['R']**2).astype('float32'),(self.nky,self.nkx,1))
            RBt_ncf=RBt*np.tile(ns_cf[:,ii_sp],(self.nky,self.nkx,1))
            fac_mom_perp_ncf=np.moveaxis(np.tile(self.grids['kzeta'].astype('float32'),[self.nkx,self.ns,1]),2,0
                                         )*self.geom['kperp_eps'].astype('float32')*np.tile(ns_cf[:,ii_sp],(self.nky,self.nkx,1))/np.pi
            b_s=0.5*(vthrat*self.input_out['SPECIES'][ii_sp]['mass']/self.input_out['SPECIES'][ii_sp]['z']
                           *kperp/bn)**2

            int=np.full((self.nky,self.nkx,self.ns,nt_kykxs),np.nan,np.single)
            for ii_t in range(nt_kykxs): # much faster than doing 4D arrays element-wise operations
              if kk=='particles': 
                if field=='phi':
                  int[:,:,:,ii_t]=np.imag(np.conj(self.kykxs['phi'][:,:,:,ii_t])*self.kykxs['dens_J0'][:,:,:,ii_t,ii_sp])
                elif field=='apar':
                  int[:,:,:,ii_t]=-2*vthrat*np.imag(np.conj(self.kykxs['apar'][:,:,:,ii_t])*self.kykxs['vpar_J0'][:,:,:,ii_t,ii_sp])
                elif field=='bpar':  
                  int[:,:,:,ii_t]=2/bn*self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['z']*np.imag(
                             np.conj(self.kykxs['bpar'][:,:,:,ii_t])*self.kykxs['Tperp_J1'][:,:,:,ii_t,ii_sp])
              elif kk=='heat': 
                if field=='phi':
                  int[:,:,:,ii_t]=np.imag(np.conj(self.kykxs['phi'][:,:,:,ii_t])*(self.kykxs['Tpar_J0'][:,:,:,ii_t,ii_sp]
                                                            +2*self.kykxs['Tperp_J0'][:,:,:,ii_t,ii_sp]))
                if field=='apar':
                  int[:,:,:,ii_t]=-2*vthrat*np.imag(np.conj(self.kykxs['apar'][:,:,:,ii_t])*self.kykxs['Qpar_J0'][:,:,:,ii_t,ii_sp])
                if field=='bpar':
                  int[:,:,:,ii_t]=1/bn*self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['z']*np.imag(
                           np.conj(self.kykxs['bpar'][:,:,:,ii_t])*self.kykxs['M24_J1'][:,:,:,ii_t,ii_sp])
              elif kk=='heat_cf': 
                if field=='phi':
                  int[:,:,:,ii_t]=np.imag(np.conj(self.kykxs['phi'][:,:,:,ii_t])*self.kykxs['dens_J0'][:,:,:,ii_t,ii_sp])
                if field=='apar':
                  int[:,:,:,ii_t]=-2*vthrat*np.imag(np.conj(self.kykxs['apar'][:,:,:,ii_t])*self.kykxs['vpar_J0'][:,:,:,ii_t,ii_sp])
                if field=='bpar':
                  int[:,:,:,ii_t]=1/bn*self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['z']*np.imag(
                           np.conj(self.kykxs['bpar'][:,:,:,ii_t])*self.kykxs['Tperp_J1'][:,:,:,ii_t,ii_sp])
                int[:,:,:,ii_t]=-int[:,:,:,ii_t]*fac_heat_cf
              elif kk=='tolab_momentum_parallel':
                if field=='phi':
                  int[:,:,:,ii_t]=np.imag(np.conj(self.kykxs['phi'][:,:,:,ii_t])*self.kykxs['dens_J0'][:,:,:,ii_t,ii_sp])
                elif field=='apar':
                  int[:,:,:,ii_t]=-2*vthrat*np.imag(np.conj(self.kykxs['apar'][:,:,:,ii_t])*self.kykxs['vpar_J0'][:,:,:,ii_t,ii_sp])
                elif field=='bpar':  
                  int[:,:,:,ii_t]=2/bn*self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['z']*np.imag(
                             np.conj(self.kykxs['bpar'][:,:,:,ii_t])*self.kykxs['Tperp_J1'][:,:,:,ii_t,ii_sp])
                int[:,:,:,ii_t]=int[:,:,:,ii_t]*fac_tolab_mompar
              elif kk=='tolab_momentum_perp':
                if field=='phi':
                  int[:,:,:,ii_t]=np.imag(np.conj(self.kykxs['phi'][:,:,:,ii_t])*self.kykxs['dens_J0'][:,:,:,ii_t,ii_sp])
                elif field=='apar':
                  int[:,:,:,ii_t]=-2*vthrat*np.imag(np.conj(self.kykxs['apar'][:,:,:,ii_t])*self.kykxs['vpar_J0'][:,:,:,ii_t,ii_sp])
                elif field=='bpar':  
                  int[:,:,:,ii_t]=2/bn*self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['z']*np.imag(
                             np.conj(self.kykxs['bpar'][:,:,:,ii_t])*self.kykxs['Tperp_J1'][:,:,:,ii_t,ii_sp])
                int[:,:,:,ii_t]=int[:,:,:,ii_t]*fac_tolab_momperp
              elif kk=='tolab_heat_part':
                if field=='phi':
                  int[:,:,:,ii_t]=np.imag(np.conj(self.kykxs['phi'][:,:,:,ii_t])*self.kykxs['dens_J0'][:,:,:,ii_t,ii_sp])
                elif field=='apar':
                  int[:,:,:,ii_t]=-2*vthrat*np.imag(np.conj(self.kykxs['apar'][:,:,:,ii_t])*self.kykxs['vpar_J0'][:,:,:,ii_t,ii_sp])
                elif field=='bpar':  
                  int[:,:,:,ii_t]=2/bn*self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['z']*np.imag(
                             np.conj(self.kykxs['bpar'][:,:,:,ii_t])*self.kykxs['Tperp_J1'][:,:,:,ii_t,ii_sp])
                int[:,:,:,ii_t]=int[:,:,:,ii_t]*fac_tolab_Q
              elif kk=='momentum_parallel': 
                if field=='phi':
                  int[:,:,:,ii_t]=np.imag(np.conj(self.kykxs['phi'][:,:,:,ii_t])*self.kykxs['vpar_J0'][:,:,:,ii_t,ii_sp])
                elif field=='apar':
                  int[:,:,:,ii_t]=-2*vthrat*np.imag(np.conj(self.kykxs['apar'][:,:,:,ii_t])*self.kykxs['Tpar_J0'][:,:,:,ii_t,ii_sp])
                elif field=='bpar':
                  int[:,:,:,ii_t]=1/bn*self.input_out['SPECIES'][ii_sp]['temp']/self.input_out['SPECIES'][ii_sp]['z']*np.imag(
                           np.conj(self.kykxs['bpar'][:,:,:,ii_t])*self.kykxs['M12_J1'][:,:,:,ii_t,ii_sp])
                int[:,:,:,ii_t]=int[:,:,:,ii_t]*self.input_out['GEOM']['signb']*RBt
              elif kk=='momentum_parallel_polmag':
                if field=='apar':
                  int1=np.imag(self.kykxs['phi'][:,:,:,ii_t]*np.conj(self.kykxs['apar'][:,:,:,ii_t]))
                  int2=0
                  if self.input_out['control']['nlbpar']:
                    if 'bpar' not in self.kykxs.keys() or self.kykxs['bpar'].size==0:
                      msg="Warning: Field bpar not available in kykxs. Flux apar momentum_parallel_polmag not computed."
                      self.msg_log('get_kykxs_spectra',msg,self.settings['verbose'])
                    else:
                      int2=np.imag(self.kykxs['bpar'][:,:,:,ii_t]*np.conj(self.kykxs['apar'][:,:,:,ii_t]))
                  int[:,:,:,ii_t]=vthrat*(self.input_out['SPECIES'][ii_sp]['z']/self.input_out['SPECIES'][ii_sp]['temp']
                                           *i0e(b_s)*int1 +(i0e(b_s)-i1e(b_s))/bn*int2)
                  int[:,:,:,ii_t]=int[:,:,:,ii_t]*self.input_out['GEOM']['signb']*RBt_ncf
              elif kk=='momentum_perp': 
                if field=='phi':
                  int[:,:,:,ii_t]=-1/(2*bn**2)*(self.input_out['SPECIES'][ii_sp]['mass']*vthrat
                        /self.input_out['SPECIES'][ii_sp]['z']*np.real(self.kykxs['Tperp_J1'][:,:,:,ii_t,ii_sp]*np.conj(self.kykxs['phi'][:,:,:,ii_t])))
                elif field=='apar':
                  int[:,:,:,ii_t]=1/(bn**2)*(self.input_out['SPECIES'][ii_sp]['temp']
                        /self.input_out['SPECIES'][ii_sp]['z']*np.real(self.kykxs['M12_J1'][:,:,:,ii_t,ii_sp]*np.conj(self.kykxs['apar'][:,:,:,ii_t])))
                elif field=='bpar':
                  int[:,:,:,ii_t]=-1/(bn*kperp**2)*vthrat*np.real(
                            (self.kykxs['Tperp_J1'][:,:,:,ii_t,ii_sp]-2*self.kykxs['Tperp_J0'][:,:,:,ii_t,ii_sp])*np.conj(self.kykxs['bpar'][:,:,:,ii_t]))
                int[:,:,:,ii_t]=int[:,:,:,ii_t]*fac_mom_perp
              elif kk=='momentum_perp_polmag': 
                if field=='phi':
                  int1=np.abs(self.kykxs['phi'][:,:,:,ii_t])**2
                  int2=0
                  if self.input_out['control']['nlbpar']:
                    if 'bpar' not in self.kykxs.keys() or self.kykxs['bpar'].size==0:
                      msg="Warning: Field bpar not available in kykxs. Flux phi momentum_perp_polmag not computed."
                      self.msg_log('get_kykxs_spectra',msg,self.settings['verbose'])
                    else:
                      int2=np.real(self.kykxs['bpar'][:,:,:,ii_t]*np.conj(self.kykxs['phi'][:,:,:,ii_t]))
                  int[:,:,:,ii_t]=-1/(2*bn**2)*(i0e(b_s)-i1e(b_s))*(0.5/vthrat*int1
                         + 0.25*self.input_out['SPECIES'][ii_sp]['mass']*vthrat/(self.input_out['SPECIES'][ii_sp]['z']*bn)*int2)
                elif field=='bpar':
                  int1=np.real(self.kykxs['phi'][:,:,:,ii_t]*np.conj(self.kykxs['bpar'][:,:,:,ii_t]))
                  int2=np.abs(self.kykxs['bpar'][:,:,:,ii_t])**2
                  int[:,:,:,ii_t]=-1/(bn*kperp**2)*(0.5*self.input_out['SPECIES'][ii_sp]['z']/(self.input_out['SPECIES'][ii_sp]['mass']*vthrat)
                                                       *((2*b_s-1)*i0e(b_s)-3*b_s*i1e(b_s))*int1
                                                +0.5/bn*vthrat*(0.5*(i0e(b_s)-i1e(b_s))+(4-2*b_s)*((4*b_s-3)*i0e(b_s)+(1-4*b_s)*i1e(b_s)))*int2)
                int[:,:,:,ii_t]=int[:,:,:,ii_t]*fac_mom_perp_ncf
              else:
                int=np.nan
                msg="Warning, spectrum "+kk+" not defined. Skipped"
                self.msg_log('get_kykxs_spectra',msg,self.settings['verbose'])
            self.kykxs_spectra[field][kk]['kyt'][:,:,ii_sp]=np.sum(np.sum(int,axis=1)
                                                             *np.moveaxis(np.tile(fac_kzeta[:,0,:],[len(self.kykxs['It']),1,1]),0,2),axis=1)*ints
            if time_average:
              int=np.trapz(int[:,:,:,I_trapz],t,axis=3)/(t[-1]-t[0])
            else:
              int=int[:,:,:,0]
            self.kykxs_spectra[field][kk]['kykxs'][:,:,:,ii_sp]=fac_kzeta*int
            self.kykxs_spectra[field][kk]['kykx'][:,:,ii_sp]=np.sum(self.kykxs_spectra[field][kk]['kykxs'][:,:,:,ii_sp],axis=2)*ints
            self.kykxs_spectra[field][kk]['ky'][:,ii_sp]=np.sum(self.kykxs_spectra[field][kk]['kykx'][:,:,ii_sp],axis=1)


  def get_transport_fluxes(self):
    """ Compute the physical fluxes to be used in 1D transport equations (Sugama PoP 1998 and Belli PoP 2018 defintions) 
    Outputs:
      GKWrun.fluxes_transport         the time-dependent fluxes including the contribution from cf energy and pol/mag effects 
      GKWrun.fluxes_transport_lab     same as above but in the Laboratory frame. 
    """
    if not hasattr(self,'fluxes_transport'):
      self.fluxes_transport={}
    if not hasattr(self,'fluxes_transport_lab'):
      self.fluxes_transport_lab={}
    for field in ('phi','apar','bpar'):
      if field not in self.fluxes_transport.keys():
        self.fluxes_transport[field]={}
      if field not in self.fluxes_transport_lab.keys():
        self.fluxes_transport_lab[field]={}
      vthrat=np.array([np.sqrt(sp['temp']/sp['mass']) for sp in self.input_out['SPECIES']])
      fac_mom_Q=2*self.input_out['GEOM']['signb']/vthrat*self.input_out['ROTATION']['vcor']
      if field in self.kykxs_spectra.keys():
        if 'particles' in self.kykxs_spectra[field]:
          self.fluxes_transport[field]['particles']=np.sum(self.kykxs_spectra[field]['particles']['kyt'],axis=0)
          self.fluxes_transport_lab[field]['particles']=self.fluxes_transport[field]['particles']
        if ('heat' in self.kykxs_spectra[field] and 'heat_cf' in self.kykxs_spectra[field]):
          self.fluxes_transport[field]['heat']=np.sum(self.kykxs_spectra[field]['heat']['kyt']
                                                   +self.kykxs_spectra[field]['heat_cf']['kyt'],axis=0)
          if ('momentum_parallel' in self.kykxs_spectra[field] and 'momentum_perp' in self.kykxs_spectra[field] and 'tolab_heat_part' in self.kykxs_spectra[field]):
            self.fluxes_transport_lab[field]['heat']=self.fluxes_transport[field]['heat']+np.sum(np.tile(fac_mom_Q,(len(self.kykxs['It']),1))*
                                                         (self.kykxs_spectra[field]['momentum_parallel']['kyt']
                                                            +self.kykxs_spectra[field]['momentum_perp']['kyt'])
                                                         +self.kykxs_spectra[field]['tolab_heat_part']['kyt'],axis=0)
        if field=='apar':
          if ('momentum_parallel' in self.kykxs_spectra[field] and 'momentum_parallel_polmag' in self.kykxs_spectra[field]):
            self.fluxes_transport[field]['momentum_parallel']=np.sum(self.kykxs_spectra[field]['momentum_parallel']['kyt']
                                                                    +self.kykxs_spectra[field]['momentum_parallel_polmag']['kyt'],axis=0)
          if ('momentum_perp' in self.kykxs_spectra[field]):
            self.fluxes_transport[field]['momentum_perp']=np.sum(self.kykxs_spectra[field]['momentum_perp']['kyt'],axis=0)
        else:
          if ('momentum_parallel' in self.kykxs_spectra[field]):
            self.fluxes_transport[field]['momentum_parallel']=np.sum(self.kykxs_spectra[field]['momentum_parallel']['kyt'],axis=0)
          if ('momentum_perp' in self.kykxs_spectra[field] and 'momentum_perp_polmag' in self.kykxs_spectra[field]):
            self.fluxes_transport[field]['momentum_perp']=np.sum(self.kykxs_spectra[field]['momentum_perp']['kyt']
                                                                +self.kykxs_spectra[field]['momentum_perp_polmag']['kyt'],axis=0)

        if ('momentum_parallel' in self.fluxes_transport[field]) and ('tolab_momentum_parallel' in self.kykxs_spectra[field]):
          self.fluxes_transport_lab[field]['momentum_parallel']=self.fluxes_transport[field]['momentum_parallel']+np.sum(
                                                                  self.kykxs_spectra[field]['tolab_momentum_parallel']['kyt'],axis=0)
        if ('momentum_perp' in self.fluxes_transport[field]) and ('tolab_momentum_perp' in self.kykxs_spectra[field]):
          self.fluxes_transport_lab[field]['momentum_perp']=self.fluxes_transport[field]['momentum_perp']+np.sum(
                                                                  self.kykxs_spectra[field]['tolab_momentum_perp']['kyt'],axis=0)


  def write_input(self,flnm,source='input'):
    """ Write a GKW input file from the content of GKWrun.input or GKWrun.input_out
    Inputs
      flnm       Name and destination of the input file to be created
      source     Source of the input data
                 Can be 'input' or 'input_out' use GKWrun.input or GKWrun.input_out, respectively
    """
    with open(flnm,'w') as f:
      if source=='input':
        self.input.write(f)
      elif source=='input_out':
        self.input_out.write(f)
      else:
        msg="Error: unknown input source"
        self.msg_log('write_input',msg,True)
        return

  def read(self,kykxs=True,prec='sp'):    
    """ Load available GKW input and output data
    Inputs
       kykxs    If True, includes kykxs diagnostic
     """
    self.get_inputs()
    self.get_geom()
    self.get_grids()
    self.get_kperp()
    self.get_info()
    self.get_eigenvalues()
    self.get_collisions()
    self.get_cfdens()
    self.get_fluxes()
    self.get_parallel()
    if kykxs is True:
      self.get_kykxs(prec=prec)
      self.get_kykxs_spectra()
      self.get_transport_fluxes()
    else:
      self.kykxs={}

  def get_kykxs_spectra_lowmem(self,time_interval,fields=None,time_avg=np.array([]),prec='sp'):
    """ Meta-function to compute the kykxs spectra without keeping all moments in memory. Useful for non-linear runs. 

    Inputs:
          time_interval    time interval for which the moments and fields will be loaded [t_start, t_end] (numpy.ndarray)
          fields           fields for which the intensity and fluxes spectra will be computed 
                           (string or set of strings, e.g. {'phi','apar'})
          time_avg         time interval used to average the fluxes and fields intensity (NL runs only)
                           If empty, uses all available values
    This routine calls get_kykxs and get_kykxs_spectra sequentially
    """
    if fields is None:
      fields={'phi','apar','bpar'}
    if type(fields)==str:
      fields={fields}

    # list of fluxes and corresponding moments to load 
    fluxes={'phi':{'particles': ('dens_J0',),'heat': ('Tpar_J0','Tperp_J0'),'momentum_parallel': ('vpar_J0',),
                       'momentum_perp': ('Tperp_J1',),'momentum_perp_polmag': ('bpar',)},
            'apar':{'particles': ('vpar_J0',),'heat': ('Qpar_J0',),'momentum_parallel': ('Tpar_J0',),
                       'momentum_parallel_polmag': ('phi','bpar'),'momentum_perp': ('M12_J1',)},
            'bpar':{'particles': ('Tperp_J1',),'heat': ('M24_J1',),'momentum_parallel': ('M12_J1',),
                       'momentum_perp': ('Tperp_J1','Tperp_J0'),'momentum_perp_polmag': ('phi',)}
            }

    # load the required moments, compute spectra and discard the moments
    for field in fields:
      self.get_kykxs(field,time_interval,prec=prec)
      if np.size(self.kykxs[field])>0:
        self.get_kykxs_spectra({field:('intensity',)},time_avg=time_avg)
        print(field+" intensity spectrum computed")
        for flux in fluxes[field].keys():
          self.get_kykxs(fluxes[field][flux],time_interval,prec=prec)
          self.get_kykxs_spectra({field:(flux,)},time_avg=time_avg)
          print(field+" "+flux+" flux spectrum computed")
          if type(fluxes[field][flux]) is tuple:
            for key in fluxes[field][flux]:
              del self.kykxs[key]
          else:
            del self.kykxs[fluxes[field][flux]]
      del self.kykxs[field]


  def __init__(self):
    self.log={}
    self.settings={}
    self.settings['verbose']=True
    self.__It_clean=np.nan # internal variable to clean up time base
    self.traceback={}
    path=os.path.dirname(inspect.getfile(self.__class__))
    self.traceback['git_commit']=subprocess.check_output(['git','-C',path,'rev-parse','HEAD']).decode('ascii').strip()
    self.traceback['git_url']=subprocess.check_output(['git','-C',path,'config','--get','remote.origin.url']).decode('ascii').strip()
    
