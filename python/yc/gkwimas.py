### required modules
import numpy as np
import re
import f90nml
import xmltodict
from dicttoxml import dicttoxml
from scipy.interpolate import CubicSpline
import scipy.constants as codata
import copy
import inspect
import subprocess
import datetime

use_imaspy=False
try:
  import imaspy
  use_imaspy=True
  print("Using IMASPy")
except:
  import idspy_toolkit as idspy
  from idspy_dictionaries import ids_gyrokinetics_local
  print("Using IDSpy")

#### deal with dependencies later 
import gkwrun as gk
import os
os.chdir('/home/yann/projects/imasgk_git/gkids/tools/')
import FS_param as FS 

# This module contains functions to deal with IMAS-GKW conversion: ids2gkw, gkw2ids

def ids2gkw(gkids,runtype,params_source='ids',gkwref=None):
  """ Convert an IMAS 'gyrokinetics' IDS to GKW inputs
  Inputs
    gkids            Instance of the idspy GKIDS class containing the reference 'gyrokinetics' IDS
    runtype          'linear' or 'non-linear'
    params_source    Origin of the GKW specific parameters. Allowed values are:
                       'ids': taken from code.parameters fields of the IDS
                       'gkwref': given from the input part of a GKWrun class instance
    gkwref           Instance of the GKWrun class containing the reference input data in gkwref.input

  Outputs
    gkwscan          Instance of the GKWscan class gathering all the GKW inputs generated from the IDS
  """
  nsp=len(gkids.species)
  rho_rat=np.nan  
  
  # Inner function to fill all the inputs independent of the wavevector
  def fill_static_inputs(): 
    nonlocal rho_rat # to modify the value of rho_rat outside of fill_static_inputs
    
    ## enforced parameter choices to ease the IMAS/GKW conversion
    gkwrun.input['GEOM']['geom_type']='mxh'
    gkwrun.input['GEOM']['gradp_type']='beta_prime'
    gkwrun.input['SPCGENERAL']['beta_type']='ref'
    gkwrun.input['SPCGENERAL']['betaprime_type']='ref'

    gkwrun.input['COLLISIONS']['freq_input']=True
    gkwrun.input['ROTATION']['cf_upphi']=True
    gkwrun.input['ROTATION']['cf_upsrc']=True

    ## model
    gkwrun.input['CONTROL']['nlapar']=bool(gkids.model.include_a_field_parallel)
    gkwrun.input['CONTROL']['nlbpar']=bool(gkids.model.include_b_field_parallel)   
    gkwrun.input['ROTATION']['coriolis']=bool(gkids.model.include_coriolis_drift)
    if gkids.model.include_centrifugal_effects==1:
      gkwrun.input['ROTATION']['cf_drift']=True
      gkwrun.input['ROTATION']['cf_trap']=True
    else:
      gkwrun.input['ROTATION']['cf_drift']=False
      gkwrun.input['ROTATION']['cf_trap']=False
    gkwrun.input['SPCGENERAL']['adiabatic_electrons']=bool(gkids.model.adiabatic_electrons)
    if gkids.model.use_mhd_approximation==True:
      gkwrun.input['SPCGENERAL']['drift_gradp_type']='curv_only'
    else:
      gkwrun.input['SPCGENERAL']['drift_gradp_type']='full_drift'
 
    ## find main ion species
    # defined as the species with positive charge and the largest density
    dum=0.0
    for ii,sp in enumerate(gkids.species):
      if (sp.charge_norm>0) and (sp.density_norm>dum):
        dum=sp.density_norm
        Iionmain=ii
    ## find electron species
    dum=[sp.charge_norm<0 for sp in gkids.species]
    assert (dum.count(True)==1), \
      "Runs with multiple or no electron species not handled yet"
    Iele=dum.index(True) # index of the electron species

    ## ratio of reference quantities GKW/IMAS
    # GKW ref quantities chosen to ease the conversion process
    q_rat = 1
    m_rat = 1
    T_rat = gkids.species[Iionmain].temperature_norm
    n_rat = 1
    R_rat = 1
    B_rat = 1
    vth_rat = np.sqrt(T_rat/m_rat)
    rho_rat = m_rat*vth_rat/(q_rat*B_rat)

    ## flux surface
    gkwrun.input['GEOM']['eps']=gkids.flux_surface.r_minor_norm/R_rat
    gkwrun.input['GEOM']['signb']=-gkids.flux_surface.b_field_phi_sign
    gkwrun.input['GEOM']['signj']=-gkids.flux_surface.ip_sign
    gkwrun.input['GEOM']['q']=gkids.flux_surface.q*gkids.flux_surface.b_field_phi_sign*gkids.flux_surface.ip_sign
    gkwrun.input['GEOM']['shat']=float(gkids.flux_surface.magnetic_shear_r_minor)
    gkwrun.input['GEOM']['dRmil']=float(gkids.flux_surface.dgeometric_axis_r_dr_minor)
    gkwrun.input['GEOM']['dZmil']=float(gkids.flux_surface.dgeometric_axis_z_dr_minor)
    gkwrun.input['GEOM']['kappa']=float(gkids.flux_surface.elongation)
    gkwrun.input['GEOM']['skappa']=gkids.flux_surface.r_minor_norm/gkids.flux_surface.elongation*gkids.flux_surface.delongation_dr_minor_norm
    gkwrun.input['GEOM']['n_shape']=len(list(-gkids.flux_surface.shape_coefficients_c))
    gkwrun.input['GEOM']['c']=list(-gkids.flux_surface.shape_coefficients_c)
    gkwrun.input['GEOM']['s']=list(gkids.flux_surface.shape_coefficients_s)
    gkwrun.input['GEOM']['c_prime']=list(-gkids.flux_surface.dc_dr_minor_norm*R_rat)
    gkwrun.input['GEOM']['s_prime']=list(gkids.flux_surface.ds_dr_minor_norm*R_rat)

    gkwrun.input['SPCGENERAL']['betaprime_ref']=-gkids.flux_surface.pressure_gradient_norm*R_rat/B_rat**2
      
    ## species (GKW default: main ion, electron and then other species)
    if gkids.model.adiabatic_electrons==1:
      gkwrun.input['GRIDSIZE']['number_of_species']=nsp-1
    else:
      gkwrun.input['GRIDSIZE']['number_of_species']=nsp
    # empty species namelist cogroup
    for ii in range(len(gkwrun.input['SPECIES'])):
      gkwrun.input['SPECIES'].__delitem__(0) 

    # fill the new species namelist 
    for sp in gkids.species:
      sp_nml=f90nml.Parser().reads('&sp_ii'+
                                   ' mass='+str(sp.mass_norm/m_rat)+
                                   ' z='+str(sp.charge_norm/q_rat)+
                                   ' temp='+str(sp.temperature_norm/T_rat)+
                                   ' dens='+str(sp.density_norm/n_rat)+
                                   ' rlt='+str(sp.temperature_log_gradient_norm/R_rat)+
                                   ' rln='+str(sp.density_log_gradient_norm/R_rat)+
                                   ' uprim='+str(sp.velocity_phi_gradient_norm*gkids.flux_surface.b_field_phi_sign*R_rat**2/vth_rat)+
                                   ' /')
      gkwrun.input.add_cogroup('SPECIES',sp_nml['sp_ii'])
      
    ## species_all
    gkwrun.input['SPCGENERAL']['beta_ref']=gkids.species_all.beta_reference*n_rat*T_rat/B_rat**2
    gkwrun.input['ROTATION']['vcor']=gkids.species_all.velocity_phi_norm*gkids.flux_surface.b_field_phi_sign*R_rat/vth_rat
    gkwrun.input['ROTATION']['shear_rate']=gkids.species_all.shearing_rate_norm*R_rat/(vth_rat*B_rat)
    
    ## collisions
    gkwrun.input['CONTROL']['collisions']=True
    gkwrun.input['COLLISIONS']['pitch_angle']=True
    gkwrun.input['COLLISIONS']['en_scatter']= (not gkids.model.collisions_pitch_only)
    gkwrun.input['COLLISIONS']['friction_coll']= (not gkids.model.collisions_pitch_only)
    gkwrun.input['COLLISIONS']['mom_conservation']=bool(gkids.model.collisions_momentum_conservation)
    gkwrun.input['COLLISIONS']['ene_conservation']=bool(gkids.model.collisions_energy_conservation)
    gkwrun.input['COLLISIONS']['nu_ab']=[]
    for jj in range(nsp):  # species b
      for ii,sp in enumerate(gkids.species): # species a
        gkwrun.input['COLLISIONS']['nu_ab'].append(gkids.collisions.collisionality_norm[ii,jj]*R_rat*np.sqrt(sp.mass_norm/sp.temperature_norm))
    if np.all(np.asarray(gkwrun.input['COLLISIONS']['nu_ab'])==0):
      # turn off collisions
      gkwrun.input['CONTROL']['collisions']=False
      gkwrun.input['COLLISIONS']['pitch_angle']=False
      gkwrun.input['COLLISIONS']['en_scatter']=False
      gkwrun.input['COLLISIONS']['friction_coll']=False

  ## end of fill_static_inputs inner function
  ## 
  def build_dict_with_datatypes(xmldict):
    """ Transform a dict obtained with xmltodict.parse in which all data are strings
        into a 'normal' dict with various data types (int, bool, str, float)
    """
    if isinstance(xmldict,dict):
      if '@type' in xmldict.keys():
        if xmldict['@type']=='str':
          if '#text' in xmldict.keys():
            dict_params=xmldict['#text']
          else:
            dict_params=''
        elif xmldict['@type']=='bool':
          if xmldict['#text']=='true' or xmldict['#text']=='True':
            dict_params=True
          else:
            dict_params=False
        elif xmldict['@type']=='int':
          dict_params=int(xmldict['#text'])
        elif xmldict['@type']=='float':
          dict_params=float(xmldict['#text'])
        elif xmldict['@type'] in ['number','complex']:
          dict_params=complex(xmldict['#text'])
        elif xmldict['@type']=='list':
          dict_params=[build_dict_with_datatypes(xx) for xx in xmldict['item']] 
        elif xmldict['@type']=='dict':
          dict_params={} 
          for k in xmldict.keys():
            if not type(xmldict[k])==str:
              dict_params[k]=build_dict_with_datatypes(xmldict[k])
        else:
          print("Unrecognised data type for xmldict['@type']: type(xmldict['@type'])="+str(type(xmldict['@type'])))
      else:
       dict_params={} 
       for k in xmldict.keys():
          dict_params[k]=build_dict_with_datatypes(xmldict[k])
    elif isinstance(xmldict,list):
       dict_params=[build_dict_with_datatypes(xx) for xx in xmldict]
    else:
      print('Unrecognised data type for xmldict: type(xmldict)='+str(type(xmldict)))
    return dict_params
  ## end of build_dict_with_datatypes inner function

  assert (runtype in ('linear','non-linear')), \
         "'linear' and 'non-linear' are the only accepted values for runtype"
  assert (params_source in ('ids','gkwref')), \
         "'ids' and 'gkwref' are the only accepted values for params_source"
  
  if params_source=='ids':
    if gkids.code.name.upper()!='GKW':
      print("Error: the reference IDS does not originate from GKW.")
      print("Need to provide a reference GKW input to fill code specific parameters")
      return      
  elif params_source=='gkwref':
    if (gkwref is None) or (not isinstance(gkwref,gk.GKWrun)):
      print("Error: gkwref is missing or not an instance of the GKWrun class")
      return      

  if runtype=='non-linear': # build a non-linear GKW input
    if params_source=='ids':
      if type(gkids.non_linear.code.parameters) is dict:
        input_dict=gkids.non_linear.code.parameters
      else:
        xml_params=gkids.non_linear.code.parameters
        dum=xmltodict.parse(xml_params,xml_attribs=True)
        input_dict=build_dict_with_datatypes(dum['root'])
      gkwrun=gk.GKWrun()
      gkwrun.input=f90nml.Namelist(input_dict)
    else:
      gkwrun=copy.deepcopy(gkwref)
    # check ref input is non-linear
    if gkwrun.input['CONTROL']['non_linear']!=True:
      print("Error: the reference GKW input file does not match runtype='non-linear'")
      return
    gkwrun.input['MODE']['mode_box']=True

    fill_static_inputs()
    kx=gkids.non_linear.radial_wavevector_norm
    ky=gkids.non_linear.binormal_wavevector_norm
    # check the grids? sorted, ky=0 included, symmetry in kx and non-duplicated values?
    gkwrun.input['GRIDSIZE']['NMOD']=len(ky)
    gkwrun.input['MODE']['krhomax']=np.max(ky)*rho_rat
    gkwrun.input['GRIDSIZE']['NX']=len(kx)      
    print("Conversion of NL runs not finalised yet")
    pass
    # need to compute kthnorm and krnorm to set up ikxspace (to match kx_min of the IDS)
    # return gkwrun (in a gkwscan structure?)
  else: # linear run
    nwav=len(gkids.linear.wavevector)
    for ii_wav in range(nwav): 
      neiv=len(gkids.linear.wavevector[ii_wav].eigenmode)
      if neiv==0 and params_source=='gkwref': # special case where only inputs are filled
        gkwrun=copy.deepcopy(gkwref)
        gkwrun.input['MODE']['kr_type']='kr'
        fill_static_inputs()
        gkwrun.input['MODE']['kthrho']=gkids.linear.wavevector[ii_wav].binormal_wavevector_norm*rho_rat
        gkwrun.input['MODE']['krrho']=gkids.linear.wavevector[ii_wav].radial_wavevector_norm*rho_rat 
      for ii_eiv in range(neiv):
        if params_source=='ids':
          if type(gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].code.parameters) is dict:
            input_dict=gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].code.parameters
          else:
            if use_imaspy:
              xml_params=gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].code.parameters.value
            else:
              xml_params=gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].code.parameters
            dum=xmltodict.parse(xml_params,xml_attribs=True)
            input_dict=build_dict_with_datatypes(dum['root'])
          gkwrun=gk.GKWrun()
          gkwrun.input=f90nml.Namelist(input_dict)
          for k in gkwrun.input.keys(): # to make species a cogroup
             if isinstance(gkwrun.input[k],list):
               tmp_k=gkwrun.input.pop(k)
               gkwrun.input.create_cogroup(k)
               for kk in tmp_k:
                 gkwrun.input.add_cogroup(k,kk)

        else:
         gkwrun=copy.deepcopy(gkwref)
        gkwrun.input['MODE']['mode_box']=False
        gkwrun.input['MODE']['kr_type']='kr'
       # check ref input matches initial/eiv
        if ((gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].initial_value_run==True and gkwrun.input['CONTROL']['method'].upper()!='EXP')
            or (gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].initial_value_run==False and gkwrun.input['CONTROL']['method'].upper()!='EIV')):
          print("Mismatch between the type of run (initial value or eigenvalue) in the IDS and in the reference GKW input file")
          return
        fill_static_inputs()
        npol=gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].poloidal_turns
        if npol % 2 == 0:
          print("Error: reference IDS has an even number of poloidal turns. This is incompatible with GKW")
          return
        else:         
          gkwrun.input['GRIDSIZE']['nperiod']=int((npol+1)/2)
        gkwrun.input['GRIDSIZE']['n_s_grid']=gkids.linear.wavevector[ii_wav].eigenmode[ii_eiv].angle_pol.size
        gkwrun.input['MODE']['kthrho']=gkids.linear.wavevector[ii_wav].binormal_wavevector_norm*rho_rat
        gkwrun.input['MODE']['krrho']=gkids.linear.wavevector[ii_wav].radial_wavevector_norm*rho_rat

        # add this gkwrun in gkwscan
  #return gkwscan     
  return gkwrun


def gkw2ids(gkwrun,time_interval=np.array([]),Nsh=10,provider=''):
  """ Convert GKW inputs and outputs to an IMAS 'gyrokinetics' IDS created with idspy
  Inputs
    gkwrun           Instance of the GKWrun class containing GKW input and output data
    time_interval    Time interval used for the outputs (fluxes + eigenmodes for NL, eigenmodes only for L initial value)  
                       If empty:
                         NL run: all available times
                         linear run: last time step only
                       If dim 2:
                         NL run: time selection (time averaged fluxes + time dependent eigenmodes)
                         linear run, initial value: time selection (time dependent eigenmodes only) 
                       Not used for linear eigenvalue runs
    Nsh              Number of moments for flux surface Fourier parametrisation (default Nsh=10)
    provider         Person who ran this GKW simulation
  Outputs
    gkids           Instance of the GKIDS class containing the generated 'gyrokinetics' IDS

  Warning and error logs are stored in gkwrun.log
  """
  def nml_to_dict(nml):
    " Recursive function to convert a f90nml namelist object into a standard dict" 
    if type(nml) is f90nml.namelist.Namelist: 
      dd=dict(nml)
      for kk in nml.keys():
         dd[kk]=nml_to_dict(nml[kk])
      return dd    
    else:
      return nml

  time_interval=np.asarray(time_interval)

  # initialise an empty GK IDS structure
  if use_imaspy:
    ids_factory=imaspy.IDSFactory("4.0.0")
    gkids=ids_factory.gyrokinetics_local()
  else:
    gkids=ids_gyrokinetics_local.GyrokineticsLocal()
    idspy.fill_default_values_ids(gkids)

  ## ids_properties
  gkids.ids_properties.provider=provider
  date=datetime.datetime.now()
  gkids.ids_properties.creation_date=date.strftime('%Y-%m-%dT%H:%M:%SZ')
  gkids.ids_properties.homogeneous_time=2

  ## constants (IMAS follows CODATA from NIST)
  me=codata.physical_constants['electron mass'][0] # electron mass [kg] 
  mD=codata.physical_constants['deuteron mass'][0] # deuterium mass [kg] 
  eV=codata.physical_constants['elementary charge'][0] # elementary charge [C] / electron volt [J]

  ## code 
  gkids.code.name="GKW"
  if hasattr(gkwrun,'info'):
    gkids.code.commit=gkwrun.info['commit']
  gkids.code.repository="https://bitbucket.org/gkw/gkw/src/develop/"

  if use_imaspy:
    dum=gkids.code.library._element_structure
  else:
    dum=ids_gyrokinetics_local.Library()
  dum.name=gkwrun.__module__
  dum.description="Python module to load the raw GKW data into a GKWrun class"
  dum.commit=gkwrun.traceback['git_commit']
  dum.repository=gkwrun.traceback['git_url']
  gkids.code.library.append(dum)

  if use_imaspy:
    dum=gkids.code.library._element_structure
  else:
    dum=ids_gyrokinetics_local.Library()
  path=os.path.dirname(inspect.getfile(gkw2ids))
  dum.name=gkw2ids.__module__+'.gkw2ids'
  dum.description="Python function to map GKW data from a GKWrun class into a 'gyrokinetics_local' IDS"
  dum.commit=subprocess.check_output(['git','-C',path,'rev-parse','HEAD']).decode('ascii').strip()
  dum.repository=subprocess.check_output(['git','-C',path,'config','--get','remote.origin.url']).decode('ascii').strip()
  gkids.code.library.append(dum)

  ## model
  gkids.model.adiabatic_electrons=int(gkwrun.input_out['SPCGENERAL']['adiabatic_electrons'])
  gkids.model.include_a_field_parallel=int(gkwrun.input_out['CONTROL']['nlapar'])
  gkids.model.include_b_field_parallel=int(gkwrun.input_out['CONTROL']['nlbpar'])

  gkids.model.include_coriolis_drift=int(gkwrun.input_out['ROTATION']['coriolis'])

  if gkwrun.input_out['ROTATION']['cf_drift'] or gkwrun.input_out['ROTATION']['cf_trap']:
    if (gkwrun.input_out['ROTATION']['cf_drift'] and gkwrun.input_out['ROTATION']['cf_trap']
        and gkwrun.input_out['ROTATION']['cf_upphi'] and gkwrun.input_out['ROTATION']['cf_upsrc']):
      gkids.model.include_centrifugal_effects=int(True)
      if gkwrun.input_out['GEOM']['r0_loc']!='LFS':
        msg="Critical: on axis specification of density and density gradient not implemented (R0_LOC='LFS' required)."
        gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
    else:
      msg="Critical: inconsistent CF switches: all or none need to be ON for the GK IDS"
      gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
      return
  else:
    gkids.model.include_centrifugal_effects=int(False)

  gkids.model.collisions_pitch_only = int(gkwrun.input_out['COLLISIONS']['pitch_angle'] 
    and not gkwrun.input_out['COLLISIONS']['en_scatter'] and not gkwrun.input_out['COLLISIONS']['friction_coll'])
  gkids.model.collisions_momentum_conservation = int(gkwrun.input_out['COLLISIONS']['mom_conservation'])
  gkids.model.collisions_energy_conservation = int(gkwrun.input_out['COLLISIONS']['ene_conservation'])
  gkids.model.collisions_finite_larmor_radius=int(False)

  if ('drift_gradp_type' in gkwrun.input_out['SPCGENERAL']) and gkwrun.input_out['SPCGENERAL']['drift_gradp_type']=='curv_only':
    gkids.model.use_mhd_approximation=int(True)
  else:
    gkids.model.use_mhd_approximation=int(False)

  ## flux_surface
  assert (gkwrun.input_out['GEOM']['geom_type'] in ('miller','fourier','mxh','chease')), \
         " 'miller', 'fourier', 'mxh', 'chease' are the only supported equilibrium descriptions for the GK IDS"

  if (gkwrun.input_out['GEOM']['geom_type']=='miller' or 
      gkwrun.input_out['GEOM']['geom_type']=='mxh' or
      gkwrun.input_out['GEOM']['geom_type']=='fourier'):

    if gkwrun.input_out['GEOM']['gradp_type']=='beta_prime':
      if gkwrun.input_out['SPCGENERAL']['betaprime_type']=='ref':
        betapr_gkw=gkwrun.input_out['SPCGENERAL']['betaprime_ref']
      else:
        msg="With 'miller', 'mxh' or 'fourier' parametrisation, betaprime_type='ref' is the only supported option (could be extended)"
        # could be extended to support betaprime_type='sp' if needed 
        gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
        return
    else:
      msg="With 'miller', 'mxh' or 'fourier' parametrisation, gradp_type='beta_prime' is the only supported option (could be extended)"
      gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
      return

    if gkwrun.input_out['GEOM']['geom_type']=='fourier':
      # First compute R/Rref_GKW and Z/Rref_GKW from the Fourier parametrisation
      c=gkwrun.input_out['GEOM']['c'][0:Nsh]
      s=gkwrun.input_out['GEOM']['s'][0:Nsh]
      dcdr=gkwrun.input_out['GEOM']['c_prime'][0:Nsh]
      dsdr=gkwrun.input_out['GEOM']['s_prime'][0:Nsh]
      rdum,Rdum,Zdum=FS.fourier2rz(c,s,dcdr,dsdr,'gkw')
      # and then compute the MXH parametrisation for the IDS
      r0,R0,dR0dr,Z0,dZ0dr,k,dkdr,cc,dccdr,ss,dssdr,R_out,Z_out,err_out=FS.rz2mxh(Rdum,Zdum,'imas',rdum[1],Nsh,doplots=False)

    if gkwrun.input_out['GEOM']['geom_type']=='miller':
      # First compute R/Rref_GKW and Z/Rref_GKW from the Miller parametrisation
      r0=gkwrun.input_out['GEOM']['eps']
      Rmil=1.0
      Zmil=gkwrun.input_out['GEOM']['zmil']
      dRmildr=gkwrun.input_out['GEOM']['drmil']
      dZmildr=gkwrun.input_out['GEOM']['dzmil']
      k=gkwrun.input_out['GEOM']['kappa']
      d=gkwrun.input_out['GEOM']['delta']
      z=gkwrun.input_out['GEOM']['square']
      sk=gkwrun.input_out['GEOM']['skappa']
      sd=gkwrun.input_out['GEOM']['sdelta']
      sz=gkwrun.input_out['GEOM']['ssquare']
      rdum,Rdum,Zdum=FS.miller2rz(r0,Rmil,Zmil,k,d,z,dRmildr,dZmildr,sk,sd,sz)
      # and then compute the MXH parametrisation for the IDS
      r0,R0,dR0dr,Z0,dZ0dr,k,dkdr,cc,dccdr,ss,dssdr,R_out,Z_out,err_out=FS.rz2mxh(Rdum,Zdum,'imas',rdum[1],Nsh,doplots=False)

    if gkwrun.input_out['GEOM']['geom_type']=='mxh':
      # Directly get the IMAS MHX parametrisation from GKW
      r0=gkwrun.input_out['GEOM']['eps']
      R0=1.0
      Z0=0.0
      dR0dr=gkwrun.input_out['GEOM']['drmil']
      dZ0dr=gkwrun.input_out['GEOM']['dzmil']
      k=gkwrun.input_out['GEOM']['kappa']
      sk=gkwrun.input_out['GEOM']['skappa'] # r/k*dkdr
      dkdr=sk*k/(r0*R0)
      cc=-np.array(gkwrun.input_out['GEOM']['c'][0:Nsh]) # opposite sign of theta in GKW
      ss=np.array(gkwrun.input_out['GEOM']['s'][0:Nsh])
      dccdr=-np.array(gkwrun.input_out['GEOM']['c_prime'][0:Nsh]) # opposite sign of theta in GKW
      dssdr=np.array(gkwrun.input_out['GEOM']['s_prime'][0:Nsh])

    R_rat=1/R0   # Rref_GKW/Rref_GKDD
    B_rat=1   # Bref_GKW/Bref_GKDD

  elif gkwrun.input_out['GEOM']['geom_type']=='chease':
    betapr_gkw=gkwrun.geom['betaprime_eq']
    # need to read hamada file, and get R,Z from it
    msg="Critical: treatment of chease equilibrium not yet implemented"
    gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
    return

  sb_gkw=gkwrun.input_out['GEOM']['signb']
  sj_gkw=gkwrun.input_out['GEOM']['signj']
  gkids.flux_surface.r_minor_norm=gkwrun.geom['eps']*R_rat
  gkids.flux_surface.q=(-sb_gkw)*(-sj_gkw)*gkwrun.geom['q']
  gkids.flux_surface.magnetic_shear_r_minor=gkwrun.geom['shat']
  gkids.flux_surface.pressure_gradient_norm=-betapr_gkw*B_rat**2/R_rat
  gkids.flux_surface.b_field_phi_sign=-sb_gkw
  gkids.flux_surface.ip_sign=-sj_gkw
  gkids.flux_surface.dgeometric_axis_r_dr_minor=float(dR0dr)
  gkids.flux_surface.dgeometric_axis_z_dr_minor=float(dZ0dr)
  gkids.flux_surface.elongation=float(k)
  gkids.flux_surface.delongation_dr_minor_norm=float(dkdr)/R_rat
  gkids.flux_surface.shape_coefficients_c=cc
  gkids.flux_surface.shape_coefficients_s=ss
  gkids.flux_surface.dc_dr_minor_norm=dccdr/R_rat
  gkids.flux_surface.ds_dr_minor_norm=dssdr/R_rat

  # compute theta_imas=f(s_gkw)
  R_dum = gkwrun.geom['R']
  Z_dum = gkwrun.geom['Z']
  th = np.arctan2(-(Z_dum-Z0),R_dum-R0)
  dum=np.cumsum(np.concatenate(([False],np.diff(th)>np.pi)))
  th_imas_of_s_grid=th-2*np.pi*dum+2*np.pi*dum[int(gkwrun.ns/2)]

  # define theta_imas grid for species_all
  th_imas_sp_all=np.linspace(-np.pi,np.pi,num=gkwrun.ns_per_turn+1,endpoint=True)

  ## compute n_cf=n_s/n_s(s=0) and n_cf_th0=n_s(theta=0)/n_s(s=0)
  n_cf_th0=np.ones(gkwrun.nsp)
  n_cf=np.ones([gkwrun.ns_per_turn+1,gkwrun.nsp])
  dlog_n_cf_ds_th0=np.zeros(gkwrun.nsp)
  dlog_n_cf_ds=np.ones([gkwrun.ns_per_turn+1,gkwrun.nsp])
  if gkids.model.include_centrifugal_effects==int(True):
    for ii in range(gkwrun.nsp):
      f=CubicSpline(th_imas_of_s_grid[::-1],gkwrun.cfdens['n_pol'][::-1,ii],bc_type='natural')
      n_cf_th0[ii]=f(0)
      n_cf[:,ii]=f(th_imas_sp_all)
      f=CubicSpline(gkwrun.grids['s'],gkwrun.cfdens['n_pol'][:,ii],bc_type='natural')
      dum=f(gkwrun.grids['s'],nu=1)/gkwrun.cfdens['n_pol'][:,ii]
      f=CubicSpline(th_imas_of_s_grid[::-1],dum[::-1],bc_type='natural')
      dlog_n_cf_ds_th0[ii]=f(0)
      dlog_n_cf_ds[:,ii]=f(th_imas_sp_all)
      
  ## metric coefficients and sorted poloidal angle grid
  f=CubicSpline(th_imas_of_s_grid[::-1],gkwrun.geom['g_eps_eps'][::-1],bc_type='natural')
  gxx_th0=f(0)
  gxx=f(th_imas_sp_all)
  f=CubicSpline(th_imas_of_s_grid[::-1],gkwrun.geom['g_eps_s'][::-1],bc_type='natural')
  gxz_th0=f(0)
  gxz=f(th_imas_sp_all)
  f=CubicSpline(th_imas_of_s_grid[::-1],gkwrun.geom['g_zeta_zeta'][::-1],bc_type='natural')
  gyy_th0=f(0)
  Ith_sorted=np.argsort(th_imas_of_s_grid)
  th_sorted=th_imas_of_s_grid[Ith_sorted]

  ## find electron species
  assert not(gkwrun.input_out['SPCGENERAL']['adiabatic_electrons']), \
         "Adiabatic electrons not allowed in a GK IDS"
  dum=[sp['z']<0 for sp in gkwrun.input_out['SPECIES']]
  assert (dum.count(True)==1), \
         "Runs with multiple or no electron species not handled yet"
  Iele=dum.index(True) # index of the electron species

  ## compute ratio of reference quantities for GKW to IMAS conversion
  # reference charge ratio, qref_GKW/qref_IMAS    
  q_rat = -1/gkwrun.input_out['SPECIES'][Iele]['z'] 
  # reference mass ratio, mref_GKW/mref_IMAS 
  m_rat = me/mD*1/gkwrun.input_out['SPECIES'][Iele]['mass']
  # reference temperature ratio, Tref_GKW/Tref_IMAS
  T_rat = 1/gkwrun.input_out['SPECIES'][Iele]['temp']
  # reference density ratio, nref_GKW/nref_IMAS - ok
  n_rat = 1/gkwrun.input_out['SPECIES'][Iele]['dens']/n_cf_th0[Iele]
  # additional reference ratio
  vth_rat = np.sqrt(T_rat/m_rat)
  rho_rat = m_rat*vth_rat/(q_rat*B_rat)

  ## species
  qN_gkw = np.array([sp['z'] for sp in gkwrun.input_out['SPECIES']])
  TN_gkw = np.array([sp['temp'] for sp in gkwrun.input_out['SPECIES']])
  mN_gkw = np.array([sp['mass'] for sp in gkwrun.input_out['SPECIES']])
  rln_gkw = np.array([sp['rln'] for sp in gkwrun.input_out['SPECIES']])
  rlt_gkw = np.array([sp['rlt'] for sp in gkwrun.input_out['SPECIES']])
  vthN_gkw = np.sqrt(TN_gkw/mN_gkw)

  rln_th0_gkw=rln_gkw
  cf_ene_gkw = np.zeros([gkwrun.ns_per_turn+1,gkwrun.nsp])
  d_cf_ene_dr_gkw = np.zeros([gkwrun.ns_per_turn+1,gkwrun.nsp])
  d_cf_ene_ds_gkw = np.zeros([gkwrun.ns_per_turn+1,gkwrun.nsp])
  d_cf_ene_1 = np.zeros([gkwrun.ns_per_turn+1,gkwrun.nsp])
  d_cf_ene_2 = np.zeros(gkwrun.nsp)
  if gkids.model.include_centrifugal_effects==int(True):
    for ii in range(gkwrun.nsp):
      f=CubicSpline(th_imas_of_s_grid[::-1],gkwrun.cfdens['rln_pol'][::-1,ii],bc_type='natural')
      rln_th0_gkw[ii]=f(0)
      cf_ene_gkw[:,ii]=-TN_gkw[ii]*np.log(n_cf[:,ii])
      d_cf_ene_dr_gkw[:,ii]=(f(th_imas_sp_all)-rln_gkw[ii])*TN_gkw[ii]-cf_ene_gkw[:,ii]*rlt_gkw[ii]
      d_cf_ene_ds_gkw[:,ii]=-TN_gkw[ii]*dlog_n_cf_ds[:,ii]
      
      d_cf_ene_1[:,ii]=d_cf_ene_dr_gkw[:,ii]+d_cf_ene_ds_gkw[:,ii]*gxz/gxx
      d_cf_ene_2[ii]=-rlt_gkw[ii]*TN_gkw[ii]*np.log(n_cf_th0[ii])

  for ii in range(gkwrun.nsp):
    if use_imaspy:
      dum=gkids.species._element_structure
    else:
      dum=ids_gyrokinetics_local.Species()
    dum.charge_norm=gkwrun.input_out['SPECIES'][ii]['z']*q_rat
    dum.mass_norm=gkwrun.input_out['SPECIES'][ii]['mass']*m_rat
    dum.density_norm=gkwrun.input_out['SPECIES'][ii]['dens']*n_rat*n_cf_th0[ii]
    dum.potential_energy_norm=(cf_ene_gkw[:,ii]+TN_gkw[ii]*np.log(n_cf_th0[ii]))*T_rat
    dum.density_log_gradient_norm=(rln_th0_gkw[ii]-dlog_n_cf_ds_th0[ii]*gxz_th0/gxx_th0)/R_rat
    dum.potential_energy_gradient_norm=(d_cf_ene_1[:,ii]+d_cf_ene_2[ii]+rln_gkw[ii]*TN_gkw[ii])*T_rat/R_rat-TN_gkw[ii]*dum.density_log_gradient_norm*T_rat
    dum.temperature_norm=gkwrun.input_out['SPECIES'][ii]['temp']*T_rat
    dum.temperature_log_gradient_norm=gkwrun.input_out['SPECIES'][ii]['rlt']/R_rat
    dum.velocity_phi_gradient_norm=-sb_gkw*gkwrun.input_out['SPECIES'][ii]['uprim']*vth_rat/R_rat**2
    gkids.species.append(dum)

  ## species_all
  gkids.species_all.angle_pol_equilibrium=th_imas_sp_all  
  gkids.species_all.debye_length_norm=0.0 # not taken into account in GKW so far

  gkids.species_all.beta_reference=0.0
  if gkwrun.input_out['CONTROL']['nlapar']==True | gkwrun.input_out['CONTROL']['nlbpar']==True: # beta value
    if gkwrun.input_out['SPCGENERAL']['beta_type']=='ref':
      gkids.species_all.beta_reference=gkwrun.input_out['SPCGENERAL']['beta_ref']*B_rat**2/(n_rat*T_rat)
    elif gkwrun.input_out['SPCGENERAL']['beta_type']=='eq':
      dum=np.sum([x['dens']*x['temp'] for x in gkwrun.input_out['SPECIES']])
      gkids.species_all.beta_reference=gkwrun.geom['beta_eq']/dum*B_rat**2/(n_rat*T_rat)
    else:
      msg="Critical: unknown beta_type option in SPCGENERAL namelist"
      gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
      return
  gkids.species_all.velocity_phi_norm=-sb_gkw*gkwrun.input_out['ROTATION']['vcor']*vth_rat/R_rat

  ## collisions
  gkids.collisions.collisionality_norm=np.full((gkwrun.nsp,gkwrun.nsp),0.0)
  is_with_collisions= gkwrun.input_out['CONTROL']['collisions'] and (gkwrun.input_out['COLLISIONS']['pitch_angle'] 
                        or gkwrun.input_out['COLLISIONS']['en_scatter'] or gkwrun.input_out['COLLISIONS']['friction_coll'])
  if gkwrun.collisions.size>0 and is_with_collisions:
    for ii in range(gkwrun.nsp):
      gkids.collisions.collisionality_norm[ii,:]=(gkwrun.collisions[ii,:]
                                                  *vth_rat/R_rat
                                                  *np.sqrt(TN_gkw[ii]/mN_gkw[ii])
                                                  *n_cf_th0)

  ## linear 
  if not gkwrun.input_out['CONTROL']['non_linear']: 
    if use_imaspy==False:
      gkids.linear=ids_gyrokinetics_local.Linear()
    ## wavevector
    if gkwrun.nkx*gkwrun.nky==1:
      if use_imaspy:
        dum=gkids.linear.wavevector._element_structure
      else:
        dum=ids_gyrokinetics_local.Wavevector()
      dum.radial_wavevector_norm=gkwrun.grids['keps']*np.sqrt(gxx_th0)/rho_rat
      dum.binormal_wavevector_norm=gkwrun.grids['kzeta']*np.sqrt(gyy_th0)/rho_rat
      gkids.linear.wavevector.append(dum)
    else:
      msg="Critical: conversion of GKW linear runs with multiple wavevectors not handled yet."
      gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
      return

    ## eigenmodes
    if gkwrun.input_out['CONTROL']['method']=='EIV': # eigenvalue run
      neiv=gkwrun.input_out['eiv_integration']['number_eigenvalues']
    else: # initial value run
      neiv=1 

    for ii_eiv in range(neiv):
      if use_imaspy:
        dum=gkids.linear.wavevector[0].eigenmode._element_structure
      else:
        dum=ids_gyrokinetics_local.Eigenmode()

      # grids 
      It_fluxes=range(gkwrun.nt-1,gkwrun.nt)
      It_moments=range(gkwrun.nt-1,gkwrun.nt)
      if (gkwrun.input_out['CONTROL']['method']=='EXP') & (time_interval.size!=0): # time interval for moments
        I1=np.abs(gkwrun.grids['time']-time_interval[0]).argmin()
        I2=np.abs(gkwrun.grids['time']-time_interval[1]).argmin()
        It_moments=range(I1,min(I2+1,gkwrun.nt))
      dum.time_norm=gkwrun.grids['time'][It_moments]/vth_rat*R_rat
      dum.poloidal_turns=2*gkwrun.input_out['GRIDSIZE']['nperiod']-1
      dum.angle_pol=th_sorted
      if use_imaspy==False:
        dum.code=ids_gyrokinetics_local.CodePartialConstant()
      if gkwrun.input_out['CONTROL']['method']=='EXP':
        dum.initial_value_run=int(True)
      elif gkwrun.input_out['CONTROL']['method']=='EIV':
        dum.initial_value_run=int(False)
      else:
        msg="Critical: only explicit time integration or eigenvalue runs are dealt with"
        gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
        return

      # eigenvalues and code parameters 
      if hasattr(gkwrun,'eigenvalues'):
        dum.growth_rate_norm=gkwrun.eigenvalues['gamma'][-1]*vth_rat/R_rat
        dum.frequency_norm=sj_gkw*gkwrun.eigenvalues['freq'][-1]*vth_rat/R_rat
        if dum.initial_value_run==int(True): # initial value run, get tolerance on convergence
          tau_par=(2*np.pi*gkwrun.geom['q']*np.sqrt([x['mass']/x['temp'] for x in gkwrun.input_out['species']])
                   *np.sqrt(m_rat/T_rat))
          tau_perp=(2*np.pi/gkids.linear.wavevector[0].binormal_wavevector_norm
                    *np.abs([x['z']/x['temp'] for x in gkwrun.input_out['species']])*q_rat/T_rat)
          mask=(np.abs([x['z']*x['dens'] for x in gkwrun.input_out['species']])
                /gkwrun.input_out['species'][Iele]['dens']>0.01) # keep only non-trace species
          Delta_t=np.max(np.concatenate((tau_par[mask],tau_perp[mask])))/2
          I1=np.abs(gkwrun.grids['time']-gkwrun.grids['time'][-1]+Delta_t).argmin()
          gamma_final=gkwrun.eigenvalues['gamma'][-1]
          dum.growth_rate_tolerance=np.sqrt(np.trapz((gkwrun.eigenvalues['gamma'][I1:]-gamma_final)**2,
                                                         gkwrun.grids['time'][I1:])/Delta_t)/np.abs(gamma_final) 
        else: # eigenvalue run
          msg="Critical: eigenvalue runs not implemented yet"
          gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose']) 
          return
      if use_imaspy==False:
        dum.code=ids_gyrokinetics_local.CodePartialConstant()
      #dum.code.parameters=nml_to_dict(gkwrun.input_out) #dicttoxml(gkwrun.input_out,return_bytes=False)
      dum.code.parameters=dicttoxml(gkwrun.input_out,return_bytes=False)
      dum.code.output_flag=0

      # eigenfunctions - by default from kykxs diagnostics, but try parallel.dat if unavailable
      fields={'name': ('phi','apar','bpar'),
              'is_field': (True,gkwrun.input_out['CONTROL']['nlapar'],gkwrun.input_out['CONTROL']['nlbpar']),
              'is_kykxs': (hasattr(gkwrun,'kykxs') and 'phi' in gkwrun.kykxs.keys() and gkwrun.kykxs['phi'].size>0, 
                           hasattr(gkwrun,'kykxs') and 'apar' in gkwrun.kykxs.keys() and gkwrun.kykxs['apar'].size>0, 
                           hasattr(gkwrun,'kykxs') and 'bpar' in gkwrun.kykxs.keys() and gkwrun.kykxs['bpar'].size>0),
              'is_parallel': (hasattr(gkwrun,'parallel') and 'phi' in gkwrun.parallel.keys() and gkwrun.parallel['phi'].size>0, 
                              hasattr(gkwrun,'parallel') and 'apar' in gkwrun.parallel.keys() and gkwrun.parallel['apar'].size>0, 
                              hasattr(gkwrun,'parallel') and 'bpar' in gkwrun.parallel.keys() and gkwrun.parallel['bpar'].size>0),
              'norm_fac': (T_rat*rho_rat/(q_rat*R_rat), B_rat*rho_rat**2/R_rat, B_rat*rho_rat/R_rat)
              }
      gkw={}
      imas={}
      if any(fields['is_kykxs']):
        if all([x in gkwrun.kykxs['It'] for x in It_moments]):
          It_sel=[gkwrun.kykxs['It'].index(x) for x in It_moments]
        elif len(It_moments)==1 and [x==gkwrun.nt-1 for x in It_moments]: # if only last time step requested, try parallel.dat
          fields['is_kykxs']=(False,False,False)
        else:
          msg="Critical: missing time slices in kykxs data for the selected time interval, can not convert eigenfunctions"
          gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
          return
      for ii,name in enumerate(fields['name']):
        gkw[name]=np.full((gkwrun.ns,len(It_moments)),0.0+1j*0.0)
        imas[name]=np.full((gkwrun.ns,len(It_moments)),0.0+1j*0.0)             
        if fields['is_field'][ii]:
          if fields['is_kykxs'][ii]: # by default, get data from kykxs
            gkw[name]=np.complex128(np.reshape(gkwrun.kykxs[name][0,0,:,It_sel],(gkwrun.ns,len(It_moments))))
          elif fields['is_parallel'][ii]: # if not available, try parallel.dat (linear runs + last time step only)
            gkw[name][:,-1]=gkwrun.parallel[name]*gkwrun.info['rotate_factor']
          else:
            msg="Warning: missing data, can not convert "+fields['name'][ii]+" eigenfunctions to IMAS"
            gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
        imas[name]=gkw[name][Ith_sorted,:]*fields['norm_fac'][ii]

      # amplitude ratio, used for fields, moments and fluxes normalisation 
      if gkwrun.input_out['CONTROL']['normalized']==False:
        msg="""Critical: for non-normalized linear runs the fluxes normalisation is affected by wrong 'ints' value 
               for GKW versions earlier than commit e9e507, 24/02/2021"""
        gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
        #return
      amp_rat = np.sqrt(2*np.pi)/(np.sqrt(np.trapz(np.abs(imas['phi'][:,-1])**2
                                                           +np.abs(imas['apar'][:,-1])**2
                                                           +np.abs(imas['bpar'][:,-1])**2,dum.angle_pol,axis=0)))
      # rotate and normalise eigenfunctions
      # rotate_factor=exp(i*alpha) in IMAS documentation
      th_fine=np.linspace(dum.angle_pol[0],dum.angle_pol[-1],gkwrun.ns*100)
      f_re=CubicSpline(dum.angle_pol,np.real(imas['phi'][:,-1]),bc_type='natural')
      f_im=CubicSpline(dum.angle_pol,np.imag(imas['phi'][:,-1]),bc_type='natural')
      Imax=np.argmax(np.abs(f_re(th_fine)+1j*f_im(th_fine)))
      rotate_factor = f_re(th_fine)[Imax] + 1j*f_im(th_fine)[Imax]
      rotate_factor = abs(rotate_factor)/rotate_factor
      if imas['phi'].any() or imas['apar'].any() or imas['bpar'].any():
        if use_imaspy==False:
          dum.fields=ids_gyrokinetics_local.EigenmodeFields()
      if imas['phi'].any():
        dum.fields.phi_potential_perturbed_norm=imas['phi']*rotate_factor*amp_rat
        dum.fields.phi_potential_perturbed_weight=np.sqrt(np.trapz(np.abs(dum.fields.phi_potential_perturbed_norm)**2,dum.angle_pol,axis=0)/(2*np.pi))
        dum.fields.phi_potential_perturbed_parity = (np.abs(np.trapz(np.real(dum.fields.phi_potential_perturbed_norm),dum.angle_pol,axis=0)
                                                           +1j*np.trapz(np.imag(dum.fields.phi_potential_perturbed_norm),dum.angle_pol,axis=0))
                                             / np.trapz(np.abs(dum.fields.phi_potential_perturbed_norm),dum.angle_pol,axis=0))
      if imas['apar'].any():
        dum.fields.a_field_parallel_perturbed_norm=imas['apar']*rotate_factor*amp_rat
        dum.fields.a_field_parallel_perturbed_weight=np.sqrt(np.trapz(np.abs(dum.fields.a_field_parallel_perturbed_norm)**2,dum.angle_pol,axis=0)/(2*np.pi))
        dum.fields.a_field_parallel_perturbed_parity = (np.abs(np.trapz(np.real(dum.fields.a_field_parallel_perturbed_norm),dum.angle_pol,axis=0)
                                                              +1j*np.trapz(np.imag(dum.fields.a_field_parallel_perturbed_norm),dum.angle_pol,axis=0))
                                             / np.trapz(np.abs(dum.fields.a_field_parallel_perturbed_norm),dum.angle_pol,axis=0))
      if imas['bpar'].any():
        dum.fields.b_field_parallel_perturbed_norm=imas['bpar']*rotate_factor*amp_rat
        dum.fields.b_field_parallel_perturbed_weight=np.sqrt(np.trapz(np.abs(dum.fields.b_field_parallel_perturbed_norm)**2,dum.angle_pol,axis=0)/(2*np.pi))
        dum.fields.b_field_parallel_perturbed_parity = (np.abs(np.trapz(np.real(dum.fields.b_field_parallel_perturbed_norm),dum.angle_pol,axis=0)
                                                              +np.trapz(np.imag(dum.fields.b_field_parallel_perturbed_norm),dum.angle_pol,axis=0))
                                             / np.trapz(np.abs(dum.fields.b_field_parallel_perturbed_norm),dum.angle_pol,axis=0))

      # moments (loop over species) - only from kykxs diagnostic
      moments={'name': ('dens','vpar','Tpar','Tperp','Qpar','M12','M24'),
               'imas_name': ('density','j_parallel','pressure_parallel','pressure_perpendicular','heat_flux_parallel',
                             'v_parallel_energy_perpendicular','v_perpendicular_square_energy'),
               'common_norm_fac': amp_rat*rho_rat/R_rat/n_cf_th0*rotate_factor,
               'norm_fac': (np.ones(gkwrun.nsp),qN_gkw*q_rat*vthN_gkw*vth_rat,2/3*TN_gkw*T_rat,4/3*TN_gkw*T_rat,
                            TN_gkw*T_rat*vthN_gkw*vth_rat,TN_gkw*T_rat*vthN_gkw*vth_rat,TN_gkw*T_rat*(vthN_gkw*vth_rat)**2)
              }

      ext_gkw=('','_J0','_J1')
      ext_imas=('','_bessel_0','_bessel_1')
      for jj in range(len(ext_gkw)):
        if use_imaspy:
          ddum=getattr(dum,'moments_norm_gyrocenter'+ext_imas[jj])
        else:
          ddum=ids_gyrokinetics_local.MomentsLinear()
        non_empty=False
        for ii,name in enumerate(moments['name']):
          if hasattr(gkwrun,'kykxs') and (name+ext_gkw[jj] in gkwrun.kykxs.keys()) and gkwrun.kykxs[name+ext_gkw[jj]].size>0:
            non_empty=True
            dum_gkw=np.moveaxis(gkwrun.kykxs[name+ext_gkw[jj]][0,0,:,It_sel,:],[0,2],[2,0])
            dum_imas=dum_gkw[:,Ith_sorted,:]*np.moveaxis(np.tile(moments['norm_fac'][ii]*moments['common_norm_fac'],[1,gkwrun.ns,len(It_moments)]),[0,2],[2,0])
            setattr(ddum,moments['imas_name'][ii],dum_imas)
          else:
             dum_imas=[]
             msg="Warning: missing data, can not convert "+moments['name'][ii]+ext_gkw[jj]+" moment to IMAS"
             gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
        if non_empty==True:
          setattr(dum,'moments_norm_gyrocenter'+ext_imas[jj],ddum)

      # quasi-linear weights (fluxes) per eigenmode, FS averaged
      # Note that GKW QL weights depend on the number of poloidal turns, hence the factor (2*gkwrun.input_out['GRIDSIZE']['nperiod']-1)
      fluxes={'name': ('particles','momentum_parallel','momentum_perp','heat'),
              'field': ('phi','apar','bpar'),
              'is_field': (True,gkwrun.input_out['CONTROL']['nlapar'],gkwrun.input_out['CONTROL']['nlbpar']),
              'imas_name': ('particles_phi_potential','momentum_phi_parallel_phi_potential','momentum_phi_perpendicular_phi_potential','energy_phi_potential',
                            'particles_a_field_parallel','momentum_phi_parallel_a_field_parallel','momentum_phi_perpendicular_a_field_parallel','energy_a_field_parallel',
                            'particles_b_field_parallel','momentum_phi_parallel_b_field_parallel','momentum_phi_perpendicular_b_field_parallel','energy_b_field_parallel'),
              'common_norm_fac': (amp_rat*rho_rat/R_rat)**2*vth_rat/n_cf_th0*(2*gkwrun.input_out['GRIDSIZE']['nperiod']-1),
              'norm_fac': (np.ones(gkwrun.nsp),-m_rat*R_rat*vth_rat*np.sqrt(TN_gkw*mN_gkw),-m_rat*R_rat*vth_rat*np.sqrt(TN_gkw*mN_gkw),T_rat*TN_gkw)
             }
      n_name=len(fluxes['name'])
      for ii, field in enumerate(fluxes['field']):
        for jj, name in enumerate(fluxes['name']):
          if fluxes['is_field'][ii]:
            # rotating frame
            if (hasattr(gkwrun,'fluxes_transport') and field in gkwrun.fluxes_transport.keys() 
                and name in gkwrun.fluxes_transport[field].keys() and gkwrun.fluxes_transport[field][name].size>0):
              if (dum.linear_weights_rotating_frame is None) and (use_imaspy==False): 
                dum.linear_weights_rotating_frame=ids_gyrokinetics_local.Fluxes()
              gkw_flux=np.squeeze(gkwrun.fluxes_transport[field][name][-1,:])
              setattr(dum.linear_weights_rotating_frame,fluxes['imas_name'][ii*n_name+jj],
                      gkw_flux*fluxes['common_norm_fac']*fluxes['norm_fac'][jj])
            else:
              msg="Warning: missing data, can not convert "+field+" "+name+" flux to IMAS"
              gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
            # lab frame
            if (hasattr(gkwrun,'fluxes_transport_lab') and field in gkwrun.fluxes_transport_lab.keys() 
                and name in gkwrun.fluxes_transport_lab[field].keys() and gkwrun.fluxes_transport_lab[field][name].size>0):
              if (dum.linear_weights is None) and (use_imaspy==False):
                dum.linear_weights=ids_gyrokinetics_local.Fluxes()
              gkw_flux=np.squeeze(gkwrun.fluxes_transport_lab[field][name][-1,:])
              setattr(dum.linear_weights,fluxes['imas_name'][ii*n_name+jj],
                      gkw_flux*fluxes['common_norm_fac']*fluxes['norm_fac'][jj])
            else:
              msg="Missing data, can not convert lab frame "+field+" "+name+" flux to IMAS"
              gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])

      gkids.linear.wavevector[0].eigenmode.append(dum)

  ## non-linear
  if gkwrun.input_out['CONTROL']['non_linear']: 
    if use_imaspy==False:
      gkids.non_linear=ids_gyrokinetics_local.NonLinear()
    gkids.non_linear.quasi_linear=int(False)    
    if use_imaspy==False:
      gkids.non_linear.code=ids_gyrokinetics_local.CodePartialConstant()
    #gkids.non_linear.code.parameters=nml_to_dict(gkwrun.input_out) #dicttoxml(gkwrun.input_out,return_bytes=False) -> gives pb with lists
    gkids.non_linear.code.parameters=dicttoxml(gkwrun.input_out,return_bytes=False)
    gkids.non_linear.code.output_flag=0 
    if time_interval.size==0: # take all available times 
      It_fluxes=range(gkwrun.nt)
      It_moments=range(gkwrun.nt) 
    else: 
      I1=np.abs(gkwrun.grids['time']-time_interval[0]).argmin()
      I2=np.abs(gkwrun.grids['time']-time_interval[1]).argmin()
      It_fluxes=range(I1,min(I2+1,gkwrun.nt))
      It_moments=range(I1,min(I2+1,gkwrun.nt))
    gkids.non_linear.time_norm=gkwrun.grids['time'][It_fluxes]*R_rat/vth_rat
    gkids.non_linear.time_interval_norm=gkwrun.kykxs_spectra['time_interval_avg']*R_rat/vth_rat
    gkids.non_linear.angle_pol=th_sorted

    ## wavevector grids
    gkids.non_linear.radial_wavevector_norm=gkwrun.grids['keps']*np.sqrt(gxx_th0)/rho_rat
    gkids.non_linear.binormal_wavevector_norm=gkwrun.grids['kzeta']*np.sqrt(gyy_th0)/rho_rat

    ## Fields intensity spectra
    intensities={'name': ('phi','apar','bpar'),
                 'imas_name': ('phi_potential_perturbed_norm','a_field_parallel_perturbed_norm','b_field_parallel_perturbed_norm'),
                 'norm_fac': (T_rat*rho_rat/(q_rat*R_rat), B_rat*rho_rat**2/R_rat, B_rat*rho_rat/R_rat)
                 }
    if hasattr(gkwrun,'kykxs_spectra'): #fill only if data is available
      if all([x in gkwrun.kykxs_spectra['It'] for x in It_moments]):
        It_sel=[gkwrun.kykxs_spectra['It'].index(x) for x in It_moments]
      else:
        msg="Critical: missing time slices in kykxs_spectra data for the selected time interval, can not convert field intensities"
        gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
        return
      for ii,field in enumerate(intensities['name']):
        if field in gkwrun.kykxs_spectra.keys():
          if gkwrun.kykxs_spectra[field]['intensity']['kykxs'].size>0:
            if (getattr(gkids.non_linear,'fields_intensity_3d') is None) and (use_imaspy==False):
              gkids.non_linear.fields_intensity_3d=ids_gyrokinetics_local.FieldsNl3D()
            gkw_spectrum=np.float64(gkwrun.kykxs_spectra[field]['intensity']['kykxs'])
            setattr(gkids.non_linear.fields_intensity_3d,intensities['imas_name'][ii],gkw_spectrum*intensities['norm_fac'][ii]**2)
          if gkwrun.kykxs_spectra[field]['intensity']['kykx'].size>0:
            if (getattr(gkids.non_linear,'fields_intensity_2d_surface_average') is None) and (use_imaspy==False):
              gkids.non_linear.fields_intensity_2d_surface_average=ids_gyrokinetics_local.FieldsNl2DFsAverage()
            gkw_spectrum=np.float64(gkwrun.kykxs_spectra[field]['intensity']['kykx'])
            setattr(gkids.non_linear.fields_intensity_2d_surface_average,intensities['imas_name'][ii],gkw_spectrum*intensities['norm_fac'][ii]**2)
          if gkwrun.kykxs_spectra[field]['zonal'][:,It_sel].size>0:
            if (getattr(gkids.non_linear,'fields_zonal_2d') is None) and (use_imaspy==False):
              gkids.non_linear.fields_zonal_2d=ids_gyrokinetics_local.FieldsNl2DKy0()
            gkw_spectrum=np.complex128(gkwrun.kykxs_spectra[field]['zonal'][:,It_sel])
            setattr(gkids.non_linear.fields_zonal_2d,intensities['imas_name'][ii],gkw_spectrum*intensities['norm_fac'][ii])
          if gkwrun.kykxs_spectra[field]['intensity']['ky'].size>0:
            if (getattr(gkids.non_linear,'fields_intensity_1d') is None) and (use_imaspy==False):
              gkids.non_linear.fields_intensity_1d=ids_gyrokinetics_local.FieldsNl1D()
            gkw_spectrum=np.float64(gkwrun.kykxs_spectra[field]['intensity']['ky'])
            setattr(gkids.non_linear.fields_intensity_1d,intensities['imas_name'][ii],gkw_spectrum*intensities['norm_fac'][ii]**2)

    ## Fluxes 
    fluxes={'name': ('particles','momentum_parallel','momentum_perp','heat'),
            'field': ('phi','apar','bpar'),
            'is_field': (True,gkwrun.input_out['CONTROL']['nlapar'],gkwrun.input_out['CONTROL']['nlbpar']),
            'imas_name': ('particles_phi_potential','momentum_phi_parallel_phi_potential','momentum_phi_perpendicular_phi_potential','energy_phi_potential',
                          'particles_a_field_parallel','momentum_phi_parallel_a_field_parallel','momentum_phi_perpendicular_a_field_parallel','energy_a_field_parallel',
                          'particles_b_field_parallel','momentum_phi_parallel_b_field_parallel','momentum_phi_perpendicular_b_field_parallel','energy_b_field_parallel'),
            'common_norm_fac': (rho_rat/R_rat)**2*vth_rat/n_cf_th0,
            'norm_fac': (np.ones(gkwrun.nsp),-m_rat*R_rat*vth_rat*np.sqrt(TN_gkw*mN_gkw),-m_rat*R_rat*vth_rat*np.sqrt(TN_gkw*mN_gkw),T_rat*TN_gkw)
           }
    n_name=len(fluxes['name'])
    # rotating frame 
    if hasattr(gkwrun,'kykxs_spectra'): #fill only if data is available
     for ii, field in enumerate(fluxes['field']):
        if field in gkwrun.kykxs_spectra.keys():
          for jj, name in enumerate(fluxes['name']):
            if name in gkwrun.kykxs_spectra[field]:
              if gkwrun.kykxs_spectra[field][name]['kykxs'].size>0:
                if (getattr(gkids.non_linear,'fluxes_4d_rotating_frame') is None) and (use_imaspy==False):
                  gkids.non_linear.fluxes_4d_rotating_frame=ids_gyrokinetics_local.FluxesNl4D()
                gkw_flux=gkwrun.kykxs_spectra[field][name]['kykxs']
                if name=='heat':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['heat_cf']['kykxs']
                if name=='momentum_parallel' and field=='apar':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['momentum_parallel_polmag']['kykxs']
                if name=='momentum_perp' and not field=='apar':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['momentum_perp_polmag']['kykxs']
                imas_flux=np.float64(np.moveaxis(gkw_flux,3,0))
                for ii_sp in range(gkwrun.nsp):
                  imas_flux[ii_sp,:,:,:]=imas_flux[ii_sp,:,:,:]*fluxes['norm_fac'][jj][ii_sp]*fluxes['common_norm_fac'][ii_sp]
                setattr(gkids.non_linear.fluxes_4d_rotating_frame,fluxes['imas_name'][ii*n_name+jj],imas_flux)
              if gkwrun.kykxs_spectra[field][name]['kykx'].size>0:
                if (getattr(gkids.non_linear,'fluxes_3d_rotating_frame') is None) and (use_imaspy==False):
                  gkids.non_linear.fluxes_3d_rotating_frame=ids_gyrokinetics_local.FluxesNl3D()
                gkw_flux=gkwrun.kykxs_spectra[field][name]['kykx']
                if name=='heat':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['heat_cf']['kykx']
                if name=='momentum_parallel' and field=='apar':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['momentum_parallel_polmag']['kykx']
                if name=='momentum_perp' and not field=='apar':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['momentum_perp_polmag']['kykx']
                imas_flux=np.float64(np.moveaxis(gkw_flux,2,0))
                for ii_sp in range(gkwrun.nsp):
                  imas_flux[ii_sp,:,:]=imas_flux[ii_sp,:,:]*fluxes['norm_fac'][jj][ii_sp]*fluxes['common_norm_fac'][ii_sp]
                setattr(gkids.non_linear.fluxes_3d_rotating_frame,fluxes['imas_name'][ii*n_name+jj],imas_flux)
              if gkwrun.kykxs_spectra[field][name]['ky'].size>0:
                if (getattr(gkids.non_linear,'fluxes_2d_k_x_sum_rotating_frame') is None) and (use_imaspy==False):
                  gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame=ids_gyrokinetics_local.FluxesNl2DSumKx()
                gkw_flux=gkwrun.kykxs_spectra[field][name]['ky']
                if name=='heat':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['heat_cf']['ky']
                if name=='momentum_parallel' and field=='apar':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['momentum_parallel_polmag']['ky']
                if name=='momentum_perp' and not field=='apar':
                  gkw_flux=gkw_flux+gkwrun.kykxs_spectra[field]['momentum_perp_polmag']['ky']
                imas_flux=np.float64(np.moveaxis(gkw_flux,1,0))
                for ii_sp in range(gkwrun.nsp):
                  imas_flux[ii_sp,:]=imas_flux[ii_sp,:]*fluxes['norm_fac'][jj][ii_sp]*fluxes['common_norm_fac'][ii_sp]
                setattr(gkids.non_linear.fluxes_2d_k_x_sum_rotating_frame,fluxes['imas_name'][ii*n_name+jj],imas_flux)
              if gkwrun.kykxs_spectra[field][name]['kyt'].size>0:
                if (getattr(gkids.non_linear,'fluxes_2d_k_x_k_y_sum_rotating_frame') is None) and (use_imaspy==False):
                  gkids.non_linear.fluxes_2d_k_x_k_y_sum_rotating_frame=ids_gyrokinetics_local.FluxesNl2DSumKxKy()
                gkw_flux=np.sum(gkwrun.kykxs_spectra[field][name]['kyt'],axis=0)
                if name=='heat':
                  gkw_flux=gkw_flux+np.sum(gkwrun.kykxs_spectra[field]['heat_cf']['kyt'],axis=0)
                if name=='momentum_parallel' and field=='apar':
                  gkw_flux=gkw_flux+np.sum(gkwrun.kykxs_spectra[field]['momentum_parallel_polmag']['kyt'],axis=0)
                if name=='momentum_perp' and not field=='apar':
                  gkw_flux=gkw_flux+np.sum(gkwrun.kykxs_spectra[field]['momentum_perp_polmag']['kyt'],axis=0)
                imas_flux=np.float64(np.moveaxis(gkw_flux,1,0))
                for ii_sp in range(gkwrun.nsp):
                  imas_flux[ii_sp,:]=imas_flux[ii_sp,:]*fluxes['norm_fac'][jj][ii_sp]*fluxes['common_norm_fac'][ii_sp]
                setattr(gkids.non_linear.fluxes_2d_k_x_k_y_sum_rotating_frame,fluxes['imas_name'][ii*n_name+jj],imas_flux)

                if (getattr(gkids.non_linear,'fluxes_1d_rotating_frame') is None) and (use_imaspy==False):
                  gkids.non_linear.fluxes_1d_rotating_frame=ids_gyrokinetics_local.FluxesNl1D()
                t=gkwrun.grids['time'][gkwrun.kykxs_spectra['It_avg']]
                I_trapz=range(gkwrun.kykxs_spectra['It_avg'][0]-gkwrun.kykxs['It'][0],gkwrun.kykxs_spectra['It_avg'][-1]-gkwrun.kykxs['It'][0]+1)
                imas_flux=np.trapz(imas_flux[:,I_trapz],t,axis=1)/(t[-1]-t[0])
                setattr(gkids.non_linear.fluxes_1d_rotating_frame,fluxes['imas_name'][ii*n_name+jj],imas_flux)

    # laboratory frame
    if hasattr(gkwrun,'fluxes_transport_lab'): #fill only if data is available
      for ii, field in enumerate(fluxes['field']):
        if field in gkwrun.fluxes_transport_lab.keys():
          for jj, name in enumerate(fluxes['name']):
            if name in gkwrun.fluxes_transport_lab[field]:
              if gkwrun.fluxes_transport_lab[field][name].size>0:
                gkw_flux=gkwrun.fluxes_transport_lab[field][name] # nt*nsp

                if (getattr(gkids.non_linear,'fluxes_2d_k_x_k_y_sum') is None) and (use_imaspy==False):
                  gkids.non_linear.fluxes_2d_k_x_k_y_sum=ids_gyrokinetics_local.FluxesNl2DSumKxKy()
                imas_flux=np.float64(np.moveaxis(gkw_flux,1,0)) # nsp*nt
                for ii_sp in range(gkwrun.nsp):
                  imas_flux[ii_sp,:]=imas_flux[ii_sp,:]*fluxes['norm_fac'][jj][ii_sp]*fluxes['common_norm_fac'][ii_sp]
                setattr(gkids.non_linear.fluxes_2d_k_x_k_y_sum,fluxes['imas_name'][ii*n_name+jj],imas_flux)

                if (getattr(gkids.non_linear,'fluxes_1d') is None) and (use_imaspy==False):
                  gkids.non_linear.fluxes_1d=ids_gyrokinetics_local.FluxesNl1D()
                t=gkwrun.grids['time'][gkwrun.kykxs_spectra['It_avg']]
                I_trapz=range(gkwrun.kykxs_spectra['It_avg'][0]-gkwrun.kykxs['It'][0],gkwrun.kykxs_spectra['It_avg'][-1]-gkwrun.kykxs['It'][0]+1)
                imas_flux=np.trapz(imas_flux[:,I_trapz],t,axis=1)/(t[-1]-t[0])
                setattr(gkids.non_linear.fluxes_1d,fluxes['imas_name'][ii*n_name+jj],imas_flux)

  ## species_all - shearing_rate (do it at the end since it requires It_moments)
  if gkwrun.input_out['CONTROL']['method']!='EXP':
    gkids.species_all.shearing_rate_norm=0.0
  else:
    if gkwrun.input_out['rotation']['shear_profile']=='none':
      gkids.species_all.shearing_rate_norm=0.0
    elif gkwrun.input_out['rotation']['shear_profile']=='wavevector_remap':
      if gkwrun.input_out['rotation']['t_shear_begin']<gkwrun.grids['time'][It_moments[0]]:
        if gkwrun.input_out['rotation']['toroidal_shear']=='none':
          gkids.species_all.shearing_rate_norm=gkwrun.input_out['rotation']['shear_rate']*B_rat*vth_rat/R_rat
        elif (gkwrun.input_out['rotation']['toroidal_shear']=='use_uprim' or 
             gkwrun.input_out['rotation']['toroidal_shear']=='add_uprim'):
          gkids.species_all.shearing_rate_norm=gkwrun.info['shearing_rate']*B_rat*vth_rat/R_rat
        else:
           print("Unknown or unsupported toroidal_shear option in ROTATION namelist")
      elif gkwrun.input_out['rotation']['t_shear_begin']>gkwrun.grids['time'][It_moments[-1]]:
        gkids.species_all.shearing_rate_norm=0.0 # shearing applied after the specified interval
      if (gkwrun.input_out['rotation']['t_shear_begin']>gkwrun.grids['time'][It_moments[0]] and
          gkwrun.input_out['rotation']['t_shear_begin']<gkwrun.grids['time'][It_moments[-1]]):
        msg=r"Varying shearing rate within the specified time interval not allowed\n" + r"t_shear_begin="+str(gkwrun.input_out['rotation']['t_shear_begin'])
        gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
        return
    else:
      msg="Critical: unknown or unsupported shear_profile option in ROTATION namelist"
      gkwrun.msg_log('gkw2ids',msg,gkwrun.settings['verbose'])
      return

  ## finally output the ids
  return gkids















