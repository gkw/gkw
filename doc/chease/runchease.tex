\documentclass[a4paper,12pt]{article}

\newcommand{\ind}[1]{\ensuremath{_{\rm{#1}}}}
\newcommand{\expo}[1]{\ensuremath{^{\rm{#1}}}}


\usepackage{amsmath}
\usepackage{amssymb}

\setlength\parindent{0in}
\oddsidemargin 0.0in
\evensidemargin 1.0in
\textwidth 6.0in
\textheight 9.0in
\headheight 0.0in
\topmargin 0.0in

% Title Page
\title{Running GKW for actual tokamak geometry}
\author{Y. Camenen}
\date{March 6, 2009\\ Updated on June 9, 2011}

\begin{document}
\maketitle
This document describes how to to run GKW for actual tokamak geometry getting the metric elements from the CHEASE code for toroidal MHD equilibria \cite{Lutjens:Comput1992,Lutjens:Comput1996}.

%*********************************************************************
\part*{Fast Track}
%*********************************************************************
Get the source, compile, run a test case and check the results. It is assumed that you already have the latest GKW version in the \texttt{./gkw} folder.

\section{Get CHEASE}
The CHEASE code has been developed and is maintained at CRPP, Lausanne. A copy of the latest version can always be obtained on the public svn server via:\\
\texttt{\small svn export https://crppsvn.epfl.ch/repos/CHEASE/chease/trunk chease}\\
It will copy  the files on your machine in the \texttt{./chease/} directory. You can use \texttt{co} instead of \texttt{export} to also get the svn links.

Alternatively, a tagged CHEASE version that has been tested with GKW can be found under:\\
\texttt{\small svn export https://crppsvn.epfl.ch/repos/CHEASE/chease/tags/CHEASE\_90\_9\_3 chease}\\
This tag, and the latest version of CHEASE will work with GKW r1459 or later.  

The latest CHEASE version includes the most recent developments and bug fixes, but unlike the tagged version, it has not necessarily been extensively tested with GKW (but test cases are now available to ensure that the latest CHEASE version produces the expected results). 

Note that CHEASE versions after tag \texttt{CHEASE\_90\_7\_5} and before tag \texttt{CHEASE\_90\_9\_3} had a bug that prevents their use to compute metric elements for GKW. Using these versions will produce wrong results!

Note also that the GKW version in the CPC journal repository, the GKW /branch/cpc, and versions prior to r1459 only work with the CHEASE version tagged \texttt{CHEASE\_90\_7\_3}.
 
\section{Compile CHEASE}

\subsection{CHEASE\_90\_7\_3, CHEASE\_90\_7\_1}

For reference, use with CPC version of GKW.

Once the sources on your local machines, the compilation should be straightforward. The \texttt{Makefile} is very simple and the target machine is selected by commenting/uncommenting the appropriate block (you may need to add a block for your machine).
Note that provided the HDF5 and FUTILS libraries are installed, it is possible to use HDF5 to output the results of the CHEASE run. This option is not required (and not used) for the interface with GKW but could be implemented relatively easily.

\subsection{CHEASE\_90\_7\_5}

Use only if you have a good reason. It is recommended to use instead the latest version or tag \texttt{CHEASE\_90\_9\_3} which benefits from several improvements. Works with newer versions of GKW (from r1459). 

Unfortunately this tagged version does not compile exactly as checked out.  To compile you first need to:
\texttt{ln -s euitm\_schemas\_407b.f90 euitm\_schemas.f90} and uncomment the lines in the build rules at the bottom relating to \texttt{euitm\_schemas}.

Use the following for CSC warwick desktop:
\begin{verbatim}
 # CSC desktop Warwick
F90 = mpif90
F90FLAGS = -r8 -O3 -I$(DIR_interpos)
LDFLAGS =
LIBS=-L$(DIR_interpos) -linterpos
F90FLAGS_parser = -O1
\end{verbatim}
An appropriately modified makefile is provided with GKW in \texttt{/doc/chease/chease\_90\_7\_5.makefile}

\subsection{CHEASE\_90\_9\_3 or latest version in the trunk}

These are the recommended CHEASE versions.\\
Works with newer versions of GKW (from r1459).  Should compile easily if you add a few lines for your machine and compiler in the \texttt{Makefile.define\_*} files. The HDF5 and FUTILS libraries are not required for the interface with GKW.

\section{Run the test cases}

To run the test cases desribed in this section, you will need \texttt{CHEASE\_90\_9\_3} or a later version.

\subsection{Create working directory}
To start with, create a working directory, for instance:\\
\texttt{\small mkdir ./chease/runs/}

\subsection{Input files}
CHEASE solves the Grad-Shafranov equation in toroidal geometry taking as an input the last closed flux surface description, the radial derivative of the pressure profile and the current profile. These quantities can be specified in various ways, the most common being the use of an EXPEQ or an EQDSK file. CHEASE also needs an input file containing the values of the various switches (fortran namelist).\\

For the first run, we will use the input files provided in\\
\texttt{./chease/WK/TESTCASES/NIDEAL10/input/}.\\
There are two test cases available: \texttt{splus} and \texttt{tcv.snd1}. 
To run the test case \texttt{splus}, you will need to copy the corresponding EXPEQ and namelist file in the working directory:\\
\texttt{\small cp ./chease/WK/TESTCASES/NIDEAL10/input/EXPEQ.splus ./chease/runs/EXPEQ}\\
\texttt{\small cp ./chease/WK/TESTCASES/NIDEAL10/input/i.splus ./chease/runs/chease\_namelist}\\
Note that the EXPEQ file and the namelist file HAVE TO be named \texttt{EXPEQ} and \texttt{chease\_namelist}, respectively.

\subsection{Run CHEASE}
Before running CHEASE, You will need to copy the executable compiled in the previous section in the working directory:\\
\texttt{\small cp ./chease/src-f90/chease ./chease/runs/}\\
Then, launch the executable (you must be in the working directory):\\
\texttt{\small cd ./chease/runs/}
\texttt{\small ./chease | tee o.chease}\\
A lot of outputs are displayed on the screen and stored in the file \texttt{o.chease}. The file \texttt{hamada.dat} is also created, it contains all the elements required for the metric coefficients calculation in GKW. All the quantities in \texttt{hamada.dat} are in SI units.

\subsection{Check the results}
The expected outputs are stored in\\
 \texttt{./chease/WK/TESTCASES/NIDEAL10/output\_ref/}.\\
If you are using matlab, you can use the script\\
\texttt{./chease/WK/TESTCASES/NIDEAL10/matlab/chease\_diff.m}\\
to check that you obtained the correct result (as CHEASE is quite sensitive to compiler and compilation options, a standard 'diff' will not do the job...). Just type:
\texttt{\small >> chease\_diff(1e-8,'splus',' ./chease/WK/TESTCASES/NIDEAL10/output\_ref/','','./chease/runs/');}\\
If you get no warning, all is fine. 

\section{Matlab scripts}
A couple of matlab scripts intended to be used in combination with CHEASE can be found in the directory \texttt{./gkw/matlab/chease/} that you naturally have downloaded from the GKW svn repository. These scripts are:
\begin{itemize}
 \item \texttt{read\_eqdsk.m} to read the content of an EQDSK file
 \item \texttt{read\_expeq.m}  to read the content of an EXPEQ file
 \item \texttt{write\_expeq.m} to write the content of a matlab structure (with the appropriate format) into an EXPEQ file.
 \item \texttt{read\_hamada.m} to read the content of the hamada.dat file (CHEASE output when NIDEAL=10)
 \item \texttt{read\_neoart.m} to read the content of the neoart.dat file (another CHEASE output when NIDEAL=10)
 \item \texttt{chease\_diff.m} to compare the outputs hamada.dat and neoart.dat to reference files
\end{itemize}
The three last scripts can also be obtained from the CHEASE svn repository.\\

Note that I wrote the matlab routines to examine the input/output files in a very non-general way as they were not intially intended to be distributed. They may still be useful, take what you want...\\

You can for instance load the content of \texttt{hamada.dat} using the matlab routine \texttt{read\_hamada.dat} and plot the magnetic shear profile as a function of the normalised radius $\rho_\psi$ (where $\psi$ is the poloidal flux):\\
\texttt{\small >> H1=read\_hamada('hamada.dat','./chease/runs/');}\\
\texttt{\small >> plot(sqrt(H1.psi./max(H1.psi)),H1.amin./H1.q.*H1.dqdpsi./H1.damindpsi);}\\
It is of course crucial to check that the output is consistent with what you expect, in particular if you want to make comparisons with the experiments and/or use rescaling of the equilibrium in CHEASE.


\section{Run GKW}
Before running GKW, you need to specify in the \texttt{GEOM} namelist that 1) you want to use CHEASE, 2) where is the output file containing the metric (I assume here that GKW is launched from the working directory \texttt{./chease/runs/}):\\
\texttt{\small GEOM\_TYPE = 'chease'}\\
\texttt{\small EQFILE = '../WK/TESTCASES/NIDEAL10/output\_ref/hamada.splus.dat '}\\
The flux surface of interest is specified using its \texttt{EPS} parameter. Depending on the value (1 or 2) of the \texttt{EPS\_TYPE} parameter, \texttt{EPS} is either the local inverse aspect ratio (i.e. the $\psi$ coordinate of GKW) or $\rho_\Psi$ the square root of the normalised poloidal flux ($\rho_\Psi=0$ on the axis and $\rho_\Psi=1$ on the last closed flux surface).
Note that when 
$$\texttt{EPS}=\psi=\frac{R\ind{max}-R\ind{min}}{2R\ind{ref}}$$
is used, $R\ind{ref}$ is taken to be the parameter \texttt{R0EXP} used for the normalisations in the CHEASE inputs. The values given in the GKW input file for \texttt{Q} and \texttt{SHAT} are not used and taken directly from CHEASE.\\

Now let's try to run GKW using the reference input file in the \texttt{./gkw/doc/chease} folder.\\
\texttt{\small cp ./gkw/doc/chease/input.gkw.chease ./chease/runs/input.dat}\\
Then copy your GKW executable in the working directory and run it. If all goes well, you should be thrown out of the code with the message\\
\texttt{Radial grid too coarse in CHEASE: increase NPSI}.\\
This is because GKW looks in the file \texttt{hamada.splus.dat} for a flux surface with the specified \texttt{EPS} value. If a flux surface is not found close enough to the specified value, the code stops. To avoid that, you can either change the value of \texttt{EPS} in the GKW input, or rerun CHEASE taking care to have a radial grid point where you want (i.e. you increase \texttt{NPSI} or you use packing with $\texttt{NMESHA}=4$). To look where are the CHEASE grid points:\\
\texttt{\small >> H1=read\_hamada('hamada.dat','./chease/runs/');}\\
\texttt{\small >> plot(sqrt(H1.psi./max(H1.psi)),H1.amin./H1.r0exp,'+');}\\
You can then choose $\texttt{EPS}=0.75$ (keeping \texttt{EPS\_TYPE=2}) in the file \texttt{input.dat} and try again. This time, it should work. The output file \texttt{geom.dat} gives the metric elements used in GKW on the $s$ grid. This file can be read in matlab using \texttt{read\_geom.m} available in \texttt{./gkw/matlab/}. The actual value used for \texttt{EPS} is given in \texttt{geom.dat}.\\

A last paragraph about the parallel grid. In CHEASE the number of points (for one poloidal turn) used for the parallel/poloidal grid is specified through the variable \texttt{NCHI} in the input namelist. To avoid interpolation in GKW, not all values of \texttt{N\_s\_grid} are allowed for a given \texttt{NCHI}. You must have: $$\texttt{NCHI}=2k \frac{\texttt{N\_s\_grid}}{2\texttt{NPERIOD}-1}$$
with $k$ a positive integer. This is a bit of a constraint, but to rerun CHEASE with another \texttt{NCHI} value takes less than a minute. It is only annoying when you want to scan \texttt{N\_s\_grid} or \texttt{NPERIOD} which does not happen that often...
Finally, \texttt{N\_s\_grid} must be odd to ensure that a point is at the outboard midplane (this constraint could be relaxed...).


%*********************************************************************
\part*{Playing with CHEASE}
%*********************************************************************
Different ways to run CHEASE have been developped over the years. Most of the available options are described in Ref.~\cite{Lutjens:Comput1996}, the others are to my knowledge not documented. Some options, useful to compute the metric for GKW, are briefly described here.

\section{CHEASE namelist}

A minimal namelist input file for CHEASE, with some comments, can be found in \texttt{./gkw/doc/chease/short.namelist.chease}.

\subsection{Normalisation}
The input parameter \texttt{R0EXP} in [m] is the approximate radial position of the magnetic axis (you do not necessarily know it precisely before running chease) and \texttt{B0EXP} in [T] is the value of the magnetic field at this position. These two parameters are used for the normalisations in CHEASE:
\begin{eqnarray*}
 R &=& R\ind{CHEASE} * \texttt{R0EXP}\\
 Z &=& Z\ind{CHEASE} * \texttt{R0EXP}\\
 \Psi &=& \Psi\ind{CHEASE} * \texttt{R0EXP}^2 * \texttt{B0EXP}\\
 T &=& T\ind{CHEASE} * \texttt{R0EXP} * \texttt{B0EXP}\\
 TT' &=& TT'\ind{CHEASE} * \texttt{B0EXP}\\
 p' &=& p'\ind{CHEASE} * \texttt{B0EXP}/(\mu_0*\texttt{R0EXP}^2)\\
 \mu_0 &=& 4\pi 10^{-7}
\end{eqnarray*}

\textbf{The normalising radius $R\ind{ref}$ in GKW is taken to be the parameter $\texttt{R0EXP}$.  When running with an EQDSK file, CHEASE uses the 5th value in the EQDSK file RCENTR for R0EXP, which may correspond to the limiter centre and not the magnetic axis.  This value can be overridden by setting a negative R0EXP in the chease input file.  You can always check the value of R0EXP used by chease in the hamada.dat file.  Since R0EXP=$R_{\rm Ref}$ in GKW, you need to to know its value in order to input eps and all the gradients correctly in GKW.}

\subsection{NIDEAL}
The parameter \texttt{NIDEAL} is used to specify the output of CHEASE. Two options are of interest here:\\
$\texttt{NIDEAL}=6$ to generate an EQDSK, file \texttt{EQDSK.OUT}\\
$\texttt{NIDEAL}=10$ to generate the metric for GKW, file \texttt{hamada.dat}\\

\subsection{Smoothing the inputs}
Very often the description of the last closed flux surface and of the pressure/current profiles has been obtained using cubic splines. Even if the fit looks smooth, standard splines can give rise to big jumps in the second derivative. Such jumps are a nightmare for the metric computation but fortunately, they can be avoided by using a spline fit with tension. This had previously to be done with the routines \texttt{bndfit} and \texttt{proffit} but is now done automatically in CHEASE.\\
If the parameter \texttt{TENSBND} is specified in the input namelist, its value is used for the tension in the fit of the plasma boundary.
The parameter \texttt{TENSPROF} can also be specified for the tension used in the fit of the profiles, but is not used at the time of writting. 
If \texttt{TENSBND=0.}, no tension is used. If the parameter is not specified in the namelist, default tension is used.\\

If you decide to specify the tension in the namelist, you NEED to check that the last closed flux surface in the output is reasonnably close to the input one. If you give too much tension, you will get too much smoothing. Too little tension and you will not remove the potential wiggles. 

\subsection{Scaling of the equilibrium}
The current profile can be rescaled using the \texttt{NCSCAL} parameter (see \cite{Lutjens:Comput1996} p.231):\\
$\texttt{NCSCAL}=1$: matches the safety factor $q(\texttt{CSSPEC})$ to \texttt{QSPEC} where \texttt{CSSPEC} is in $\rho_\psi$.\\
$\texttt{NCSCAL}=2$: matches the total current value to $\texttt{CURRT}$ (CHEASE units, works only if $\texttt{NSTTP}\neq3$)\\
$\texttt{NCSCAL}=4$: no rescaling\\
The flux function $T=F=R B\ind{T}$ is also rescaled to have $T\ind{axis}=1$ if $\texttt{NTMF0}=1$ or $T\ind{edge}=1$ if $\texttt{NTMF0}=0$ (in CHEASE units). If you choose $\texttt{R0EXP}=R\ind{axis}$
and $\texttt{B0EXP}=B\ind{axis}$, the natural choice would be to have $\texttt{NTMF0}=1$.

\subsection{Packing}
In CHEASE, you can locally increase the resolution of the grids used for the mapping. For instance, you can increase the radial resolution around a rational flux surface or the poloidal resolution
around the X-point. I have added a new option for the packing in CHEASE to specify the exact position in $\rho_\psi$ of a few radial grid points, this to avoid guessing the
correct \texttt{EPS}
value in GKW. This option can be used with $\texttt{NMESHA}=4$. The number of flux surfaces around which the packing is done is then given by \texttt{NPOIDA} and their position ($\rho_\psi$: square
root of the poloidal flux, 0 in the center, 1 in the edge) by \texttt{APLACE}. The "width" of the densification is specified by \texttt{AWIDTH} and the total fraction of the radial grid which remains
undensified is given by \texttt{SOLPDA}. Hence, to densify around $\rho_\psi=0.4$ and $\rho_\psi=0.65$, you can use:\\
\texttt{\small NMESHA=4, NPOIDA=4, SOLPDA=.50,}\\
\texttt{\small APLACE=.4,.4,.65,.65, AWIDTH=.07,.03,.07,.03,}\\
where the double specification of each point with a progressive decrease of the densification width is a "standard" way to get a "good" densification (from O. Sauter).\\
The flux surface is then selected in GKW by using \texttt{EPS\_TYPE=2} and specifying the value of $\rho_\psi$ in \texttt{EPS}.

\section{EXPEQ file}
The EXPEQ file is used as an input in  CHEASE if $\texttt{NSURF}=6$ and $\texttt{NEQDSK}=0$. All quantities in the EXPEQ file are in CHEASE units (see \cite{Lutjens:Comput1996} p.236), i.e.
they are all normalised using \texttt{R0EXP} and \texttt{B0EXP}. The content of the EXPEQ file is:\\
Line 1: $(R\ind{max}-R\ind{min})/(R\ind{max}+R\ind{min})$ of the LCFS\\
Line 2: $(Z\ind{max}+Z\ind{min})/(R\ind{max}+R\ind{min})$ of the LCFS\\
Line 3: pressure at the LCFS\\
Line 4: NP, number of points used in the discretization of the LCFS\\
Line 5 to 5+NP-1: first column R, second column Z of the LCFS points (do not forget that these quantities are normalised to \texttt{R0EXP})\\
Line 5+NP: N, number of points used in the discretization of the profiles\\
Line 6+NP: \texttt{NSTTP}, form used for the current profile specification. For $\texttt{NSTTP}=1$, you give $TT'$, see \cite{Lutjens:Comput1996} p.247 for other options. Note that the value of
\texttt{NSTTP} given in the CHEASE namelist is not used if you choose to read the EXPEQ file.\\
Line 6+NP+1 to 6+NP+N: radial grid in $\rho_\psi$\\
Line 7+NP+N to 6+NP+2N: $p'=\partial p/\partial \psi$ radial derivative of the pressure versus $\psi$, not $\rho_\psi$ (and in CHEASE units, of course)\\
Line 7+NP+2N to 6+NP+3N: $TT'$ if $\texttt{NSTTP}=1$. Here again the radial derivative is versus $\psi$ and the input is in CHEASE units.\\
The exact definition of the radial grid that has to be used is:
$$\rho_\psi=\sqrt{\frac{|\psi-\psi\ind{axis}|}{|\psi\ind{edge}-\psi\ind{axis}|}}$$
which means that 
$$p'=\frac{\partial p}{\partial \psi} = \frac{1}{2\rho_\psi|\psi\ind{edge}-\psi\ind{axis}|}\frac{\partial p}{\partial \rho_\psi}$$ 
Depending from where you get your value of $|\psi\ind{edge}-\psi\ind{axis}|$, you might need to divide it by $2\pi$ to be consistent with the CHEASE definition (which has the $2\pi$). Anyway, you will
easily spot the problem when comparing the input and output pressure profiles: they should be quasi-identical if the input value of $|\psi\ind{edge}-\psi\ind{axis}|$ is correct.\\
Did I say that all the quantities in the EXPEQ file must be in CHEASE units?

\section{EQDSK file}
The EQDSK file is used as an input in CHEASE if $\texttt{NSURF}=6$ and $\texttt{NEQDSK}=1$. All quantities in the EQDSK are in SI units (ouf...). The content of the EQDSK is relatively well described
in \cite{Lutjens:Comput1996} p.236. You can run CHEASE directly from the EQDSK obtained from a reconstruction code like EFIT. Just make sure that the EQDSK is of type 3 (first number after the date
in the first line) and that you have run BNDFIT to have a good descritption of the LCFS. Important (and a bit confusing), the EQDSK file has to be named \texttt{EXPEQ} in the working directory to be
found by CHEASE.

\section{Output file \texttt{hamada.dat}}
The CHEASE output file containing the metric elements for the $(\Psi,\zeta,s)$ coordinate system is named \texttt{hamada.dat} and is created in \texttt{hamada.f90}. All quantities are
given in MKSA units. The output file content is described below (see \texttt{gkwandchease.tex} for further information).\\

\textbf{Scalars}\\
NPSI: number of points for the radial grid\\
NS: number of points for the poloidal grid (specified with NCHI in the CHEASE namelist)\\
R0EXP: reference major radius used in CHEASE, should be taken to be close to the magnetic axis radius for faster convergence (but the actual position is not necessarily known beforehand).\\
B0EXP: reference magnetic field in CHEASE defined as the magnetic field at R0EXP\\
Raxis: magnetic axis radius as calculated by CHEASE
\vspace{0.5cm}

\textbf{Grids}\\
PSI: $\Psi$, the poloidal flux, zero at the magnetic axis, increasing towards the edge. The $\Psi$ grid given in output is equidistant in $\sqrt{\Psi}$ (except if mesh densification is used). The
first point is NOT on the magnetic axis (it is at $\sqrt{\Psi}=\sqrt{\Psi\ind{\max}}/\mathrm{NPSI}$, while on the magnetic axis $\sqrt{\Psi}=0$). The steps of the grid are $\sqrt{\Psi\ind{\max}}/\mathrm{NPSI}$ and the last point is on the last closed flux surface.\\
S: $s$, as defined in GKW. The grid is equidistant and for a given flux surface $s=0$ is on the low field side, at the height of the magnetic axis. 
\vspace{0.5cm}

\textbf{1D arrays}\\
Rgeom: major radius of the flux surface centre, defined as $(R\ind{max}+R\ind{min})/2$\\
amin: minor radius of the flux surface, defined as $(R\ind{max}-R\ind{min})/2$\\
damindpsi: $\partial \mathrm{amin}/\partial \Psi$\\
d2amindpsi2: $\partial^2 \mathrm{amin}/\partial \Psi^2$\\
bmax: maximum of the magnetic field strength on the flux surface\\
bmin: minimum of the magnetic field strength on the flux surface\\
q: safety factor\\
dqdpsi: $\partial q /\partial \Psi$\\
dqdpsi\_chk: value of $\partial \zeta / \partial \Psi$ at $s=1$ (equal to $\partial q /\partial \Psi$)\\
p: plasma pressure\\
dpdpsi: $\partial p /\partial \Psi$\\
jac: $J_{\Psi\zeta s}$ the jacobian of the $(\Psi,\zeta,s)$ coordinates\\
djacdpsi: $\partial J_{\Psi\zeta s} /\partial \Psi$\\
f: $F=RB\ind{t} = R^2|\mathbf{B}\cdot\nabla\phi|$ (always positive by convention)\\
dfdpsi: $\partial F /\partial \Psi$\\

\textbf{2D arrays}\\
g11: $\nabla \Psi \cdot \nabla \Psi$\\
g12: $\nabla \Psi \cdot \nabla \zeta$\\
g13: $\nabla \Psi \cdot \nabla s$\\
g22: $\nabla \zeta \cdot \nabla \zeta$\\
g23: $\nabla \zeta \cdot \nabla s$\\
g33: $\nabla s \cdot \nabla s$\\
B: the norm of the magnetic field\\
dBdpsi: $\partial B/\partial \Psi$\\
dBds: $\partial B/\partial s$\\
R: major radius of the $(\Psi,s)$ grid points (which describe flux surfaces) \\
Z: height of the $(\Psi,s)$ grid points (which describe flux surfaces) \\
dRdpsi: $\partial R/\partial \Psi$\\
dRds: $\partial R/\partial s$
dZdpsi: $\partial Z/\partial \Psi$\\
dZds: $\partial Z/\partial s$\\
theta: the poloidal angle, zero at $s=0$, increasing anticlockwise and measured from the magnetic axis\\
Ah: $\mathcal{A}$ \\
dAhdpsi: $\partial \mathcal{A}/\partial \Psi$\\ 
dzetadpsi: $\partial \zeta/\partial \Psi$\\ 
dzetadchi: $\partial \zeta/\partial \chi$\\ 


%*********************************************************************
\part*{List of reference cases}
%*********************************************************************
A couple of reference input files are included in the folder \texttt{./gkw/doc/chease/ref\_cases/}. They include the standard \texttt{chease\_namelist} file called \texttt{i.case\_name} and the corresponding EXPEQ or EQDSK. Packing is used such that the radial mesh is densified around $\rho_\psi=0.2$, $0.4$, $0.6$ and $0.8$. Furthermore, the value of \texttt{NCHI} is 126, which means that the number of points per poloidal turn in GKW can be 7, 9, 21 or 63 ($k=9$, 7, 3 or 1). You can of course change the value of NCHI in the input namelist at will to run GKW with a different number of points with poloidal turn. The input last closed flux surface and profiles are already smooth, so no tension is needed for the fits (\texttt{TENSBND=0.} and \texttt{TENSPROF=0.} in the input namelist).

\section{Circular case: \texttt{circ}}
Circular LCFS, $\texttt{R0EXP}=1$ and $\texttt{B0EXP}=1$. No rescaling of the current profile.\\
In that example, the input file is an EQDSK. Just to show it works...

\section{MAST DND: \texttt{mast.dnd1}}
MAST double-null plasma, based on \#18696 at $t=0.25\,\textrm{s}$, $\texttt{R0EXP}=1.002$ and $\texttt{B0EXP}=0.436$, no rescaling of the current profile.

\section{TCV SND: \texttt{tcv.snd1}}
TCV single-null plasma, based on \#31837 at $t=1.0\,\textrm{s}$, $\texttt{R0EXP}=0.891$ and $\texttt{B0EXP}=1.44$, no rescaling of the current profile.

\section{Waltz and Cyclone}
These two cases do not need introduction. The CHEASE input file have been tweaked so that the $q$ and shear value matches the one of the Waltz or Cyclone cases at the required radial position. The
flux surfaces are concentric. The \texttt{hamada.dat} are available for several values of $N_s$ (parallel grid). You can generate your own by changing the parameter $\texttt{NCHI}$ in the CHEASE input namelist.


\bibliographystyle{iopart-num}
\bibliography{gkwandchease}

\end{document}
