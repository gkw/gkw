******************************************************************
UPDATE

THIS TEST was later improved by setting MPI_ENVIRONMENT variables
See ../nonspectral2

******************************************************************

Input files used for nonspectral scaling tests.  There are 6 x ghost cells in the 
gyroaverage in this setup with lx=128.  The gridsizes are
 
 NX = 256,   
 N_s_grid = 32, 
 N_mu_grid = 16, 
 N_vpar_grid = 32, 
 NMOD = 16,
 number_of_species = 2 

The results in perform_helios were generated with GKW r2733.  They show good nonspectral 
scaling up to 8192 cores, with a significant bottleneck after that. 

The results demonstrate that x parallel is required for nonspectral because vpar parallel 
is much worse than for non spectral (this is because the "diagonal part" of the fields 
solve is a much bigger fraction of the total). The good news is that there is no scaling 
penalty for collisions (which are only about 15% slower anyway).

The matrix compression routines now allow a good memory efficiency allowing access to a 
lower limit number of cores for the scaling test reference value.  The perflib does not 
slowdown the code noticeably even a large numbers of cores and is now output in a matlab 
readable format.

Given that the communication hardware on this machine is not as good as hector, this result 
compares favorably with the first spectral scaling test we had on Hector. 
