3rd Iteration
--------------

The tarballs contain data from the 2nd iteration of results.  In the 3rd
iteration the results were improved further by comparing Intel MPI and
BullX MPI (there is not one clear winner) and using the optimisations
of issue 197 (with r3373).

BullX MPI seems to perform slightly better at ghost cell communications
of derived datatypes.  Intel MPI seems to perform better at the large
collective operations.  Which is better therefore depends on which MPI
communication is dominant for a given problem.  It is possible the results
could be improved further by further tuning of the MPI environment 
variables (for example with intel mpitune)


2nd Iteration
---------------

Input files and 2nd iteration results for nonspectral scaling tests on Helios 
with GKW r3074.  Two changes were necessary to improve the scaling
test from the original in ../nonspectral at large numbers of cores.


1) The timings using perform.f90 were performed from the second loop of the code.
   This is because in the first loop the MPI initializations are slow
   
   The MPI initialization times seem to scale with N^2, but it is not a major issue 
   for us since at 32K cores it is about 15 s, so not a problem for production runs
   
   
2) For BullX MPI,MPI environment variables had to be set as follows:

      It was found that at 32K cores and above, OMPI environment variables have 
      a large impact on communications performance (for bullxmpi).  

      The code contains three main types of MPI communications:
      # MPI_allreduce (over a small Cartesian communicator direction: 32)
      # MPI_allgather (over a small Cartesian communicator direction: 32)
      # MPI persistent point to point using MPI_startall and MPI_waitall (many small arrays)

      The ''OMPI_MCA_coll'' environment variable has a large effect at 32K+ cores:

      * With ''export OMPI_MCA_coll="^ghc,tuned"'', then the collective allgather 
      and allreduce operations take MUCH longer than at 16K

      * Not setting it at all, then the persistent communications take MUCH longer than at 16K. 

      * With ''export OMPI_MCA_coll="^ghc"'', which disables the 
      "generalized hierarchical collective" 
      then all types of operations perform adequately (perhaps not optimally).

      Therefore the generalized hierarchical collective causes the point to point ghost cell 
      communications to be a least an order of magnitude slower.  The collective operations 
      are 25% to 100% slower without ghc, but relative to the alternative ghost cell communication
      slowdown this is a minor problem.  And the option ''OMPI_MCA_coll="tuned"'' is no help, 
      it actually makes all the collective communications slower.


For the medium size test:

There are 6 x ghost cells in the 
gyroaverage in this setup with lx=128.  The gridsizes are
 
 NX = 256,   
 N_s_grid = 32, 
 N_mu_grid = 16, 
 N_vpar_grid = 32, 
 NMOD = 16,
 number_of_species = 2 


For the large size test:

There are 6 x ghost cells in the 
gyroaverage in this setup with lx=256.  The gridsizes are

 NX = 512,
 N_s_grid = 64,
 N_mu_grid = 16,
 nperiod  = 1,
 N_vpar_grid = 64,
 NMOD = 43,
 number_of_species = 2
 
 
The results demonstrate that x parallel is required for nonspectral because vpar parallel 
is much worse than for non spectral (this is because the "diagonal part" of the fields 
solve is a much bigger fraction of the total). The good news is that there is no scaling 
penalty for collisions (which are only about 15% slower anyway).  

However, the results for the large test case also demonstrated that it is preferable to
use a couple of processors in the vpar direction rather than using the x parallelism to
the maximum.  N_procs_x > 16 appears to generate a performance penalty.

The matrix compression routines now allow a good memory efficiency allowing access to a 
lower limit number of cores for the scaling test reference value.  The perflib does not 
slowdown the code noticeably even a large numbers of cores and is now output in a matlab 
readable format.

1st Iteration
-------------

In ../nonspectral
