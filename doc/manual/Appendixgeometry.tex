\chapter{Geometry - generalised}
\label{chapter:geometry-generalised}

\section{Field aligned Hamada coordinates} 
\index{Hamada coordinates}
\label{sec:hamada}
In this Appendix the straight field line coordinates as well as the shifted metric are 
described in detail. 
The development of the material in this section has benefited from, and closely follows the 
publications of B.D. Scott \cite{SCO98,SCO01}, and the references cited therein. 
This material is in addition to Section \ref{geometry} of this document (which describes the specific geometry models implemented); here we add
a generalised formulation for deriving the geometry tensors, and describe the shifted metric procedure for the nonspectral version of the code.
At some point in the future the two sections should be consolidated.

We start with an arbitrarily shaped, but toroidally symmetric geometry, for which the magnetic 
field can be written in the form \index{Magnetic field representation}
\begin{equation}
{\bf B} = s_B F \nabla \varphi + s_j \nabla \varphi \times \nabla \Psi,
\end{equation}
where $F = RB_t$ with $B_t > 0$ being the toroidal magnetic field strength. The quantities 
$s_B$ and $s_j$ represent the sign of the magnetic field and plasma current (both positive 
when in the direction of $\nabla \varphi$. Finally, $\Psi$ is the poloidal flux, not to 
be confused with the radial coordinate ($\psi$). Of course, $\psi = \psi(\Psi)$, and the 
choice $\psi = \Psi$ will sometimes be made below. 
 
An orthogonal coordinate system $(\psi,\theta,\varphi)$ is assumed, where $\psi$ is the 'radial'
coordinate (i.e. ${\bf B}\cdot \nabla \psi = 0$), $\theta$ is the poloidal angle (upward 
on the outboard midplane), and $\varphi$ is the toroidal angle (clockwise when viewed from 
above). 
Furthermore, it is assumed that the magnetic field and quantities like the major radius are 
known in this coordinate system. 
Below two coordinate transformations are introduced. 
The first will make the field lines straight in the new coordinates, and the second will 
align one of the coordinates with the magnetic field. 

In the first step we transform the poloidal and toroidal angle 
\begin{equation} 
s = s(\psi,\theta), \qquad \gamma = \gamma(\psi,\theta,\varphi)
\end{equation} 
in such a way that the contra-variant components of the magnetic field are flux functions 
in the new coordinate system ($B^s = B^s(\psi)$, $B^\gamma = B^\gamma(\psi)$). These 
coordinates are known as Hamada coordinates \cite{HAM62}.
Note that the new coordinate $\gamma$ is still an ignorable coordinate, since any function that is independent of $\varphi$
will be independent of $\gamma$ ($f(\theta,\psi) \rightarrow f(s,\psi)$) after the coordinate 
transformation. The transformation of the poloidal angle is chosen such that $B^s$ becomes 
a flux function 
\begin{equation} 
B^s = {\bf B} \cdot \nabla s = {\bf B}\cdot \nabla \theta {\partial s \over \partial \theta} 
+ {\bf B} \cdot \nabla \psi {\partial s \over \partial \psi} = {\bf B} \cdot \nabla \theta 
{\partial s \over \partial \theta}
\end{equation} 
From which the relation between $s$ and $\theta$ can be derived 
\begin{equation}
{\partial s \over \partial \theta}={B^{s} \over {\bf B \cdot \nabla \theta}}\Leftrightarrow
\int_{0}^{\theta}{\partial s \over \partial \theta}d\theta =s=B^{s}\int_{0}^{\theta}{d\theta \over 
{\bf B}\cdot \nabla \theta}
\end{equation}
since $B^{s}$ does not depend on $\theta$ (flux function) and
\begin{equation}
\oint {\partial s \over \partial \theta}d\theta =1=B^{s}\oint {d\theta \over {\bf B}\cdot \nabla 
\theta}\Leftrightarrow B^{s}={1 \over \oint {d\theta \over {\bf B}\cdot \nabla \theta} }
\end{equation}
which gives the following equation for s:
\begin{equation} 
s(\theta,\psi) = {\int_0^\theta {{\rm d} \theta^\prime \over {\bf B} \cdot \nabla \theta^\prime} 
\biggl /  \oint {{\rm d}\theta^\prime \over {\bf B}\cdot \nabla \theta^\prime}} \qquad 
{\rm with} \qquad 
B^s =  {1
\biggl /  \oint {{\rm d}\theta^\prime \over {\bf B}\cdot \nabla \theta^\prime}}
\end{equation}  
where the normalizing constant has been chosen such that the domain [-1/2,1/2] in $s$ corresponds 
to one poloidal turn. 

The new coordinate is closely related to the flux surface average. This average is defined 
as the volume average between two flux surfaces in the limiting case in which the distance between 
the flux functions goes to zero. One can derive the following relation  
\begin{equation} 
\{ g \} = \oint {{\rm d} l_p \over B_p } g \biggl / \oint {{\rm d} l_p \over B_p }
\end{equation}
where $l_p$ is the length in the poloidal direction (${\rm d}l_p = \vert {\bf e}_\theta \vert 
{\rm d} \theta$), $B_p$ is the poloidal component of the magnetic field strength ($B_p = 
{\bf B}\cdot \nabla \theta / \vert \nabla \theta \vert$), and the integral is to be taken at 
constant $\psi$. Inserting the expressions for the 
poloidal length and the poloidal magnetic field strength and using $\vert {\bf e}_\theta\vert 
\vert \nabla \theta \vert = 1$ one obtains 
\begin{equation} 
\{ g \} = \oint {\rm d}s g 
\end{equation} 


For the transformation of the toroidal angle we choose 
\begin{equation} 
\gamma = {\varphi \over 2 \pi} + g(\theta,\psi) 
\end{equation}
The $2\pi$ is added here such the domain [0,1] in $\gamma$ will correspond to one toroidal 
turn around the flux surface. The contra-variant component of the magnetic field is 
\begin{equation} 
B^\gamma = s_B {F \over 2\pi} {1\over R^2} + {\bf B} \cdot \nabla \theta {\partial g \over \partial 
\theta} 
\end{equation} 
where $F = R B_t = F(\psi)$ is a flux function. (Here $B_t$ is the toroidal magnetic field 
strength $B_t = {\bf B} \cdot \nabla \varphi / \vert \nabla \varphi \vert$ where $\vert \nabla \varphi 
\vert = 1/R$ with $R$ being the local major radius)
From the expression above one can derive 
\begin{equation} 
g(\theta,\psi) = \int_0^\theta {{\rm d}\theta^\prime \over {\bf B}\cdot \nabla \theta^\prime } 
\biggl [ B^\gamma - s_B{F \over 2\pi} {1 \over R^2} \biggr ] 
\end{equation} 
Demanding that $g$ is periodic
\begin{equation}
\oint {{\rm d}\theta^\prime \over {\bf B}\cdot 
\nabla \theta^\prime } 
\biggl [ B^\gamma - s_B{F \over 2\pi} {1 \over R^2} \biggr ] =0
\end{equation}
yields 
\begin{equation} 
B^\gamma = s_B {F\over 2 \pi }  \left \{ {1\over R^2} \right \} 
\end{equation} 
The coordinate $\gamma$ can then be expressed as 
\begin{equation} 
\gamma = {\varphi \over 2\pi} + s_B {F \over 2\pi} \int_0^\theta {{\rm d} \theta^\prime \over {\bf B}
\cdot \nabla \theta^\prime} \biggl [ \left \{ {1\over R^2} \right \} - {1\over R^2} 
\biggr ] 
\end{equation} 
The coordinates $(\psi,s,\gamma)$ are the Hamada coordinates. The expression above should 
allow these coordinates to be calculated for an arbitrary toroidally symmetric geometry. 

\index{Field aligned coordinates}
We now go through the second coordinate transformation where one of the coordinates is 
aligned with the magnetic field, i.e. we demand 
\begin{equation} 
{\bf B} \cdot \nabla = B^s {\partial \over \partial s}
\end{equation}
Note that this does not mean that $\nabla s$ is in the direction of the magnetic field. 
In general it is not. The transformation is 
\begin{equation} 
\zeta = \zeta(s,\gamma,\psi) 
\end{equation}
In the new coordinates 
\begin{equation}
{\bf B}\cdot \nabla =B^{\psi}{\partial \over \partial \psi}+B^{s}{\partial \over \partial s}+
B^{\zeta}{\partial \over \partial \zeta}
\end{equation}
where $B^\psi$ is zero. The transformation to field aligned coordinates is the transformation 
for which $B^\zeta = 0$. The latter condition can be formulated as 
\begin{equation} 
B^\gamma {\partial \zeta \over \partial \gamma} + B^s {\partial \zeta \over \partial s} = 0
\end{equation} 
which is satisfied for the simple linear transformation
\begin{equation} 
\zeta =  q s - \gamma \qquad {\rm where} \qquad q = {B^\gamma \over B^s} 
\end{equation}
From the field line equation it follows that 
\begin{equation} 
{{\rm d} \gamma \over {\rm d} s} = {B^\gamma \over B^s} \qquad \rightarrow \qquad 
\oint {{\rm d} \gamma \over {\rm d} s} {\rm d} s = \oint {B^\gamma \over B^s} {\rm d} s = q 
\end{equation} 
i.e. $q = q(\psi)$ is indeed the safety factor. 

Note that the coordinate transformation above flips the sign of the toroidal angle. 
The right handed coordinate system can therefore be defined as $(\psi,\zeta,s)$. 
The Jacobian of the new coordinate system can be expressed in terms of the original 
Jacobian through 
\begin{equation} 
(\nabla \psi \times \nabla \zeta) \cdot \nabla s = {1 \over J_{\psi  \zeta s} } = 
{1\over 2 \pi}{\partial s \over \partial \theta} {1 \over J_{\psi \theta \varphi}} 
\end{equation} 
From which it follows 
\begin{equation} 
J_{\psi  \zeta s} = 2\pi {\bf B} \cdot \nabla \theta \oint {{\rm d} \theta^\prime \over 
{{\bf B} \cdot \nabla \theta^\prime}} J_{\psi \theta \varphi}  = 
2 \pi {{\bf B} \cdot \nabla \theta \over B^s } J_{\psi \theta \varphi}
\end{equation} 
We note here that the coordinates $s$ and $\zeta$ are dimensionless, but that the 
dimension of $\psi$ is not yet defined. We will always use a normalized $\psi$ that 
is dimensionless. 


\section{Caculating the Geometry tensors from the metric tensor and derivatives of coordinates
and magnetic field} 

The various tensors that are used for the implementation of the geometry are not independent. 
${\cal E}$, ${\cal D}$, ${\cal F}$, ${\cal G}$, ${\cal H}$, ${\cal I}$, ${\cal J}$, ${\cal K}$ 
can be expressed in the metric tensor and derivatives of the 
coordinates as well as the magnetic field. We will assume here that the metric tensor $g^{\alpha 
\beta}$, the magnetic field strength ($B$) and its derivatives towards the coordinates (${\partial 
B / \partial s}, \partial B / \partial \psi$), the major radius $R$ and its derivatives 
($\partial R / \partial s, \partial R / \partial \psi$), the z-coordinate $Z$ and its 
derivatives ($\partial Z / \partial s, \partial Z / \partial \psi$) and finally the 
derivative of the poloidal flux towards the radial coordinate $\partial \Psi / \partial \psi$ 
are given. Of course, even these quatities are not independent, since the derivatives of $R,Z$ towards 
the coordinates can be used to calculate the metric tensor. 
The tensors can then be calculated as discussed below 

Since we can write any vector ${\bf A}$ as 
\begin{equation} 
{\bf A} = \frac{1}{2}\epsilon_{ijk} A^i J_{x^i x^j x^k} \nabla x^j \times \nabla x^k, 
\end{equation}
one finds for the magnetic field, using $\psi = \Psi$, 
\begin{equation} 
\mathbf{B} = B^s J_{\Psi \zeta s} \nabla \Psi \times \nabla \zeta.
\end{equation}
Using 
\begin{equation}
B^s = \mathbf{B}\cdot\nabla s = s_b F \nabla \varphi \cdot \nabla s + s_j \nabla \varphi \times \nabla \Psi \cdot \nabla s
\end{equation}
and remembering that by definition of $\zeta$
\begin{equation}
\nabla \varphi \times \nabla \Psi \cdot \nabla s = -2\pi \nabla \zeta \times \nabla \Psi \cdot \nabla s,
\end{equation}
one gets
\begin{equation} 
 B^s = 2\pi s_j \frac{1}{J_{\Psi \zeta s}} 
\end{equation}
% YC 05.03.2012: I prefer the few lines above, but feel free to revert to the lines below if you find it clearer
%\begin{equation} 
%\mathbf{B} = B^s J_{\Psi \zeta s} \nabla \Psi \times \nabla \zeta = 2 \pi ({\bf B} \cdot \nabla \theta) J_{\Psi \theta \varphi} 
%\end{equation}
%Since $\nabla \Psi$, $\nabla \theta$ and $\nabla \varphi$ are orthogonal 
%\begin{equation} 
%J_{\Psi \theta \varphi} = { 1\over \vert \nabla \Psi \vert \vert \nabla \theta \vert \vert \nabla \varphi \vert} 
%\end{equation}
%And 
%\begin{equation} 
%{\bf B} = 2 \pi {R {\bf B} \cdot \nabla \theta \over \vert \nabla \Psi \nabla \vert \vert \nabla \theta \vert } 
%\nabla \Psi \times \nabla \zeta 
%\end{equation} 
%Finally using  
%\begin{equation} 
%\nabla \Psi = R B_p  \qquad {\bf B} \cdot \nabla \theta = s_j B_p \vert \nabla \theta \vert 
%\end{equation}
%
Finally, one obtains \index{Magnetic field representation} 
\begin{equation} 
{\bf B} = 2 \pi s_j \nabla \Psi \times \nabla \zeta 
\label{clebsch}
\end{equation}

The Clebsch representation of the magnetic field can directly be used to calculate the tensor ${\cal E}$ 
\begin{equation} 
{\cal E}^{\alpha \beta} = {1 \over 2 B^2 }{\bf B} \cdot ( \nabla x^\alpha \times \nabla x^\beta )  =
{\pi s_j \over B^2} {\partial \Psi \over \partial \psi} [ g^{\psi \alpha} g^{\zeta \beta} - g^{\zeta \alpha} g^{\psi \beta} ] 
\end{equation} 
With the ${\cal E}$ tensor and the derivatives of the magnetic field strength given, the ${\cal D}$ vector 
directly follows from 
\begin{equation} 
{\cal D}^\alpha = - 2 {\cal E}^{\alpha \beta} {1 \over B} {\partial B \over \partial x_\beta} 
\end{equation}
The scalars that have been given earlier in this document are 
\begin{equation} 
{\cal F} = {B^s \over B} \qquad {\cal G} = {{\cal F} \over B} {\partial B \over \partial s} 
\end{equation}
The angular rotation vector points in the $\nabla Z$ direction, and 
\begin{equation} 
{\bf \Omega}_\perp = \Omega {\bf b} \times (\nabla z \times {\bf b} ) 
\end{equation} 
This allows the ${\cal H}$ vector to be written in the form 
\begin{equation} 
{\cal H^\alpha} = - \frac{s_B}{B_N} \biggl [ {\partial Z \over \partial x_\beta } g^{\alpha \beta} - 
\biggl ( {B^s \over B} \biggr )^2 {\partial Z \over \partial s} \delta_{\alpha s} \biggr ] 
\end{equation}
The ${\cal I}$ tensor is straightforwardly 
\begin{equation} 
{\cal I}^\alpha = {1 \over 2 B} (\nabla x_\alpha \times \nabla R^2 ) \cdot {\bf b} = 2 R {\cal E}^{\alpha \beta} 
{\partial R \over \partial x_\beta} 
\end{equation}
The ${\cal J}$ and ${\cal K}$ tensor can be directly calculated from the known quantities. 

\section{Periodicity} 

\index{Periodicity on the torus}
The torus is periodic in both the toroidal as well as the poloidal angle. In the new coordinates
this means that for any function $f(\psi,\zeta,s)$ 
\begin{equation} 
f(\psi,\zeta+1,s) = f(\psi,\zeta,s)
\label{toroidalperiodic}
\end{equation}
\begin{equation}
f(\psi,\zeta+q,s+1) = f(\psi,\zeta,s) 
\label{poloidalperiodic}
\end{equation}
In a flux tube one, furthermore, demands that the solution is periodic in the radial direction
\begin{equation} 
f(\psi + L_\psi,\zeta,s) = f(\psi,\zeta,s) 
\label{radialperiodic}
\end{equation}

None of the equilibrium quantities is a function of $\zeta$. For these quantities Eq.~(\ref{toroidalperiodic})
is trivially satisfied, and the Eq. (\ref{poloidalperiodic}) reduces to 
\begin{equation} 
B(\psi,s+1) = B(\psi,s)
\end{equation}
where we have use the background magnetic field strength as example. The perturbed quantities (distribution
function as well as fields) for the $\zeta-$ direction are always represented by Fourier modes 
\begin{equation} 
f(\psi,\zeta,s) = \sum\nolimits_{k_\zeta} \hat f (\psi,k_\zeta,s) \exp [ {\rm i} k_\zeta \zeta / \rho_* ] 
\end{equation} 
The periodicity constraint of Eq. (\ref{poloidalperiodic}) then reduces to 
\begin{equation} 
\hat f(\psi, k_\zeta,s+1) = \hat f (\psi, k_\zeta, s) \exp [ - {\rm i} k_\zeta q / \rho_* ] 
\end{equation}

The flux tube treats the limit of $\rho_* \rightarrow 0$ to the lowest relevant order only. Even though the 
phase factor can in principle take any value, we can assume that a very small change in the safety factor can always be 
choosen such that 
\begin{equation} 
\exp [ - {\rm i} k_\zeta q / \rho_* ]  = 1 
\end{equation}
without changing the results. This choice can, however, only be made for one location since the safety factor is a 
function of the radial coordinate $\psi$. We choose the phase factor above to be zero for the centre of the box (presented
by $\psi = 0$ to obtain a periodicity constraint 
\begin{equation} 
\hat f(\psi, k_\zeta,s+1) = \hat f (\psi, k_\zeta, s) \exp \left [ - {\rm i} k_\zeta {1 \over  \rho_*}{{\rm d} q \over {\rm d} \psi }\psi \right ] 
\end{equation}


\section{Shifted metric} 

In general the metric tensor has no zero elements. Especially the magnetic shear leads to a finite $g^{\zeta \psi}$ element that is 
a function of the coordinate along the field line. This means that an originally rectangular grid cell, deforms when moving along the 
magnetic field. This can potentially lead to problems \cite{SCO01}. A remedy, that does not cost anything computationally when using 
Fourier modes in $\zeta$ is the shifted metric procedure. In this procedure we change the coordinate $\zeta$ at every location in 
$s$. 
\begin{equation} 
\zeta_k = q s - \gamma - \alpha_k(\psi)
\end{equation}
It is important that $\alpha$ is not a function of $\zeta$. With this transformation one has a different set of coordinates at 
every grid point in $s$, with the different grid points denoted by the index k. 
The metric tensor in the new coordinates can be expressed as 
\begin{equation} 
g_k^{\zeta \zeta} = g^{\zeta \zeta} - 2 \alpha_k^\prime g^{\zeta \psi} + (\alpha_k^\prime)^2 g^{\psi \psi}
\end{equation}
\begin{equation} 
g_k^{\zeta \psi} = g^{\zeta \psi} - \alpha_k^\prime g^{\psi \psi}
\end{equation}
\begin{equation}
g_k^{\zeta s} = g^{\zeta s} - \alpha_k^\prime g^{\psi s} 
\end{equation}
where $\alpha_k^\prime = {\partial \alpha_k / \partial \psi}$. With this transformation one can 
set the $g^{\zeta \psi}$ element at the grid point $s = s_k$ locally to zero 
\begin{equation} 
\alpha_k^\prime = {g^{\zeta \psi}(s_k) \over g^{\psi \psi}(s_k)}
\end{equation}
In the flux tube the metric elements are evaluated at one surface only, and are not a function of 
the radial coordinate. The equation above can then be trivially integrated to obtain 
\begin{equation} 
\alpha_k = {g^{\zeta \psi}(s_k) \over g^{\psi \psi}(s_k)} \psi 
\end{equation}
In the new coordinates 
\begin{equation}  
g_k^{\zeta \zeta} = g^{\zeta \zeta} -  (g^{\zeta \psi})^2 / g^{\psi \psi}
\end{equation}
\begin{equation} 
g_k^{\zeta \psi} = 0
\end{equation}
\begin{equation} 
g_k^{\zeta s} = g^{\zeta s} - g^{\zeta \psi} g^{\psi s} / g^{\psi \psi} 
\end{equation}

The perturbed solution in the new coordinates has the form 
\begin{equation} 
\hat f_k (\psi, k_\zeta, s) \exp [ {\rm i} k_\zeta \zeta_k / \rho_* ] 
\end{equation}
We must demand that the solution is single valued, and therefore 
\begin{equation} 
\hat f_k (\psi, k_\zeta, s) \exp [ {\rm i} k_\zeta \zeta_k / \rho_* ] =
\hat f_{p} (\psi, k_\zeta, s) \exp [ {\rm i} k_\zeta \zeta_{p} / \rho_* ]
\end{equation}
Using 
\begin{equation} 
\zeta_{p} = \zeta_k + \alpha_k - \alpha_{p} 
\end{equation} 
We obtain 
\begin{equation} 
\hat f_k (\psi, k_\zeta, s)  =
\hat f_{p} (\psi, k_\zeta, s) \exp [ {\rm i} k_\zeta (\alpha_k - \alpha_{p}) / \rho_* ]
\end{equation}
The perturbed distribution function as well as the fields are calculated at the discrete points 
$s_k$ as $f_k$. When taking a parallel derivative one must use the transformation given above 
to transform $\hat f_p$ to $\hat f_k$. 

Finally we must consider the possible radial periodicity of the domain. In the new coordinates 
Eq.~(\ref{radialperiodic}) becomes 
\begin{equation}  
f_k(\psi + L_\psi,\zeta_k + \alpha_k^\prime \psi + \alpha_k^\prime L_\psi,s) = 
f_k(\psi, \zeta_k + \alpha_k^\prime \psi, s) 
\end{equation}
Or 
\begin{equation} 
\hat f_k(\psi + L_\psi,k_\zeta,s) = \hat f_k (\psi, k_\zeta,s) \exp \left [ -
{\rm i} k_\zeta \alpha_k^\prime L_x \right ] 
\end{equation}

\section{Summary of the sign conventions in GKW} 
\label{signsappendix}
In GKW, the cylindrical coordinate system $(R,Z,\varphi)$ is right handed, which means that $\varphi$ is increasing clockwise when the torus is viewed from above.\\
The toroidal rotation is defined positive for a plasma flowing in the direction of ${\bf B}$.\\
The mode frequency is defined positive for a perturbation evolving in the direction opposite to $\nabla \zeta$. This corresponds to the ion $\nabla B$ drift direction if $s_{\rm j}=1$ and to the
electron $\nabla B$ drift direction if $s_{\rm j}=-1$.  See also Fig.~\ref{directions}\\
Coordinate system:
\begin{itemize}
 \item $\psi=\varepsilon=(R_{\rm max}-R_{\rm min})/(2R_{\rm ref})$ is always increasing from the plasma center to the plasma edge.
 \item $s$ is always increasing upwards from the low field side midplane. It is zero at the height of the magnetic axis, on the low field side midplane.
 \item $\zeta$ is always increasing in the direction opposite to $\varphi$ (i.e. anticlockwise when viewed from above) at constant $\psi$ and $s$. The direction of $\nabla \zeta$ in the poloidal plane is
given by $\textrm{sign}(\nabla s \cdot \nabla \zeta)=\textrm{sign}(\nabla 
\zeta \cdot \nabla \theta)=s_{\rm B}s_{\rm j}$
\end{itemize}

\begin{align}
\textrm{sign}({\bf B} \cdot \nabla \varphi)&\equiv& s_B \\
\textrm{sign}({\bf j} \cdot \nabla \varphi)&\equiv&s_j \\
\textrm{sign}({\bf B} \cdot \nabla \theta)&=& s_j \\
\textrm{sign}({\bf B} \cdot \nabla s)&=& s_j \\
\textrm{sign}(\nabla \varphi \cdot \nabla \zeta)&=& -1 \\
\textrm{sign}(\nabla s \cdot \nabla \theta)&=& 1\\
\textrm{sign}(\nabla \theta \cdot \nabla \zeta)&=& s_B s_j \\
\textrm{sign}(B_\theta \nabla \theta \cdot \nabla \zeta)&=& s_B \\
\textrm{sign}(\nabla s \cdot \nabla \zeta)&=& s_B s_j \\
{\bf B} \cdot \nabla \zeta&=& 0 \\
{\bf \Omega} &=& -s_B \Omega \nabla z \\
u &=& R \Omega 
\end{align}

\section{Flux surface average \label{sec.fsa}}
\subsection{General}
The general definition of the flux surface average is
\begin{equation}
 <A> = \frac{\int A(\mathbf{x}) \delta(r-r_0) d^3{x}}{\int\delta(r-r_0) d^3{x}}
\end{equation}
where $A$ is the quantity being averaged, $r_0$ is the flux surface label of flux surface on which the average is being 
performed, $r$ is the value of the flux surface label at position $\mathbf{x}$, $\delta$ is the Dirac function and the integral is being performed over the whole plasma (or entire world, it does not matter) volume.  

\subsection{GKW coordinates}
In GKW coordinates $(r,\zeta,s)$, the elementary plasma volume is 
\begin{equation}
 d^3 x = dr d\zeta ds \mathcal{J}
\end{equation}
with $\mathcal{J}^{-1} = \nabla r \cdot \nabla \zeta \times \nabla s$. Actually the expression above is true for any coordinates, but
one of the specificity of GKW coordinate system is that $\mathcal{J}$ is a flux surface label:
\begin{equation}
 \mathcal{J}= \frac{\partial\Psi}{\partial r} \frac{s_j}{B^s}
\end{equation}
and $B^s$ is a flux surface label by construction. This implies that the flux surface average can be written as
\begin{equation}
 <A> = \frac{\oint_{r=r_0} A(r,\zeta,s) d\zeta ds}{\oint_{r=r_0} d\zeta ds}
\end{equation}
If $A$ is independent of the toroidal angle, one arrives at
\begin{equation}
 <A> = \oint A(r_0,s) ds
\end{equation}

\subsection{Alternative expression}
Noting that the surface element $\textrm{d} S$ on a flux surface is related to the volume element 
$\textrm{d}^3x$ by
\begin{equation}
 \textrm{d}^3x = \textrm{d} S \frac{\textrm{d}r}{|\nabla r|}
\end{equation}
with $r$ an arbitrary flux surface label, an alternative expression for the flux surface average is obtained:
\begin{equation} \label{eq:FS3}
 <A> = \frac{1}{V'} \oint A \frac{\textrm{d}S}{|\nabla r|}
\end{equation}
with 
\begin{equation}
 V'= \frac{\partial V}{\partial r} = \oint \mathcal{J} \textrm{d}\zeta \textrm{d}s
\end{equation}
and $V$ being the volume enclosed by the flux surface labelled by $r$.


\section{Use of fluxes in transport equations \label{sec.transport}}
In the following the density conservation equation is taken as an example, but the same can be applied to heat, momentum or any
conserved quantity.
The local density conservation equation is 
\begin{equation}
 \frac{\partial n}{\partial t} + \nabla \cdot \mathbf{\Gamma} = S_n
\end{equation}
with $n$ the density in $\textrm{m}^{-3}$, $\mathbf{\Gamma}$ the particle flux in $\textrm{m}^{-2}\textrm{s}^{-1}$ and $S_n$ the particle source density
in $\textrm{m}^{-3}\textrm{s}^{-1}$

Integrating this equation over the volume enclosed by a flux surface labelled by $r_0$ leads to:
\begin{equation}
 \frac{\partial }{\partial t} \int n \textrm{d}V + \oint \mathbf{\Gamma}\cdot\frac{\nabla r}{|\nabla r|} \textrm{d}S = 
\int S_n \textrm{d} V
\end{equation}
where the flux surface shape and position has been assumed to be constant in time. 
Using Eq.~(\ref{eq:FS3}), the continuity equation can then be written as
\begin{equation}
 \frac{\partial }{\partial t} \int n \,\textrm{d}V + V'\left<\mathbf{\Gamma\cdot\nabla r}\right> = 
\int S_n \textrm{d}V
\end{equation}
In steady-state we have:
\begin{equation}
 <\Gamma^r>=<\mathbf{\Gamma\cdot\nabla r}> = \frac{1}{V'}\int S_n \textrm{d}V = \frac{1}{V'}S^\textrm{int}_n
\end{equation}
This means that $\Gamma^r$ which is the quantity we calculate in GKW needs to be compared to the particle source inside the flux 
surface $S^\textrm{int}_n$ divided by $V'$. This is the obvious choice for comparisons with the experiments or transport codes.

Concerning the flux decomposition, the best choice is a direct decomposition of $<\Gamma^r>$:
\begin{equation}
 <\Gamma^r> = - D \frac{\partial n}{\partial r} + V n
\end{equation}
with possibly a normalising length if $r$ is a dimensionless flux label (e.g. GKW $\psi$).
%But it is also possible to rather decompose $\mathbf{\Gamma}$ as above and then a factor $<|\nabla r|^2>$ 
%arises in the definition of $D$ and $V$. 
For a benchmark or a comparison with experiment one needs to make sure that everybody uses the same definition.






% related to auctex mode and latex-preview-mode in Emacs:
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "doc"
%%% End:
