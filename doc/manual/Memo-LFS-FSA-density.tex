\chapter{Transformation of transport coefficients in presence of poloidal asymmetries}
% The ABC memo

%\documentclass[12pt]{article}
%\include{amsmath}
%\textwidth=167mm
%\textheight=245mm
%\oddsidemargin=-3mm
%\topmargin=-18mm
%\newcommand{\Gfs}{\langle \Gamma \rangle}
\newcommand{\Gfs}{\Gamma}
\newcommand{\exparg}{\exp\left\{-\frac{Ze\Phi(r,\theta)}{T(r)} +\frac{m \Omega^2(r)}{2T(r)} \left(R(r,\theta)^2 - R_0(r)^2\right)\right\}}
\newcommand{\nparder}{\frac{\partial n(r,\theta)} {\partial r}}
\newcommand{\Pparder}{\frac{\partial \Phi(r,\theta)} {\partial r}}
\newcommand{\noder}{\frac{d n_0(r)} {d r}}
\newcommand{\nder}{\frac{d n(r)} {d r}}
\newcommand{\Tlnder}{\frac{1}{T(r)} \frac{d T(r)} {d r}}
\newcommand{\Rsqdiff}{\left(R(r,\theta)^2 - R_0(r)^2\right)}
\newcommand{\dnoovdnoder}{n_0(r) \frac{dr}{d n_0(r)} }
\newcommand{\Ecf} E
\newcommand{\expargs}{\exp\left\{-{\Ecf}(r,\theta)\right\}}
\newcommand{\calGr}{{\cal G}_r}
%\begin{document}

\begin{center} 
{\bf Memo on the transformation of the transport
coefficients\\ 
from LFS density to flux-surface averaged density}\\[0.3cm]
C. Angioni, E. Belli and F.J. Casson\\[0.2cm]
Please report comments and corrections to C. Angioni
\end{center}

Note: The notation used in this Appendix does not attempt to be consistent with the rest of the GKW manual.

In the presence of a poloidally asymmetric density $n(r,\theta)$
of a particle species like W, 
the flux surface averaged particle flux $\Gfs$
can be expressed as a function of the flux surface averaged density
$n(r) = \langle n(r,\theta) \rangle$
or as a function of densities evaluated at specific locations
or poloidal angles. A natural choice, for instance,
is $n_0(r) = n(r,\theta = 0)$, evaluated at the LFS $\theta = 0$ location,
which is adopted by codes like GKW and NEO.

In the present memo we provide the equations to transform 
the corresponding transport coefficients from one description to the other.
This is practical for many applications, in particular when the output
of codes which can include 2D poloidal asymmetries has to be
used inside usual 1D transport codes, which consider only 1D
(or in any case flux surface averaged) quantities.

The flux surface averaged particle flux is expressed in the two forms
\[ \Gfs = - D \frac {d n}{d r} + V n, \]
and
\[ \Gfs = -D_0 \frac {d n_0}{d r} + V_0 n_0. \]
and we state that these two forms are equivalent.
In the present memo, we shall made this equivalence explicit.

We note that the first expression for $\Gfs$
involves the flux surface averaged density 
$n(r) = \langle n(r,\theta) \rangle$,
and the corresponding transport coefficients $D$ and $V$.
In contrast, the second expression involves the LFS density $n_0 = n(r,\theta = 0)$
and the corresponding transport coefficients $D_0$ and $V_0$.

We remind that in general
\[ n(r,\theta) = n_0(r) \, \exparg \]
where $\Phi$ is the background electrostatic potential,
defined in such a way that $\Phi(r,\theta = 0) = 0$,
and $R_0(r)$ is the major radius of the location where $n_0$ is
evaluated (usually LFS $\theta = 0$).

We introduce also the auxiliary (species dependent) normalized energy
\[
\Ecf(r,\theta) = \frac{Ze\Phi(r,\theta)}{T(r)} - \frac{m \Omega^2(r)}{2T(r)} \left(R(r,\theta)^2 - R_0(r)^2\right)
\]
For clarity, with circular concentric flux surfaces,
$R(r,\theta) = R_{\rm geo}+r \cos \theta$ and $R_0(r) = R_{\rm geo}+r$.

In general flux tube geometry, $\theta$ is a generalized poloidal angle,
which describes the distance along the field line.
Considering the right handed system of coordinates $(r,\theta, \phi)$,
the flux surface average of the quantity $n(r,\theta)$ is given by
\[
\langle f \rangle = \frac{1}{V^\prime} \oint d\theta d\phi \; \sqrt{g} \; n(r, \theta),
\]
where $g = (\nabla r \times \nabla \theta \cdot \nabla \phi)^{-2}$ is
the determinant of the metric tensor, and ${V^\prime} = d V/ dr$, where
$V(r)$ is the plasma volume up to the flux surface $r$.

Then, the following relationship holds,
\[
\frac {d \langle n(r, \theta) \rangle}{d r} = 
\left \langle{ \frac {\partial n(r, \theta)}{ \partial r} } \right \rangle + n_0(r) \, \calGr,
\]
where we have defined 
\[
\calGr =  \left [ - \frac{1}{V^\prime} \frac{d^2 V}{d r ^2} \langle{n(r, \theta)}\rangle 
+ \frac{1}{V^\prime} \oint { d\theta d\phi \;  n(r, \theta) \, \frac{\partial{\sqrt{g}}}{\partial r}}
\right ] \, n_0(r)^{-1}
\]

that is
\[
\calGr =  - \frac{1}{V^\prime} \frac{d^2 V}{d r ^2} \, \left \langle \expargs \right \rangle 
+ \left \langle \expargs \frac{\partial \log(\sqrt{g})}{\partial r} \right \rangle
\]
Partial derivatives versus the minor radius $r$ are intended to 
be performed at constant $\theta$.
We note that $\calGr = 0$ with a Hamada coordinate system 
(as used in GKW).

In order to derive the relationship between the two pairs
$(D,V)$ and $(D_0,V_0)$, we proceed by expressing the
flux surface averaged density $n(r)$ and its radial
derivative $d n(r)/d(r)$ in terms of $n_0(r)$ and 
$d n_0(r)/d(r)$.
The relationship between $n(r)$ and $n_0(r)$ is straightforward
\[ n(r) = n_0(r) \, \left \langle \exparg \right \rangle \]
In order to express $d n(r)/d(r)$ in terms of $n_0(r)$ and 
$d n_0(r)/d(r)$, we proceed with the computation of 
$ \partial n(r,\theta) / \partial r$.
\begin{eqnarray}
\nparder &=& \exparg \; \left\{ \noder \; + {n_0(r)} \, \cdot \right. \nonumber \\
& \cdot & \left[-\frac{Ze}{T(r)} \Pparder + \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r} \Rsqdiff + \frac{m\Omega^2}{2T(r)}\frac{\partial}{\partial r} \Rsqdiff \right]
\nonumber \\
& + & \left. n_0(r) \Tlnder \left[\frac{Ze}{T(r)} \Phi(r,\theta) - \frac{m\Omega^2}{2T(r)} \Rsqdiff \right] \right\}
\label{eq.nderiv}
\end{eqnarray}

We take now the flux surface average, and recalling
that
\[
\frac{d n(r)}{d r} = \left \langle { \frac{ \partial n(r,\theta)}{\partial r}} \right \rangle + n_0(r)\,\calGr,
\]
we find, 
\begin{eqnarray*}
\nder &=& \left \langle \expargs \right \rangle \noder \,+\\
& + & n_0(r) \left\{ - \left \langle \expargs \, \frac{Ze}{T(r)} \Pparder \right \rangle \right. \\
& + & \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r}\\
& + & \left \langle \expargs \, \frac{\partial}{\partial r} \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)}\\
& + & \left \langle \expargs \, \frac{Ze}{T(r)} \Phi(r,\theta) \right \rangle
\Tlnder \\
& - & \left. \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)} \Tlnder
\right\}
+ n_0(r)\,\calGr.
\end{eqnarray*}

At this point, we replace the expressions of $d n(r) /d r$ and $n(r)$ in the expression for $\Gfs$ and
we obtain
\begin{eqnarray*}
\Gfs &=& -D \left \langle \expargs \right \rangle \noder  + \\
& & - D\, n_0(r)\,\left\{ - \left \langle \expargs \, \frac{Ze}{T(r)} \Pparder \right \rangle \right. \\
& & + \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r}\\
& & + \left \langle \expargs \, \frac{\partial}{\partial r} \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)}\\
& &
+ \left \langle \expargs \, \frac{Ze}{T(r)} \Phi(r,\theta) \right \rangle
\Tlnder \\
& & \left. - \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)} \Tlnder
+\calGr \right\}\\
& & + V \,n_0(r) \, \left \langle \expargs \right \rangle
\end{eqnarray*}

Finally, we compare with equation 
\[ \Gfs = -D_0 \frac {d n_0}{d r} + V_0 n_0, \]
and we identify the following relationships:

\[ D_0 = D \left \langle \expargs \right \rangle, \]
and
\begin{eqnarray*}
V_0 &=& - D \left\{ - \left \langle \expargs \, \frac{Ze}{T(r)} \Pparder \right \rangle \right. \\
& + &  \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r}\\
& + &  \left \langle \expargs \, \frac{\partial}{\partial r} \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)}\\
& + & \left \langle \expargs \, \frac{Ze}{T(r)} \Phi(r,\theta) \right \rangle
\Tlnder \\
& - & \left. \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)} \Tlnder
\,+\, \calGr
\right\}\\
& + &  V \left \langle \expargs \right \rangle.
\end{eqnarray*}

The final relationships follow directly
\[ D = D_0 \left \langle \expargs \right \rangle^{-1} = D_0 \frac{n_0}{n}, \]

\begin{eqnarray*}
V &=& V_0 \left \langle \expargs \right \rangle^{-1} +
D_0 \left\{ - \left \langle \expargs \, \frac{Ze}{T(r)} \Pparder \right \rangle \right. \\
& + &  \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r}\\
& + & \left \langle \expargs \, \frac{\partial}{\partial r} \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)}\\
& + & \left \langle \expargs \, \frac{Ze}{T(r)} \Phi(r,\theta) \right \rangle
\Tlnder \\
& - & \left. \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)} \Tlnder 
\,+\, \calGr \right\} \, \left \langle \expargs \right \rangle^{-2}
\end{eqnarray*}


We define the following conversion factor for the convection,
\begin{eqnarray*}
{\cal V} &=& a \left\{ - \left \langle \expargs \, \frac{Ze}{T(r)} \Pparder \right \rangle \right. \\
& + &  \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r}\\
& + & \left \langle \expargs \, \frac{\partial}{\partial r} \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)}\\
& + & \left \langle \expargs \, \frac{Ze}{T(r)} \Phi(r,\theta) \right \rangle
\Tlnder \\
& - & \left. \left \langle \expargs \, \Rsqdiff \right \rangle \frac{m\Omega^2}{2T(r)} \Tlnder 
\,+\, \calGr
\right\} \, \left \langle \expargs \right \rangle^{-1},
\end{eqnarray*}

by which the transformation from the pair $(D_0, V_0)$ into the pair $(D, V)$
simply reads
\begin{eqnarray} 
D &=& \left \langle \expargs \right \rangle^{-1} D_0 = \frac{n_0}{n} D_0, \\
a V &=&  \left \langle \expargs \right \rangle^{-1} \left( a V_0 \,+\,
D_0 \cal{V} \right).
\label{eq.DVtransform}
\end{eqnarray}

Here we have introduced an arbitrary normalizing length $a$
(usually the geometrical major radius $R_{geo}$ in GKW and 
the minor radius $a$ in NEO).

The quantity $\cal{V}$ can be directly computed inside codes
which treat poloidal aysmmetries (like GKW and NEO).
It is suggested that the quantity $\cal{V}$ is computed for
each species independently.

Thereby, for each kinetic species, codes like GKW and NEO 
can provide directly in output the two flux surface averaged 
quantities $n_0 / n$ (already present in output actually)
and the convection conversion factor $\cal{V}$.
In this way the equation to be applied in 1D 
transport codes to pass from $(D_0, V_0)$ to $(D, V)$ is
very simple.

Optionally, codes like GKW and NEO can also provide in output 
the entire set of flux surface averaged quantities which are required to
perform the transformation from the pair $(D_0, V_0)$ into the pair $(D, V)$,
\[ e_0 = \left \langle \expargs \right \rangle ,\]
\[ e_1 = \left \langle \expargs \, \frac{Ze}{T(r)} \Phi(r,\theta) \right \rangle, \]
\[ e_2 = a \left \langle \expargs \, \frac{Ze}{T(r)} \Pparder \right \rangle, \]
\[ e_3 = a^{-2} \left \langle \expargs \, \Rsqdiff \right \rangle, \]
\[ e_4 = a^{-1} \left \langle \expargs \, \frac{\partial}{\partial r} \Rsqdiff \right \rangle, \]
\[ e_5 = a \, \calGr = a \left [
- \frac{1}{V^\prime} \frac{d^2 V}{d r ^2} \, e_0
\,+\, \left \langle \expargs \frac{\partial \log(\sqrt{g})}{\partial r} \right \rangle
\right] 
\]
where we included normalizations to an arbitrary length $a$, which can be chosen
in order to be consistent with the other normalizations used in the codes
(that is, e.g., major radius in GKW, and minor radius in NEO). We remind
that $e_5 = 0$ with Hamada coordinates, as used in GKW.
Then, the convection conversion factor reads
\[
{\cal V} = \frac{1}{e_0} \left\{ - e_2 
+ e_3 a^3 \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r}
+ e_4 a^2 \frac{m\Omega^2}{2T(r)} 
+ e_1 a \Tlnder 
- e_3 a^3 \frac{m\Omega^2}{2T(r)} \Tlnder + e_5
\right\}.
\]
and the transformation equations read
\[ D =  \frac{D_0}{e_0} = \frac{n_0}{n} D_0,\]
\[ a V =  {\left( a V_0 \,+\,D_0 \cal{V} \right)}\,{e_0}^{-1}.\]

We notice that the convection conversion factor $\cal{V}$ has
been defined in such a way that
\[ \frac{a V}{D} = \frac{a V_0}{D_0}+{\cal V}, \]
which can also be regarded as a direct consequence of the following relationship
\[ \frac{a}{n} \frac{d n}{d r} = \frac{a}{n_0} \frac{d n_0}{d r} + {\cal V}. \]

\subsection{Anisotropic minorities (Reinke model) \label{sec.memo-icrh}}

In the Reinke model (see Sec. \ref{sec.icrh}), the poloidal density variation for the anisotropic minority is
\begin{equation}
 n(r,\theta) = n_0 \left( \frac{B}{B_0} \right)^{-\eta} \exp(-{\cal E}_{\Omega}/T_\parallel) = n_0 \exp(-{\cal E}_{\Omega,\eta}/T_\parallel)
\end{equation}
The radial deriviative then has additional terms 
\begin{equation}
\frac{\partial n(r,\theta)}{\partial r} = n_0
%\left(\frac{B_0}{B}\right)^\eta e_\eta 
\left[ -\ln(B/B_0)\frac{\partial \eta}{\partial r} - \frac{\eta}{B}\frac{\partial B}{\partial r} + \frac{\eta}{B_0}\frac{\partial B_0}{\partial r} \right]
\exp(-{\cal E}_{\Omega,\eta}/T_\parallel) + \cdots
\end{equation}
where $\cdots$ represents all the terms on the RHS of equation \ref{eq.nderiv} with the generalised normalised energy $E = {\cal E}_{\Omega,\eta}/T_\parallel $
in place of the former $E = {\cal E}_{\Omega}/T $ (as defined in Eq. \ref{eq.poloidal-energy}).  The coefficients $e_0,e_1,e_2,e_3,e_4,e_5$ are also generalised 
using this generalised energy in their definition.
Then, the transformation identities above all still hold if $\cal{V}$ is extended to
\begin{equation}
{\cal V} = \frac{1}{e_0} \left\{ - e_2 
+ e_3 a^3 \frac{m\Omega}{T(r)} \frac{d \Omega(r)}{d r}
+ e_4 a^2 \frac{m\Omega^2}{2T(r)} 
+ e_1 a \Tlnder 
- e_3 a^3 \frac{m\Omega^2}{2T(r)} \Tlnder + e_5 + e_\eta
\right\}.
\label{eq.vcalex}
\end{equation}
where the additional coefficient $e_\eta$ is defined as
\[
e_\eta = a \left \langle \exp\left\{-{\cal E}_{\Omega,\eta}(r,\theta)/T_\parallel\right\} \left[ -\ln(B/B_0)\frac{\partial \eta}{\partial r} - \frac{\eta}{B}\frac{\partial B}{\partial r}
+ \frac{\eta}{B_0}\frac{\partial B_0}{\partial r} \right] \right \rangle.
\].

\subsection{Anisotropic minorities (Bilato model)}
%spellcheck me
In the Bilato model (see Sec. \ref{sec.icrh2}), the poloidal density variation for the anisotropic minority is
\begin{equation}
 n_m = n_0 \frac{T_{\perp}(\theta)}{T_{\perp 0}} \exp(-{\cal E}_{\Omega}/T_\parallel) = n_0 \exp(-{\cal E}_{\Omega,\eta}/T_\parallel)
\end{equation}
where
\begin{equation}
 \frac{T_{\perp}(\theta)}{T_{\perp 0}} = 
\left [ \frac{T_{\perp 0}}{T_{\parallel}} + \left(1 - \frac{T_{\perp 0}}{T_{\parallel}} \right )\frac{B_{0}}{B} \right ]^{-1}.
\end{equation}
The radial deriviative then has additional terms 
\begin{equation}
\frac{\partial n(r,\theta)}{\partial r} = -n_0
%\left(\frac{B_0}{B}\right)^\eta e_\eta 
 \left[ \frac{\partial (T_{\perp 0}/T_\parallel)}{\partial r} \left(1- \frac{B_{0}}{B} \right )
+ \left( -\frac{B_{0}}{B^2}\frac{\partial B}{\partial r} 
+ \frac{1}{B}\frac{\partial B_{0}}{\partial r} \right) 
\left(1- \frac{T_{\perp 0}}{T_\parallel}\right)
 \right] \frac{T_\perp(\theta)}{T_{\perp 0}} 
\exp(-{\cal E}_{\Omega,\eta}/T_\parallel) + \cdots
\end{equation}
where $\cdots$ represents all the terms on the RHS of equation \ref{eq.nderiv} with the generalised normalised energy $E = {\cal E}_{\Omega,\eta}/T_\parallel $
in place of the former $E = {\cal E}_{\Omega}/T $ (as defined in Eq. \ref{eq.poloidal-energy2}).  The coefficients $e_0,e_1,e_2,e_3,e_4,e_5$ are also generalised 
using this generalised energy in their definition.
Then $\cal{V}$ is extended as in Eq. \ref{eq.vcalex} with the additional coefficient $e_\eta$ is defined as
\[
e_\eta = -a \left \langle \exp\left\{-{\cal E}_{\Omega,\eta}(r,\theta)/T_\parallel\right\} 
 \left[ \frac{\partial (T_{\perp 0}/T_\parallel)}{\partial r} \left(1- \frac{B_{0}}{B} \right )
+ \left( -\frac{B_{0}}{B^2}\frac{\partial B}{\partial r} 
+ \frac{1}{B}\frac{\partial B_{0}}{\partial r} \right) 
\left(1- \frac{T_{\perp 0}}{T_\parallel}\right)
 \right] \frac{T_\perp(\theta)}{T_{\perp 0}} 
 \right \rangle.
\]

\pagebreak

%\end{document}

% related to auctex mode and latex-preview-mode in Emacs:
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "doc"
%%% End:
