\chapter*{Preface}

This documentation comes with the GKW code. It is largely based on our 2009 paper in Computer Physics Communications Ref. \cite{CPC-paper},
but contains additional material and descriptions of additional features that the code has aquired since that paper.
In this manual GKW is presented 
with the aim of documenting the exact equations solved, presenting the essential benchmarks which 
document the proper numerical solution, and explaining the code structure and installation.  
This document is evolving along with the code, and 
currency and completeness presently take precedence over integration and polish of all sections.  
GKW developers are strongly urged to update this document (\doc{tex/doc.tex}) to reflect changes made in the code repository.


\section*{What is GKW?}

Gyrokinetic Workshop (GKW) is a code for the simulation of microinstabilities and turbulence in a magnetically confined plasma.
%The code incorporates all physics effects that can be expected from a state of the art gyrokinetic simulation code in the 
%local limit: kinetic electrons, electromagnetic effects, collisions, full general geometry with a coupling to a MHD equilibrium code, 
%and ExB shearing.
%In addition the physics of plasma rotation has been implemented through a formulation of the gyrokinetic equation in the co-moving %system. 
The gyrokinetic model is is five dimensional partial differential equation and its solution requires a massively parallel approach. 
GKW is written in Fortran 95 and has been parallelised using MPI, which enables it to scale well up to 32768+ cores.
The code was initially developed at the University of Warwick in 2007 through the extension of the linear code
LINART \cite{PEE04}, and can be used both in the linear as well as in the nonlinear regime. In 2016 it has been renamed from 'Gyrokinetics at Warwick' to 'Gyrokinetic Workshop', to reflect the nonlocality of its developers and users.
The code has been freely available online since 2010,
and has been hosted at \href{https://bitbucket.org/gkw/gkw}{https://bitbucket.org/gkw/gkw},
since 2015, where it is actively maintained.

\section*{Can I use GKW?}

You can obtain and use GKW for any purpose with minimal restrictions.
Essentially you can make any changes to the source, and are not obliged to inform us or communicate 
these changes back to us (although we would welcome it if you did).  
Furthermore, you are free to redistribute the code to others under the terms of the
license (see the LICENSE file), which is the GNU General Public License version 3.

We politely request that the effort made by us is recognised. 
In any publication that uses the results of GKW, either directly or indirectly,
you should cite the CPC paper \cite{CPC-paper}. (If you want to be kind to us you could additionally
cite the original LINART paper Ref. \cite{PEE04} and/or the first paper in which GKW has been
used Ref. \cite{PEE07}).

%N.B. This section only works because of the present document style and the ordering of the references above.
\subsection*{References}

\begin{enumerate}
\item A.G. Peeters, Y. Camenen, F.J. Casson, W.A. Hornsby, A.P. Snodin, D. Strintzi, G. Szepesi,\\
Computer Physics Communications, {\bf 180}, \href{http://dx.doi.org/10.1016/j.cpc.2009.07.001}{2650}, (2009)
\item A.G. Peeters, D. Strintzi, Phys. Plasmas {\bf 11},  \href{http://dx.doi.org/10.1063/1.1762876}{3748}, (2004)
\item A.G. Peeters, C. Angioni, D. Strintzi, Phys. Rev. Lett. {\bf 98},  \href{http://dx.doi.org/10.1103/PhysRevLett.98.265003}{265003}, (2007)
\end{enumerate}

\pagebreak


\section*{What does GKW do?}

GKW aims at solving the turbulent transport problem arising in tokamak plasmas. It solves
the gyrokinetic (the kinetic Vlasov equation averaged over the fast gyro-motion) equation on a fixed
grid in the 5-dimensional space using a combination of finite difference and pseudo spectral
methods. The solution of the kinetic equations governing this problem is an active area of 
research, with many groups worldwide contributing. The two approaches used are Particle in Cell 
(PIC) codes \cite{PAR93,LIN98,IDO03,HEI04,BOT07,JOL07,GRA06}, and Vlasov codes 
\cite{KOT95,DOR00,JEN00,DAN05,CAN03}. GKW obviously falls in the latter category, and 
reaches a standard that can be expected from a modern Vlasov code, i.e. it includes kinetic 
electrons, a general description of the geometry with a coupling to a MHD equilibrium solver, 
electro-magnetic effects, collisions, and the effect of ExB shearing. In addition the physics of 
toroidal plasma rotation is included through a formulation of the equations in the co-moving system.   
The physics of rotation is currently an active area of research and many of the applications of 
GKW have concentrated on the transport of toroidal momentum \cite{PEE07,PEE05,PEE06,
PEE09com,PEE09,CAM09,PEE09scmode,CAM09pop,CAS09}, the influence of rotation on 
other transport channels \cite{CAM09rot,CAS10}, and the interaction between turbulence and islands \cite{HOR10,HOR10epl}.
% Update the references above

\section*{Code summary}

{\bf Licensing provisions:} GNU GPLv3

{\bf Programming language:} Fortran 95

{\bf RAM:} $\sim$~128MB-1GB for a linear run; 25GB for typical nonlinear kinetic run (30 million grid points).

{\bf Number of processors used:} 1, although the program can efficiently utilise 4096+ processors (spectral), depending on problem and available computer. 128 processors is reasonable for a typical nonlinear kinetic run on the latest x86-64 machines.  The nonspectral scheme can efficiently scale to 32768+ processors.

{\bf External routines/libraries:}  None required, although the functionality of the program is somewhat
limited without a MPI implementation (preferably MPI-2) and the FFTW3 library.  In addition, the nonspectral and
implicit schemes require the UMFPACK and AMD libraries, and the eigenvalue solver requires the SLEPc and PETSc libraries. Furthermore, GKW can be compiled to support the HDF5 output format.

{\bf Standard solution method:} Pseudo-spectral and finite difference with explicit time integration.

{\bf Running time:} (On recent x86-64 hardware) $\sim$5 minutes for a short linear problem; 4000
hours for typical nonlinear kinetic run.

{\bf Geometry Interface:}  The MHD equilibrium code CHEASE \cite{LUT96} is used for the general geometry calculations. This code has been developed in CRPP Lausanne and
is not distributed together with GKW, but can be downloaded separately. The geometry module of GKW is based on the version 7.3 of CHEASE, which includes the output for Hamada coordinates.\\

\setlength{\parskip}{0pt}
\setlength{\parindent}{0pt}

\tableofcontents

\setlength{\parskip}{8pt}
\setlength{\parindent}{0pt}

\newpage
 

% related to auctex mode and latex-preview-mode in Emacs:
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "doc"
%%% End:
