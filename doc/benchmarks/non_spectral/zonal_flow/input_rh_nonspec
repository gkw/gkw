! This input file can be used to recreate the Rosenbluth Hinton residual result
! See   F.L. Hinton, M.N. Rosenbluth, Phys. Rev. Lett. 80 (1998) 724
! Also  Y. Xiao, P.J. Catto, Phys. Plasmas 13 (2006) 102311
! Also  J. Candy, R.E. Waltz, J. Comp. Phys. 186 (2003) 545
!
! To quickly obtain the potential output needed, use
! write(i_kxspec,fmt = '(256(es13.5,1x))') sum(phi(1,1,:))/n_s_grid
! in diagostic/phi_ky_spec
!
! This is not the same input file used for the CPC paper result,
! but is recreated as close as possible from reading the parameters in the paper
! Runs well on 8-32 processors 

&CONTROL 
 testing = .false.,
 silent = .true.,
 order_of_the_scheme = 'fourth_order'
 parallel_boundary_conditions = 'zero'
 matrix_format = 'complex'
 READ_FILE  = .false.
 NON_LINEAR = .false.
 zonal_adiabatic=.true.
 METHOD = 'EXP',
 METH   = 2, 
 DTIM=0.01
 NTIME=5000
 NAVERAGE=5
 normalized=.false.
 nlapar=.false.
 disp_par=0.1
 disp_vp=0.1
 lverbose = .false.
 spectral_radius = .false.
 min_gr = -100
 /
 &GRIDSIZE
 n_procs_mu = 8
 n_procs_vpar = 4
 NX = 400,   
 N_s_grid=64
 N_mu_grid=8
 nperiod=1
 N_vpar_grid=64
 NMOD = 2	
 number_of_species=1
 lx = 222.150640349221
 /
 &MODE
 CHIN    = 0., 
 kthrho=0.43
 mode_box = .true. 
 krhomax=0.4
 ikxspace=10
 /
 &GEOM
 shat=0.1
 Q    = 1.3, 
 EPS  = 5.000000e-02,
 /
 &SPCGENERAL
 finit='zonal'
 beta = 3e-4,
 adiabatic_electrons=.true.
 /
 &SPECIES 
 MASS  = 1, 
 Z     = 1.0, 
 TEMP  = 1.0, 
 dens  = 1.0,
 rlt=1
 rln=1
 uprim = 0.0,
 / 
 &SPECIES 
 MASS  = 2.77e-4, 		!sqrt(mi/me)=60
 Z     = -1.0, 
 TEMP  = 1.0,
 dens  = 1.0,
 rlt=1
 rln=1
 uprim = 0.0,
 / 
 &ROTATION
 VCOR = 0.000000e+00
 shear_rate=0.0
 cf_trap=.true.
 cf_drift=.true.
 / 
