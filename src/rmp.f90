!-----------------------------------------------------------------------
!> Contains initialization of the three-dimensional magnetic field 
!> perturbation.
!-----------------------------------------------------------------------
module rmp

  implicit none

  private
  
  public :: rmp_init, initialize_rmp
  public :: rmp_struct, rmp_indx, rmp_phi_indx, rmp_phi_struct
  
  ! three-dimensional magnetic field structures and indexing arrays
  complex, save, allocatable :: rmp_struct(:,:)
  complex, save, allocatable :: rmp_phi_struct(:,:)
  integer, save, allocatable :: rmp_indx(:,:)
  integer, save, allocatable :: rmp_phi_indx(:,:)
  complex, save, allocatable :: rmp_res(:)
  
  ! parity types 
  integer, save :: RMP_GENERIC = 1
  integer, save :: RMP_PARITY_EVEN = 2
  integer, save :: RMP_PARITY_ODD = 3
  integer, save :: RMP_FINITE_KX_SIN = 4
  integer, save :: RMP_FINITE_KX_COS = 5
  
contains
  
  
subroutine rmp_init

  ! The three-dimensional magnetic perturbation is initialised as a 
  ! perturbation in the parallel vector potential.
  call initialize_rmp

end subroutine rmp_init


!-----------------------------------------------------------------------
!> Subroutine that initializes the three-dimensional magnetic 
!> perturbation.
!-----------------------------------------------------------------------
subroutine initialize_rmp
  
  use components,     only : l_rmp, rmp_imod, rmp_ampl, rmp_parity
  use components,     only : rmp_ls, rmp_polmod, wstar, rmp_radmod
  use components,     only : l_rmp_phi, rmp_phi_fac, rmp_phi_phase
  use components,     only : rmp_phi_model, rmp_phase, l_rmp_approx
  use components,     only : psi_0, delta_psi_0
  use control,        only : spectral_radius
  use grid,           only : ns, nx, n_x_grid, gx
  use mode,           only : ixzero, iyzero, ikxspace, krho
  use geom,           only : sgr, shat, q, dxgr, shift_end_grid
  use geom,           only : efun, ffun, isg_lfs, eps
  use dist,           only : iapar, iphi
  use index_function, only : indx
  use constants,      only : pi, ci1
  use general,        only : gkw_abort
  
  integer :: i, ierr, ix, modnum, p, iplusmode, iminusmode
  complex :: dumbuf
  real :: aparamp, balloonpos, ix_mid, psi_tmp

  if (.not.l_rmp) return
  
  
  
  ! Allocate the rmp structure and the rmp index.
  allocate(rmp_struct(nx,ns),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate the array &
                         & rmp_struct in initialize_rmp')
  allocate(rmp_phi_struct(nx,ns),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate the array &
                         & rmp_phi_struct in initialize_rmp')
  allocate(rmp_indx(nx,ns),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate the array &
                             & rmp_indx in initialize_rmp')
  allocate(rmp_phi_indx(nx,ns),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate the array &
                             & rmp_phi_indx in initialize_rmp')
  allocate(rmp_res(ns),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate the array &
                             & rmp_res in initialize_rmp')
  
  if (spectral_radius) then
  
  ! Initialized the structure of the trhee-dimensional magnetic perturbation 
  ! and the index.
  if(rmp_parity == RMP_PARITY_EVEN .or. rmp_parity == RMP_PARITY_ODD) then
  do i=1, ns
    do ix=1, nx    
      ! the index
      rmp_indx(ix,i) = indx(iapar,iyzero+rmp_imod,ix,i)
      if(l_rmp_phi) then
        rmp_phi_indx(ix,i) = indx(iphi,iyzero+rmp_imod,ix,i)
      end if
      
      ! the mode structure
      if(rmp_parity == RMP_PARITY_EVEN) then
        if(ix == ixzero) then
          rmp_struct(ix,i) = rmp_ampl * exp(-(2.*pi*sgr(i)) * (2.*pi*sgr(i)))
        else
          rmp_struct(ix,i) = (0.E0, 0.E0)
        end if
      else if(rmp_parity == RMP_PARITY_ODD) then
        if(ix == ixzero) then
        rmp_struct(ix,i) = rmp_ampl * (2.*pi*sgr(i)) &
                      & * exp(-(2.*pi*sgr(i)) * (2.*pi*sgr(i)))
        else
          rmp_struct(ix,i) = (0.E0, 0.E0)
        end if
      end if
      
    end do
  end do
  end if
 
  
  ! generic definition of a magnetic perturbation
  if(rmp_parity == RMP_GENERIC) then
  
    ! This complex factor discerns between resonant or non-resonant 
    ! perturbation.
    do i=1, ns
      rmp_res(i) = exp(ci1 * 2.E0 * pi * rmp_polmod * sgr(i)) &
                 & * exp(ci1 * rmp_phase * pi)
    end do
    
    ! the amplitude of the rmp
    aparamp = wstar**2 * shat / (4.E0 * q)
    
    do ix=1, nx
      
      ! the signed radial mode number
      p = ix - ixzero
      
      ! for p=0 ballooning position can obey zero values
      ! at s=0, hence a special treatment is necessary
      if(p == 0) then
        
        do i=1,ns
          if (abs(sgr(i)).lt.1e-10) then
            ! Set rmp amplitude explicitly through analytical expression 
            ! [sin(x) / x -> 1 for x -> 0], since it contains division 
            ! by zero.
            dumbuf = aparamp * (1.E0,0.E0) / 2.E0
          else             
            balloonpos = ikxspace * rmp_imod * sgr(i) + 1.E0 * p
            dumbuf = (1.E0,0.E0) * aparamp * exp(-(balloonpos/rmp_ls)**2) &
                   & * sin(pi*balloonpos) / (2.E0 * pi * balloonpos)
          end if
          ! modification that makes three-dimensional perturbation consistent for 
          ! arbitrary geometry
          if (.not. l_rmp_approx) then
            dumbuf = dumbuf * (ffun(ix,i)/efun(ix,i,1,2)) * (q**2/(2*eps)) 
          end if
          rmp_struct(ix,i) = rmp_res(i) * dumbuf
          rmp_indx(ix,i) = indx(iapar,iyzero+rmp_imod,ix,i)
        end do
      
      ! no special treatment
      else
        do i=1,ns
          balloonpos = ikxspace * rmp_imod * sgr(i) + 1.E0 * p
          dumbuf = (1.E0,0.E0)*aparamp*exp(-(balloonpos/rmp_ls)**2)* &
                 & sin(pi*balloonpos) / (2.E0 * pi * balloonpos) 
          ! modification that makes three-dimensional  perturbation consistent for 
          ! arbitrary geometry
          if (.not. l_rmp_approx) then
            dumbuf = dumbuf * (ffun(ix,i)/efun(ix,i,1,2)) * (q**2/(2*eps)) 
          end if
          rmp_struct(ix,i) = rmp_res(i) * dumbuf
          rmp_indx(ix,i) = indx(iapar,iyzero+rmp_imod,ix,i)
        end do
      end if
    
    end do
    
    ! Initialize the phi perturbation as a in an equivalent way. 
    if(l_rmp_phi) then
      do ix=1, nx
      
        ! shift p by rmp_radmod to allow for finite radial wavenumber of 
        ! the potential perturbation
        p = ix - ixzero + rmp_radmod
        
        ! for p=0 ballooning position can obey zero values
        ! at s=0, hence a special treatment is necessary
        if(p == 0) then
          
          do i=1,ns
            if (abs(sgr(i)).lt.1e-10) then
              ! Set rmp amplitude in the phi field at s=0 explicitly 
              ! through analytical expression [sin(x) / x -> 1 for x -> 0], 
              ! since it contains division by zero.
              dumbuf = aparamp * (1.E0,0.E0) / 2.E0
            else             
              balloonpos = ikxspace * rmp_imod * sgr(i) + 1.E0 * p
              dumbuf = (1.E0,0.E0) * aparamp * exp(-(balloonpos/rmp_ls)**2) &
                     & * sin(pi*balloonpos) / (2.E0 * pi * balloonpos)
            end if
            ! modification that makes three-dimensional  perturbation consistent for 
            ! arbitrary geometry
            if (.not. l_rmp_approx) then
              dumbuf = dumbuf * (ffun(ix,i)/efun(ix,i,1,2)) * (q**2/(2*eps)) 
            end if
            rmp_phi_struct(ix,i) = rmp_res(i) * dumbuf
            rmp_phi_indx(ix,i) = indx(iphi,iyzero+rmp_imod,ix,i)
          end do
        
        ! no special treatment
        else
          do i=1,ns
            balloonpos = ikxspace * rmp_imod * sgr(i) + 1.E0 * p
            dumbuf = (1.E0,0.E0)*aparamp*exp(-(balloonpos/rmp_ls)**2)* &
                   & sin(pi*balloonpos) / (2.E0 * pi * balloonpos)  
            ! modification that makes three-dimensional  perturbation consistent for 
            ! arbitrary geometry
            if (.not. l_rmp_approx) then
              dumbuf = dumbuf * (ffun(ix,i)/efun(ix,i,1,2)) * (q**2/(2*eps)) 
            end if
            rmp_phi_struct(ix,i) = rmp_res(i) * dumbuf
            rmp_phi_indx(ix,i) = indx(iphi,iyzero+rmp_imod,ix,i)
          end do
        end if
      
      end do
      
      ! Scale by rmp_phi_fac and apply phase factor
      rmp_phi_struct = rmp_phi_fac * exp(ci1*pi*rmp_phi_phase) * rmp_phi_struct
      
    end if ! l_rmp_phi
  
  end if
  
  ! initialization of smallest kx components only
  if(rmp_parity == RMP_FINITE_KX_SIN .or. &
    & rmp_parity == RMP_FINITE_KX_COS) then 
  
    ! This complex factor discerns between resonant or non-resonant perturbation.
    do i=1, ns
      if(rmp_parity == RMP_FINITE_KX_SIN) then
        rmp_res(i) = (1.E0, 0.E0) * sin(2.E0 * pi * rmp_polmod * sgr(i))
      elseif(rmp_parity == RMP_FINITE_KX_COS) then
        rmp_res(i) = (1.E0, 0.E0) * (cos(2.E0 * pi * rmp_polmod * sgr(i)) + 1) / 2.E0
      end if
    end do
    
    ! the amplitude of the rmp
    aparamp = wstar**2 * shat / (4.E0 * q)
  
  
    do i=1,ns
    
      ! if rmp_radmode = 0 -> ixzero is to be initialized
      if (rmp_radmod == 0) then
        dumbuf = (1.E0,0.E0) * aparamp
      ! otherwise initialize ixzero mode to zero
      else             
        dumbuf = (0.E0,0.E0)
      end if
      rmp_struct(ixzero,i) = rmp_res(i) * dumbuf
      rmp_indx(ixzero,i) = indx(iapar,iyzero+rmp_imod,ixzero,i)
      if(l_rmp_phi) then
        rmp_phi_indx(ixzero,i) = indx(iphi,iyzero+rmp_imod,ixzero,i)
      end if
    end do
             
    ! number of non-zero positive wave vector radial modes
    modnum = (nx-1)/2
           
    if (nx.gt.1) then
      ! This is loop over x modes
      ! Initialise symmetric in kx.
      do p = 1,modnum
        iplusmode = ixzero+p
        iminusmode = ixzero-p
        do i=1,ns
          ! if rmp_radmod != 0 -> set corresponding finite kx mode
          if(p == rmp_radmod) then
            dumbuf = (1.E0,0.E0)*aparamp*exp(-(p/rmp_ls)**2) / 2.E0              
            ! Initialise rmp amplitude in connected x modes               
            !in the rmp struct that will be re-applied
            rmp_struct(iplusmode,i) = rmp_res(i) * dumbuf
            rmp_indx(iplusmode,i) = indx(iapar,iyzero+rmp_imod,iplusmode,i)
            if(l_rmp_phi) then
              rmp_phi_indx(iplusmode,i) = indx(iphi,iyzero+rmp_imod,iplusmode,i)
            end if
                   
            dumbuf = (1.E0,0.E0)*aparamp*exp(-(-p/rmp_ls)**2) / 2.E0 
            ! And do the same in the kx refelction
            ! and in the island struct that will be re-applied
            rmp_struct(iminusmode,i) = rmp_res(i) * dumbuf
            rmp_indx(iminusmode,i) = indx(iapar,iyzero+rmp_imod,iminusmode,i)
            if(l_rmp_phi) then
              rmp_phi_indx(iminusmode,i) = indx(iphi,iyzero+rmp_imod,iminusmode,i)
            end if
            
          ! initialize higher kx components to zero
          else
            dumbuf = (0.E0,0.E0)              
            ! Initialise rmp amplitude in connected x modes               
            !in the rmp struct that will be re-applied
            rmp_struct(iplusmode,i) = rmp_res(i) * dumbuf
            rmp_indx(iplusmode,i) = indx(iapar,iyzero+rmp_imod,iplusmode,i)
            if(l_rmp_phi) then
              rmp_phi_indx(iplusmode,i) = indx(iphi,iyzero+rmp_imod,iplusmode,i)
            end if
                   
            dumbuf = (0.E0,0.E0)
            ! And do the same in the kx refelction
            ! and in the island struct that will be re-applied
            rmp_struct(iminusmode,i) = rmp_res(i) * dumbuf
            rmp_indx(iminusmode,i) = indx(iapar,iyzero+rmp_imod,iminusmode,i)
            if(l_rmp_phi) then
              rmp_phi_indx(iminusmode,i) = indx(iphi,iyzero+rmp_imod,iminusmode,i)
            end if
          end if
        end do
      end do
    end if
    
  end if
  
  else ! not spectral radius
    
    ! the amplitude of the magnetic perturbation
    aparamp = wstar**2 * shat / (4.E0 * q)
    
    ! center of the radial domain
    ix_mid = real((n_x_grid+1)*0.5E0)

    
    do i=1,ns; do ix = 1,nx
    
        ! gobal position on the radial grid
        psi_tmp = dxgr * (real(gx(ix))-ix_mid)
        
        dumbuf = 0.5E0 * aparamp * exp(-ci1 * krho(iyzero+rmp_imod) &
               & * shift_end_grid(ix) * sgr(i)) &
               & * exp(ci1 * 2.0E0 * pi * rmp_polmod * sgr(i)) &
               & * exp(ci1 * rmp_phase * pi) &
               & * 0.25E0 * (1+tanh((psi_tmp+psi_0)/delta_psi_0)) &
               & * (1-tanh((psi_tmp-psi_0)/delta_psi_0)) 
        ! modification that makes three-dimensional  perturbation consistent for 
        ! arbitrary geometry
        if (.not. l_rmp_approx) then
          dumbuf = dumbuf * (ffun(ix,i)/efun(ix,i,1,2)) * (q**2/(2*eps)) 
        end if
        rmp_struct(ix,i) = dumbuf
        rmp_indx(ix,i)   = indx(iapar,iyzero+rmp_imod,ix,i)
        
      end do; end do
  
  end if 
  
  
end subroutine initialize_rmp


end module rmp 
