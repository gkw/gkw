!-----------------------------------------------------------------------
!> Energy evolution diagnostics:
!>
!>---- FURTHER NOTES ---------------------------------------------------
!>
!-----------------------------------------------------------------------
module diagnos_energy_evo
  use control, only : lcalc_energy_evo

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem, calc_smallstep
  public :: output

  !> The switch for this diagnostic must be defined in the module control,
  !> for dependency reasons.
  !> The following line makes it possible
  !> to write 'use diagnos_energy_evo, only : lcalc_energy_evo' .
  public :: lcalc_energy_evo

  
  !> switch for ky output
  logical, save, public :: ky_ene_evo

  
  !> switch for output of drift- and landau term
  logical, save, public :: ene_drift
  logical, save, public :: ene_landau

  !> switch for kykx output
  logical, save, public :: kykx_ene_evo
  

  integer, save :: i_ene_e = -1, i_ene_m = -1, i_ene_f
  integer, save :: i_dt_ene_e = -1, i_dt_ene_m = -1, i_dt_ene_f
  integer, save :: i_ene_f_coll = -1, i_ene_f_num_perp =-1
  integer, save :: i_ene_f_num_dis = -1, i_ene_f_num_vp = -1
  integer, save :: i_ene_f_landau = -1, i_ene_f_drift = -1
  
  integer, save :: i_ene_f_kykx = -1
  integer, save :: i_ene_e_kykx = -1
  integer, save :: i_ene_m_kykx = -1
  
  integer, save :: i_dt_ene_f_kykx = -1
  integer, save :: i_dt_ene_e_kykx = -1
  integer, save :: i_dt_ene_m_kykx = -1
  
  integer, save :: i_ene_f_twist = -1, i_ene_e_twist = -1
  integer, save :: i_ene_f_source = -1, i_ene_f_source_m = -1
  integer, save :: i_ene_f_disp = -1, i_ene_e_disp = -1
  
  integer, save :: i_ene_f_landau_kykx = -1, i_ene_f_drift_kykx = -1
  integer, save :: i_ene_f_source_kykx = -1, i_ene_f_source_m_kykx = -1
  integer, save :: i_ene_f_disp_kykx = -1, i_ene_e_disp_kykx = -1
  integer, save :: i_ene_f_twist_kykx = -1, i_ene_e_twist_kykx = -1
  
  
  !> time between call of calc_smallstep() and output()
  real, save :: delta_time_energetics = 1.
  

  !> quantities with kzeta dependence
  real, allocatable :: ene_f(:), ene_e(:), ene_m(:)
  real, allocatable :: last_ene_f(:), last_ene_e(:), last_ene_m(:)
  real, allocatable :: dt_ene_f(:), dt_ene_e(:), dt_ene_m(:)
  real, allocatable :: ene_f_num_dis(:), ene_f_num_vp(:), ene_f_num_perp(:)
  real, allocatable :: ene_f_coll(:), ene_f_twist(:), ene_e_twist(:)
  real, allocatable :: ene_f_drift(:), ene_f_landau(:)
  real, allocatable :: ene_f_source(:), ene_f_source_m(:)
  real, allocatable :: ene_f_disp(:), ene_e_disp(:)

  
  !> quantities with length nsolc
  complex, allocatable :: num_disp01(:), num_disp02(:), num_disp03(:)
  complex, allocatable :: collisionop(:)
  complex, allocatable :: term01(:), term05(:), term05_m(:), term08(:)
  
  
  !> quantities with kzeta and kpsi dependence
  real, allocatable :: ene_f_twist_kykx(:,:)
  real, allocatable :: ene_e_twist_kykx(:,:)
  real, allocatable :: ene_f_drift_kykx(:,:)
  real, allocatable :: ene_f_landau_kykx(:,:)
  real, allocatable :: ene_f_disp_kykx(:,:)
  real, allocatable :: ene_e_disp_kykx(:,:)
  real, allocatable :: ene_f_source_kykx(:,:)
  real, allocatable :: ene_f_source_m_kykx(:,:)
  
  real, allocatable :: ene_f_kykx(:,:)
  real, allocatable :: ene_e_kykx(:,:)
  real, allocatable :: ene_m_kykx(:,:)
  
  real, allocatable :: last_ene_f_kykx(:,:)
  real, allocatable :: last_ene_e_kykx(:,:)
  real, allocatable :: last_ene_m_kykx(:,:)
  
  real, allocatable :: dt_ene_f_kykx(:,:)
  real, allocatable :: dt_ene_e_kykx(:,:)
  real, allocatable :: dt_ene_m_kykx(:,:)
  

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
  
    lcalc_energy_evo = .false.
    
    ky_ene_evo = .true.
    kykx_ene_evo = .false.
    
    ene_drift = .true.
    ene_landau = .true.
    
  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast
    call mpibcast(lcalc_energy_evo,1)
    call mpibcast(ene_drift,1)
    call mpibcast(ene_landau,1)
    call mpibcast(ky_ene_evo,1)
    call mpibcast(kykx_ene_evo,1)

  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use control, only : nlapar, naverage, flux_tube
    use general, only : gkw_warn


    if (.not.lcalc_energy_evo) return

    if (.not. flux_tube) then 
      call gkw_warn('The entropy balance diagnostic does not make much sense &
         & without the local limit.')
    end if
    if (naverage == 1) then
      call gkw_warn ('energetics diagnostic does not work with naverage == 1 &
         & because it needs to compute a d/dt!')
      lcalc_energy_evo = .false.
    end if
 
    ! not with normalized = .true. or normalized_per_toroidal_mode = .true.
    ! not with source time
 
  end subroutine check
  
  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init(requirements)
    use control, only : nlapar, io_legacy, spectral_radius, disp_x, disp_vp
    use control, only : lcollisions
    use mode, only : mode_box
    use io, only : open_real_lu, ascii_fmt, attach_metadata
    use io, only : description_key, comments_key, phys_unit_key, not_avail
    use io, only : binary_fmt
    use diagnos_generic, only : attach_metadata_grid
    use global, only : PHI_FIELD, DISTRIBUTION, PHI_GA_FIELD
    use global, only : APAR_FIELD, APAR_GA_FIELD
    use diagnos_generic, only : LOCAL_DATA, S_GHOSTCELLS, VPAR_GHOSTCELLS
    use diagnos_generic, only : X_GHOSTCELLS, MU_GHOSTCELLS
    use grid, only : nmod, n_x_grid
    use mpiinterface, only : root_processor
    use global, only : r_tiny

    logical, intent(inout) :: requirements(:,:)
    character(len=7) :: repr

    if (lcalc_energy_evo) then
      requirements(PHI_FIELD,LOCAL_DATA) = .true.
      requirements(DISTRIBUTION,LOCAL_DATA) = .true.
      requirements(PHI_GA_FIELD,LOCAL_DATA) = .true.
    end if
    
    if(lcalc_energy_evo) then
      requirements(DISTRIBUTION,S_GHOSTCELLS) = .true.
      requirements(PHI_GA_FIELD,S_GHOSTCELLS) = .true.
      requirements(DISTRIBUTION,VPAR_GHOSTCELLS) = .true.
      if(lcollisions) then
        requirements(DISTRIBUTION,VPAR_GHOSTCELLS) = .true.
        requirements(DISTRIBUTION,MU_GHOSTCELLS) = .true.
      end if
      if(disp_vp > 0) requirements(DISTRIBUTION,VPAR_GHOSTCELLS) = .true.
      if (mode_box .and. abs(disp_x) > r_tiny .and..not. spectral_radius) then
        ! the perp dissipation is implemented with finite differences
        requirements(DISTRIBUTION,X_GHOSTCELLS) = .true.
      end if
      if(nlapar) then
        requirements(APAR_FIELD,LOCAL_DATA) = .true.
        requirements(APAR_GA_FIELD,LOCAL_DATA) = .true.
      end if
    end if

    if(io_legacy) then
      repr = '.kyspec'
    else
      repr = '_ky'
    end if

    if(root_processor) then
    
      if (.not.lcalc_energy_evo) return
      
      ! ky-output
      if(ky_ene_evo) then

        call open_real_lu('ene_e'//repr, 'diagnostic/diagnos_energy_evo', &
           & (/ nmod /), ascii_fmt, i_ene_e)
        call attach_metadata_grid(i_ene_e, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_e, phys_unit_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_e, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_e, comments_key, not_avail, ascii_fmt)

        if (nlapar) then
          call open_real_lu('ene_m'//repr, 'diagnostic/diagnos_energy_evo', &
             & (/ nmod /), ascii_fmt, i_ene_m)
          call attach_metadata_grid(i_ene_m, 'time', 'krho', ascii_fmt)
          call attach_metadata(i_ene_m, phys_unit_key, not_avail, ascii_fmt)
          call attach_metadata(i_ene_m, description_key, not_avail, ascii_fmt)
          call attach_metadata(i_ene_m, comments_key, not_avail, ascii_fmt)
        end if

        call open_real_lu('ene_f'//repr, 'diagnostic/diagnos_energy_evo', &
           & (/ nmod /), ascii_fmt, i_ene_f)
        call attach_metadata_grid(i_ene_f, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_f, phys_unit_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_f, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_f, comments_key, not_avail, ascii_fmt)
        
        
        call open_real_lu('dt_ene_e'//repr, 'diagnostic/diagnos_energy_evo', &
           & (/ nmod /), ascii_fmt, i_dt_ene_e)
        call attach_metadata_grid(i_dt_ene_e, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_dt_ene_e, phys_unit_key, not_avail, ascii_fmt)
        call attach_metadata(i_dt_ene_e, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_dt_ene_e, comments_key, not_avail, ascii_fmt)

        if (nlapar) then
          call open_real_lu('dt_ene_m'//repr, 'diagnostic/diagnos_energy_evo', &
             & (/ nmod /), ascii_fmt, i_dt_ene_m)
          call attach_metadata_grid(i_dt_ene_m, 'time', 'krho', ascii_fmt)
          call attach_metadata(i_dt_ene_m, phys_unit_key, not_avail, ascii_fmt)
          call attach_metadata(i_dt_ene_m, description_key, not_avail, ascii_fmt)
          call attach_metadata(i_dt_ene_m, comments_key, not_avail, ascii_fmt)
          
        end if

        call open_real_lu('dt_ene_f'//repr, 'diagnostic/diagnos_energy_evo', &
           & (/ nmod /), ascii_fmt, i_dt_ene_f)
        call attach_metadata_grid(i_dt_ene_f, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_dt_ene_f, phys_unit_key, not_avail, ascii_fmt)
        call attach_metadata(i_dt_ene_f, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_dt_ene_f, comments_key, not_avail, ascii_fmt)

        
        if(lcollisions) then
          call open_real_lu('ene_f_coll'//repr, 'diagnostic/diagnos_energy_evo', &
             & (/ nmod /), ascii_fmt, i_ene_f_coll)
          call attach_metadata_grid(i_ene_f_coll, 'time', 'krho', ascii_fmt)
          call attach_metadata(i_ene_f_coll, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
          call attach_metadata(i_ene_f_coll, description_key, not_avail, ascii_fmt)
          call attach_metadata(i_ene_f_coll, comments_key, not_avail, ascii_fmt)
        end if


        call open_real_lu('ene_f_num_dis'//repr, 'diagnostic/diagnos_energy_evo',&
           & (/ nmod /), ascii_fmt, i_ene_f_num_dis)
        call attach_metadata_grid(i_ene_f_num_dis, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_f_num_dis, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_f_num_dis, description_key, &
           & 'Numerical dissipation from parallel derivatives', ascii_fmt)
        call attach_metadata(i_ene_f_num_dis, comments_key, not_avail, ascii_fmt)


        call open_real_lu('ene_f_num_vp'//repr, 'diagnostic/diagnos_energy_evo',&
           & (/ nmod /), ascii_fmt, i_ene_f_num_vp)
        call attach_metadata_grid(i_ene_f_num_vp, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_f_num_vp, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_f_num_vp, description_key, &
           & 'Numerical dissipation from v_\parallel derivatives', ascii_fmt)
        call attach_metadata(i_ene_f_num_vp, comments_key, not_avail, ascii_fmt)


        call open_real_lu('ene_f_num_perp'//repr, &
           & 'diagnostic/diagnos_energy_evo', (/ nmod /), &
           & ascii_fmt, i_ene_f_num_perp)
        call attach_metadata_grid(i_ene_f_num_perp, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_f_num_perp, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_f_num_perp, description_key, &
           & 'Numerical dissipation from perpendicular derivatives', ascii_fmt)
        call attach_metadata(i_ene_f_num_perp, comments_key, not_avail, ascii_fmt)

        
        if(ene_landau) then
          call open_real_lu('ene_f_landau'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
             & ascii_fmt, i_ene_f_landau)
          call attach_metadata_grid(i_ene_f_landau, 'time', 'krho', ascii_fmt)
          call attach_metadata(i_ene_f_landau, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
          call attach_metadata(i_ene_f_landau, description_key, not_avail, ascii_fmt)
          call attach_metadata(i_ene_f_landau, comments_key, not_avail, ascii_fmt)
        end if
        
        call open_real_lu('ene_f_twist'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
           & ascii_fmt, i_ene_f_twist)
        call attach_metadata_grid(i_ene_f_twist, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_f_twist, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_f_twist, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_f_twist, comments_key, not_avail, ascii_fmt)
        
        
        call open_real_lu('ene_e_twist'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
           & ascii_fmt, i_ene_e_twist)
        call attach_metadata_grid(i_ene_e_twist, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_e_twist, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_e_twist, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_e_twist, comments_key, not_avail, ascii_fmt)
        
        
        if(ene_drift) then
          call open_real_lu('ene_f_drift'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
             & ascii_fmt, i_ene_f_drift)
          call attach_metadata_grid(i_ene_f_drift, 'time', 'krho', ascii_fmt)
          call attach_metadata(i_ene_f_drift, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
          call attach_metadata(i_ene_f_drift, description_key, not_avail, ascii_fmt)
          call attach_metadata(i_ene_f_drift, comments_key, not_avail, ascii_fmt)
        end if
        
        
        call open_real_lu('ene_f_disp'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
           & ascii_fmt, i_ene_f_disp)
        call attach_metadata_grid(i_ene_f_disp, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_f_disp, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_f_disp, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_f_disp, comments_key, not_avail, ascii_fmt)
        
        
        call open_real_lu('ene_e_disp'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
           & ascii_fmt, i_ene_e_disp)
        call attach_metadata_grid(i_ene_e_disp, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_e_disp, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_e_disp, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_e_disp, comments_key, not_avail, ascii_fmt)
        
        
        call open_real_lu('ene_f_source'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
           & ascii_fmt, i_ene_f_source)
        call attach_metadata_grid(i_ene_f_source, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_ene_f_source, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
        call attach_metadata(i_ene_f_source, description_key, not_avail, ascii_fmt)
        call attach_metadata(i_ene_f_source, comments_key, not_avail, ascii_fmt)
        
        
        if(nlapar) then
          call open_real_lu('ene_f_source_m'//repr, 'diagnostic/diagnos_energy_evo', (/ nmod /), &
             & ascii_fmt, i_ene_f_source_m)
          call attach_metadata_grid(i_ene_f_source_m, 'time', 'krho', ascii_fmt)
          call attach_metadata(i_ene_f_source_m, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', ascii_fmt)
          call attach_metadata(i_ene_f_source_m, description_key, not_avail, ascii_fmt)
          call attach_metadata(i_ene_f_source_m, comments_key, not_avail, ascii_fmt)
        end if

      end if !ky_ene_evo


      
      ! ky- and x-dependent data
      if(kykx_ene_evo) then
      

        call open_real_lu('ene_f_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
           & (/ nmod, n_x_grid /), binary_fmt, i_ene_f_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_ene_f_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_ene_f_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_ene_f_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_ene_f_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_ene_f_kykx, comments_key, &
           & not_avail, binary_fmt)
      
      
        call open_real_lu('ene_e_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_ene_e_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_ene_e_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_ene_e_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_ene_e_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_ene_e_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_ene_e_kykx, comments_key, &
           & not_avail, binary_fmt)
           
      
        call open_real_lu('dt_ene_f_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_dt_ene_f_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_dt_ene_f_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_dt_ene_f_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_dt_ene_f_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_dt_ene_f_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_dt_ene_f_kykx, comments_key, &
           & not_avail, binary_fmt)
           
           
        call open_real_lu('dt_ene_e_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_dt_ene_e_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_dt_ene_e_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_dt_ene_e_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_dt_ene_e_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_dt_ene_e_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_dt_ene_e_kykx, comments_key, &
           & not_avail, binary_fmt)
          
        
        if(nlapar) then
           
          call open_real_lu('ene_m_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
           & (/ nmod, n_x_grid /), binary_fmt, i_ene_m_kykx)
          if(spectral_radius) then
            call attach_metadata_grid(i_ene_m_kykx, 'time', &
               & 'krho', 'kxrh', binary_fmt)
          else
            call attach_metadata_grid(i_ene_m_kykx, 'time', &
               & 'krho', 'xphi', binary_fmt)
          end if
          call attach_metadata(i_ene_m_kykx, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
          call attach_metadata(i_ene_m_kykx, description_key, &
             & 'perpendicular entropy slice, flux surface averaged', &
             & binary_fmt)
          call attach_metadata(i_ene_m_kykx, comments_key, &
             & not_avail, binary_fmt)
             
             
          call open_real_lu('dt_ene_m_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
           & (/ nmod, n_x_grid /), binary_fmt, i_dt_ene_m_kykx)
          if(spectral_radius) then
            call attach_metadata_grid(i_dt_ene_m_kykx, 'time', &
               & 'krho', 'kxrh', binary_fmt)
          else
            call attach_metadata_grid(i_dt_ene_m_kykx, 'time', &
               & 'krho', 'xphi', binary_fmt)
          end if
          call attach_metadata(i_dt_ene_m_kykx, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
          call attach_metadata(i_dt_ene_m_kykx, description_key, &
             & 'perpendicular entropy slice, flux surface averaged', &
             & binary_fmt)
          call attach_metadata(i_dt_ene_m_kykx, comments_key, &
             & not_avail, binary_fmt)
             
          call open_real_lu('ene_f_source_m_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
           & (/ nmod, n_x_grid /), binary_fmt, i_ene_f_source_m_kykx)
          if(spectral_radius) then
            call attach_metadata_grid(i_ene_f_source_m_kykx, 'time', &
               & 'krho', 'kxrh', binary_fmt)
          else
            call attach_metadata_grid(i_ene_f_source_m_kykx, 'time', &
               & 'krho', 'xphi', binary_fmt)
          end if
          call attach_metadata(i_ene_f_source_m_kykx, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
          call attach_metadata(i_ene_f_source_m_kykx, description_key, &
             & 'perpendicular entropy slice, flux surface averaged', &
             & binary_fmt)
          call attach_metadata(i_ene_f_source_m_kykx, comments_key, &
             & not_avail, binary_fmt)
             
        end if
        
           
        call open_real_lu('ene_f_twist_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_ene_f_twist_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_ene_f_twist_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_ene_f_twist_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_ene_f_twist_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_ene_f_twist_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_ene_f_twist_kykx, comments_key, &
           & not_avail, binary_fmt)
           
           
        call open_real_lu('ene_e_twist_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_ene_e_twist_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_ene_e_twist_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_ene_e_twist_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_ene_e_twist_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_ene_e_twist_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_ene_e_twist_kykx, comments_key, &
           & not_avail, binary_fmt)


        if(ene_drift) then
          call open_real_lu('ene_f_drift_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
           & (/ nmod, n_x_grid /), binary_fmt, i_ene_f_drift_kykx)
          if(spectral_radius) then
            call attach_metadata_grid(i_ene_f_drift_kykx, 'time', &
               & 'krho', 'kxrh', binary_fmt)
          else
            call attach_metadata_grid(i_ene_f_drift_kykx, 'time', &
               & 'krho', 'xphi', binary_fmt)
          end if
          call attach_metadata(i_ene_f_drift_kykx, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
          call attach_metadata(i_ene_f_drift_kykx, description_key, &
             & 'perpendicular entropy slice, flux surface averaged', &
             & binary_fmt)
          call attach_metadata(i_ene_f_drift_kykx, comments_key, &
             & not_avail, binary_fmt)
        end if
        
        
        if(ene_landau) then
          call open_real_lu('ene_f_landau_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
           & (/ nmod, n_x_grid /), binary_fmt, i_ene_f_landau_kykx)
          if(spectral_radius) then
            call attach_metadata_grid(i_ene_f_landau_kykx, 'time', &
               & 'krho', 'kxrh', binary_fmt)
          else
            call attach_metadata_grid(i_ene_f_landau_kykx, 'time', &
               & 'krho', 'xphi', binary_fmt)
          end if
          call attach_metadata(i_ene_f_landau_kykx, phys_unit_key, &
             & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
          call attach_metadata(i_ene_f_landau_kykx, description_key, &
             & 'perpendicular entropy slice, flux surface averaged', &
             & binary_fmt)
          call attach_metadata(i_ene_f_landau_kykx, comments_key, &
             & not_avail, binary_fmt)
        end if
           
           
        call open_real_lu('ene_f_disp_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_ene_f_disp_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_ene_f_disp_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_ene_f_disp_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_ene_f_disp_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_ene_f_disp_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_ene_f_disp_kykx, comments_key, &
           & not_avail, binary_fmt)
           
           
        call open_real_lu('ene_e_disp_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_ene_e_disp_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_ene_e_disp_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_ene_e_disp_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_ene_e_disp_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_ene_e_disp_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_ene_e_disp_kykx, comments_key, &
           & not_avail, binary_fmt)
           
           
        call open_real_lu('ene_f_source_kykx_fsa', 'diagnostic/diagnos_energy_evo', &
         & (/ nmod, n_x_grid /), binary_fmt, i_ene_f_source_kykx)
        if(spectral_radius) then
          call attach_metadata_grid(i_ene_f_source_kykx, 'time', &
             & 'krho', 'kxrh', binary_fmt)
        else
          call attach_metadata_grid(i_ene_f_source_kykx, 'time', &
             & 'krho', 'xphi', binary_fmt)
        end if
        call attach_metadata(i_ene_f_source_kykx, phys_unit_key, &
           & 'v_{th,ref}n_{R,0}/R_{ref}', binary_fmt)
        call attach_metadata(i_ene_f_source_kykx, description_key, &
           & 'perpendicular entropy slice, flux surface averaged', &
           & binary_fmt)
        call attach_metadata(i_ene_f_source_kykx, comments_key, &
           & not_avail, binary_fmt)
           
      end if !kykx_ene_evo
      
    end if ! root_processor
    
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
  
    use grid, only : nmod, nx, nsp, number_of_species
    use general, only : gkw_abort
    use dist, only : nsolc
    use diagnos_fluxes_vspace, only : allocate_flux_det
    use control, only : nlapar, lcollisions
    use source_time, only : source_time_ampl
    use global, only : r_tiny
    
    integer :: ierr 
    
    if (.not.lcalc_energy_evo) return

  
  
    allocate(ene_f(nmod), ene_e(nmod), ene_m(nmod), stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate')
    allocate(last_ene_f(nmod), last_ene_e(nmod), last_ene_m(nmod), stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate')
    allocate(dt_ene_f(nmod), dt_ene_e(nmod), dt_ene_m(nmod), stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate')
    
    allocate(ene_f_num_dis(nmod), ene_f_num_vp(nmod), ene_f_num_perp(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate')
    allocate(ene_f_coll(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate')
    allocate(ene_f_landau(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
       & ene_f_landau')
    allocate(ene_f_twist(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
       & ene_f_twist')
    allocate(ene_f_drift(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnos_energy_evo :: could not &
       & allocate ene_f_drift')
    allocate(ene_e_twist(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
       & ene_e_twist')
    allocate(ene_f_source(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnos_energy_evo :: could not &
       & allocate ene_f_source')
    allocate(ene_f_disp(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnos_energy_evo :: could not &
       & allocate ene_f_disp')
    allocate(ene_e_disp(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnos_energy_evo :: could not &
       & allocate ene_e_disp')
    if(nlapar) then
      allocate(ene_f_source_m(nmod),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnos_energy_evo :: could not &
         & allocate ene_f_source_m')
    end if
    
    
    allocate(num_disp01(nsolc), num_disp02(nsolc), num_disp03(nsolc),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate')
    
    if(lcollisions) then
      allocate(collisionop(nsolc),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate collisionop')
    end if
    
    
    ! needed for twisted modes generation and, hence, for any
    ! spectral output
    if(kykx_ene_evo .or. ky_ene_evo) then
      allocate(term01(nsolc), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate term01')
    end if
    
    ! needed for transfer between entropy and electric field energy through
    ! drift
    if((kykx_ene_evo .and. ene_drift) &
      & .or. (ky_ene_evo .and. ene_drift))  then
      allocate(term08(nsolc), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate term08')
    end if
      
    ! needed for any spectral output
    if(kykx_ene_evo .or. ky_ene_evo) then
      allocate(term05(nsolc), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate term05')
      if(nlapar) then
        allocate(term05_m(nsolc), stat=ierr)
        if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate term05_m')
      end if
    end if

    
    if(kykx_ene_evo) then
    
      allocate(ene_f_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_f_kykx')
                          
      allocate(ene_e_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_e_kykx')
                          
      allocate(ene_m_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_m_kykx')
                          
      allocate(last_ene_f_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & last_ene_f_kykx')
                          
      allocate(last_ene_e_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & last_ene_e_kykx')
                          
      allocate(last_ene_m_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & last_ene_m_kykx')
                          
      allocate(dt_ene_f_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & dt_ene_f_kykx')
                          
      allocate(dt_ene_e_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & dt_ene_e_kykx')
      
      allocate(dt_ene_m_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & dt_ene_m_kykx')
                          
      allocate(ene_f_twist_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_f_twist_kykx')
                          
      allocate(ene_e_twist_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_e_twist_kykx')
                          
      allocate(ene_f_landau_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_f_landau_kykx')
                          
      allocate(ene_f_drift_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_f_drift_kykx')
                          
      allocate(ene_f_disp_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_f_disp_kykx')
                          
      allocate(ene_e_disp_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_e_disp_kykx')
                          
      allocate(ene_f_source_kykx(nmod,nx), stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                          & ene_f_source_kykx')
      
      if(nlapar) then
        allocate(ene_f_source_m_kykx(nmod,nx), stat=ierr)
        if (ierr /= 0) call gkw_abort('diagnostic :: could not allocate &
                            & ene_f_source_m_kykx')
      end if
      
    end if !kykx_ene_evo
    

  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()
  
    use io,           only : close_lu, binary_fmt, ascii_fmt
    use mpiinterface, only : root_processor
    use control,      only : nlapar, lcollisions

    if(allocated(ene_f_kykx)) deallocate(ene_f_kykx)
    
    if(allocated(ene_e_kykx)) deallocate(ene_e_kykx)
    if(allocated(ene_m_kykx)) deallocate(ene_m_kykx)
    
    if(allocated(ene_f_twist_kykx)) deallocate(ene_f_twist_kykx)
    if(allocated(ene_e_twist_kykx)) deallocate(ene_e_twist_kykx)
    if(allocated(ene_f_landau_kykx)) deallocate(ene_f_landau_kykx)
    if(allocated(ene_f_drift_kykx)) deallocate(ene_f_drift_kykx)
    if(allocated(ene_f_disp_kykx)) deallocate(ene_f_disp_kykx)
    if(allocated(ene_e_disp_kykx)) deallocate(ene_e_disp_kykx)
    if(allocated(ene_f_source_kykx)) deallocate(ene_f_source_kykx)
    if(allocated(ene_f_source_m_kykx)) deallocate(ene_f_source_m_kykx)
    
    if(allocated(last_ene_f_kykx)) deallocate(last_ene_f_kykx)
    if(allocated(last_ene_e_kykx)) deallocate(last_ene_e_kykx)
    if(allocated(last_ene_m_kykx)) deallocate(last_ene_m_kykx)
    
    if(allocated(dt_ene_f_kykx)) deallocate(dt_ene_f_kykx)
    if(allocated(dt_ene_e_kykx)) deallocate(dt_ene_e_kykx)
    if(allocated(dt_ene_m_kykx)) deallocate(dt_ene_m_kykx)
    
    if(allocated(ene_f)) deallocate(ene_f)
    if(allocated(ene_e)) deallocate(ene_e)
    if(allocated(ene_m)) deallocate(ene_m)
    
    if(allocated(last_ene_f)) deallocate(last_ene_f)
    if(allocated(last_ene_e)) deallocate(last_ene_e)
    if(allocated(last_ene_m)) deallocate(last_ene_m)
    
    if(allocated(dt_ene_f)) deallocate(dt_ene_f)
    if(allocated(dt_ene_e)) deallocate(dt_ene_e)
    if(allocated(dt_ene_m)) deallocate(dt_ene_m)
    
    if(allocated(ene_f_num_dis)) deallocate(ene_f_num_dis)
    if(allocated(ene_f_num_vp)) deallocate(ene_f_num_vp)
    if(allocated(ene_f_num_perp)) deallocate(ene_f_num_perp)
    if(allocated(ene_f_coll)) deallocate(ene_f_coll)
    
    if(allocated(num_disp01)) deallocate(num_disp01)
    if(allocated(num_disp02)) deallocate(num_disp02)
    if(allocated(num_disp03)) deallocate(num_disp03)
    if(allocated(collisionop)) deallocate(collisionop)
    if(allocated(ene_f_drift)) deallocate(ene_f_drift)
    if(allocated(ene_f_landau)) deallocate(ene_f_landau)
    if(allocated(ene_f_disp)) deallocate(ene_f_disp)
    if(allocated(ene_f_source)) deallocate(ene_f_source)
    if(allocated(ene_f_source_m)) deallocate(ene_f_source_m)

    
    if(allocated(term01)) deallocate(term01)
    if(allocated(term05)) deallocate(term05)
    if(allocated(term05_m)) deallocate(term05_m)
    if(allocated(term08)) deallocate(term08)

    
    ! close logical units
    if(root_processor) then
      
      ! ky-dependent output
      if(ky_ene_evo) then
      
        call close_lu(i_ene_f, ascii_fmt)
        call close_lu(i_ene_e, ascii_fmt)
        
        if(nlapar) then
          call close_lu(i_ene_m, ascii_fmt)
          call close_lu(i_dt_ene_m, ascii_fmt)
          call close_lu(i_ene_f_source_m, ascii_fmt)
        end if
        
        call close_lu(i_dt_ene_f, ascii_fmt)
        call close_lu(i_dt_ene_e, ascii_fmt)
        
        
        if(lcollisions) then
          call close_lu(i_ene_f_coll, ascii_fmt)
        end if
        call close_lu(i_ene_f_num_dis, ascii_fmt)
        call close_lu(i_ene_f_num_vp, ascii_fmt)
        call close_lu(i_ene_f_num_perp, ascii_fmt)
        
        if(ene_landau) then
          call close_lu(i_ene_f_landau, ascii_fmt)
        end if
        if(ene_drift) then
          call close_lu(i_ene_f_drift, ascii_fmt)
        end if
        
        call close_lu(i_ene_f_twist, ascii_fmt)
        call close_lu(i_ene_e_twist, ascii_fmt)
        
        call close_lu(i_ene_f_source, ascii_fmt)
        call close_lu(i_ene_f_disp, ascii_fmt)
        call close_lu(i_ene_e_disp, ascii_fmt)
      
      end if ! ky_ene_evo
      
      
      if(kykx_ene_evo) then
      
        call close_lu(i_ene_f_kykx, binary_fmt)
        call close_lu(i_ene_e_kykx, binary_fmt)
        call close_lu(i_dt_ene_f_kykx, binary_fmt)
        call close_lu(i_dt_ene_e_kykx, binary_fmt)
        
        if(nlapar) then
          call close_lu(i_ene_m_kykx, binary_fmt)
          call close_lu(i_dt_ene_m_kykx, binary_fmt)
          call close_lu(i_ene_f_source_m_kykx, binary_fmt)
        end if
        
        call close_lu(i_ene_f_twist_kykx, binary_fmt)
        call close_lu(i_ene_e_twist_kykx, binary_fmt)
        
        if(ene_landau) then
          call close_lu(i_ene_f_landau_kykx, binary_fmt)
        end if
        if(ene_drift) then
          call close_lu(i_ene_f_drift_kykx, binary_fmt)
        end if
        
        call close_lu(i_ene_f_source_kykx, binary_fmt)
        call close_lu(i_ene_f_disp_kykx, binary_fmt)
        call close_lu(i_ene_e_disp_kykx, binary_fmt)
      end if ! kykx_ene_evo
      
    end if

  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine calc_smallstep(i_smallstep)
  
    use control,          only : naverage, time
    
    integer, intent(in) :: i_smallstep
    real, save :: last_time_pre_energetics = 0.

    if (.not.lcalc_energy_evo) return

    if (i_smallstep == naverage - 1) then
    
      if(kykx_ene_evo) then
        call calc_ene(last_ene_f, last_ene_e, last_ene_m, &
         & last_ene_f_kykx, last_ene_e_kykx, &
         & last_ene_m_kykx)
      else
        call calc_ene(last_ene_f, last_ene_e, last_ene_m)
      end if
         
        
      ! store the time this subroutine was last called in a local variable
      last_time_pre_energetics = time
      

    else if (i_smallstep == naverage) then
      ! save time interval between this step and the
      ! last call of calc_ene().
      ! this intervall is needed by several subroutines of this module
      delta_time_energetics = time - last_time_pre_energetics
    end if

  end subroutine calc_smallstep
  
 

  !--------------------------------------------------------------------
  !> The routine calc_largestep() is to be called repeatedly, after every
  !> large timestep, and here the diagnostic should calculate its
  !> quantities.
  !> 
  !> Splitting calculation and output is to be encouraged, particularly when
  !>  - the output is higher dimensional, and requires MPI or MPI-IO
  !>  - same data is output in different dimensional slices
  !--------------------------------------------------------------------
  subroutine calc_largestep()
    use grid,             only : nmod, nx, ns, nvpar, nmu, nsp
    use grid,             only : gsp, gx, gs, proc_subset
    use fields,           only : get_averaged_phi, get_averaged_apar
    use matdat,           only : get_f_from_g
    use matdat,           only : matd, matvpd, matperpd, matcoll
    use matdat,           only : add_source
    use matrix_format,    only : usmv
    use dist,             only : fdisi, fmaxwl, ifdis, fdis_tmp, iphi
    use geom,             only : ints, bn, lfun, ffun, gfun
    use velocitygrid,     only : intmu, intvp, vpgr, mugr
    use components,       only : signz, de, tmp, mas, vthrat, vp, tp, fp
    use components,       only : vthrat
    use index_function,   only : indx
    use constants,        only : c1
    use global,           only : r_tiny
    use normalise,        only : fnorm1d
    use rotation,         only : cfen, vcor, dcfen_ds
    use source_time,      only : add_source_time
    use control,          only : zonal_adiabatic, normalize_per_toroidal_mode
    use control,          only : nlapar, lcollisions
    use components,       only : adiabatic_electrons
    use mpiinterface,     only : mpireduce_sum_inplace, root_processor
    use mpicomms,         only : COMM_SP_NE_X_NE, COMM_X_EQ, COMM_S_EQ_X_EQ
    use mpicomms,         only : COMM_SP_EQ
    use diagnos_fluxes,   only : pflux_es, eflux_es, vflux_es
    use diagnos_fluxes_vspace, only : calc_fluxes_full_detail
    use diagnos_fluxes_vspace, only : pflux_det, eflux_det, vflux_det
    use global,  only : PHI_GA_FIELD, PHI_FIELD
    use diagnos_generic,  only : parseval_correction, xy_slice_ipar, dfieldds
    use matdat, only : mat_vpar_grd_phi, mat_vd_grad_phi_fm, mat_vpar_grad_df
    use matdat, only : mat_vdgradf, mat_trapdf_4d, mat_ve_grad_fm
    use matdat, only : mat_v_del_B_perp_grad_fm


    integer :: i, j, k, is, imod, ix
    integer :: iih, ierr
    integer :: ixg, isglb
    complex :: fdis, phi, phi_ga, hdis
    real :: fdis_sq, phi_sq, phi_ga_sq
    real :: d2X, d3X, dumint, d3v, dum
    complex :: dphids(ns)

    if (.not.lcalc_energy_evo) return
    
    
    ! calcualte various energies and entropy
    if(.not. kykx_ene_evo) then
      call calc_ene(ene_f, ene_e, ene_m)
    else
      call calc_ene(ene_f, ene_e, ene_m, ene_f_kykx, &
          & ene_e_kykx, ene_m_kykx)
    end if
    
  
    ! needed for twisted modes generation and, hence, for any
    ! spectral output
    if(kykx_ene_evo .or. ky_ene_evo) then
      term01 = (0.E0, 0.E0)
      call usmv(c1,mat_vpar_grad_df,fdis_tmp,term01,ierr)
    end if
    
    ! needed for any spectral output
    if(kykx_ene_evo .or. ky_ene_evo) then
      term05 = (0.E0, 0.E0)
      call usmv(c1,mat_ve_grad_fm,fdis_tmp,term05,ierr)
      if(nlapar) then
        term05_m = (0.E0, 0.E0)
        call usmv(c1,mat_v_del_B_perp_grad_fm,fdis_tmp,term05_m,ierr)
      end if
    end if
    
    
    ! needed for transfer between entropy and electric field energy through
    ! drift
    if((kykx_ene_evo .and. ene_drift) &
      & .or. (ky_ene_evo .and. ene_drift))  then
      term08 = (0.E0, 0.E0)
      call usmv(c1,mat_vd_grad_phi_fm,fdis_tmp,term08,ierr)
    end if

    ! dissipation elements are stored in the matrices matd, matvpd, matperpd.
    num_disp01 = 0.0
    num_disp02 = 0.0
    num_disp03 = 0.0
    call usmv(c1,matd,fdis_tmp,num_disp01,ierr)
    call usmv(c1,matvpd,fdis_tmp,num_disp02,ierr)
    call usmv(c1,matperpd,fdis_tmp,num_disp03,ierr)
    
    ! collision elements
    if(lcollisions) then
      collisionop = 0.0
      call usmv(c1,matcoll,fdis_tmp,collisionop,ierr)
    end if
    
    ene_f_num_dis = 0.; ene_f_num_vp = 0.; ene_f_num_perp = 0.
    ene_f_coll = 0.; ene_f_source = 0.
    ene_f_landau = 0.; ene_f_twist = 0.
    ene_e_twist = 0.; ene_f_drift = 0.
    ene_f_source = 0; ene_f_disp = 0; ene_e_disp = 0.
    
    if(nlapar) then
      ene_f_source_m = 0.
    end if
    
    if(kykx_ene_evo) then
      ene_f_twist_kykx = 0.
      ene_e_twist_kykx = 0.
      ene_f_drift_kykx = 0.
      ene_f_landau_kykx = 0.
      ene_f_source_kykx = 0.
      ene_f_disp_kykx = 0.
      ene_e_disp_kykx = 0.
    end if
    

    do imod=1,nmod

      ! Perpendicular real space jacobian
      d2X = 1.0

      do ix=1,nx
        ! the actual (global) x index - blanks are left elsewhere
        ixg = gx(ix)

        do is=1,nsp
          if(de(ix,is) < r_tiny) cycle
          
          ! the actual (global) species index - blanks are left elsewhere
          isglb = gsp(is)

          do i=1,ns
          
            phi = fdisi(indx(iphi,imod,ix,i))
            phi_sq = abs(phi)**2
            
            ! ints has n_s_grid elements but in parallelize_geom some elements
            ! are copied so that every processor can index his by ints(1:ns)
            d3X = ints(i) * d2X 
            
            do j=1,nmu
            
              phi_ga  = get_averaged_phi(imod,ix,i,j,is,fdisi)
              phi_ga_sq = abs(phi_ga)**2
              
              
              call dfieldds(PHI_GA_FIELD,imod,ix,j,is,dphids)

              ! bn is the magnetic field modulus. The 2*pi which is
              ! furthermore contained in the velocity-space Jacobian is
              ! defined into intmu.
              dumint = intmu(j)*bn(ix,i)

              do k=1,nvpar
              
                ! fdisi stores g, but this function returns f:
                fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)
                fdis_sq = abs(fdis)**2
                
                ! the nonadiabatic part of the distribution function
                hdis = fdis + signz(is) * phi_ga * fmaxwl(ix,i,j,k,is) &
                  & / tmp(ix,is)
                  
                ! velocity space jacobian
                d3v = dumint * intvp(i,j,k,is)

                ! phase space index
                iih = indx(ifdis,imod,ix,i,j,k,is)
                

                ! collisional dissipation
                if(lcollisions) then
                  dum = tmp(ix,is) * de(ix,is) * &
                     & real(conjg(fdis) / fmaxwl(ix,i,j,k,is) &
                     & * collisionop(iih)) * d3v * d3X
                  ene_f_coll(imod) = ene_f_coll(imod) + dum
                  ene_f_disp(imod) = ene_f_disp(imod) + dum
                  if(kykx_ene_evo) then
                    ene_f_disp_kykx(imod,ix) = &
                      & ene_f_disp_kykx(imod,ix) + dum
                  end if
                end if


                ! numerical parallel dissipation:
                dum = tmp(ix,is) * de(ix,is) &
                   & * d3X * d3v * real(conjg(fdis) / fmaxwl(ix,i,j,k,is) &
                   & * num_disp01(iih))
                ene_f_num_dis(imod)  = ene_f_num_dis(imod) + dum
                ene_f_disp(imod) = ene_f_disp(imod) + dum
                if(kykx_ene_evo) then
                  ene_f_disp_kykx(imod,ix) = &
                    & ene_f_disp_kykx(imod,ix) + dum
                end if
                
          
                ! numerical parallel velocity dissipation
                dum = tmp(ix,is) * de(ix,is) &
                   & * d3X * d3v * real(conjg(fdis) / fmaxwl(ix,i,j,k,is) &
                   & * num_disp02(iih))
                ene_f_num_vp(imod)   = ene_f_num_vp(imod)  + dum
                ene_f_disp(imod) = ene_f_disp(imod) + dum
                if(kykx_ene_evo) then
                  ene_f_disp_kykx(imod,ix) = &
                    & ene_f_disp_kykx(imod,ix) + dum
                end if
                   
                  
                ! numerical dissipation from perpendicular (hyper)diffusion
                dum = tmp(ix,is) * de(ix,is) &
                   & * d3X * d3v * real(conjg(fdis) / fmaxwl(ix,i,j,k,is) &
                   & * num_disp03(iih))
                ene_f_num_perp(imod) = ene_f_num_perp(imod) + dum
                ene_f_disp(imod) = ene_f_disp(imod) + dum
                if(kykx_ene_evo) then
                  ene_f_disp_kykx(imod,ix) = &
                    & ene_f_disp_kykx(imod,ix) + dum
                end if

                
                !The distribution contribution to the energy in the 
                ! Landau damping term (VII).
                ! This term appears in the ene_f equation.
                if((ky_ene_evo .and. ene_landau) .or. &
                  & (kykx_ene_evo .and. ene_landau)) then
                  dum = - tmp(ix,is) * de(ix,is) * d3X * d3v * &
                     & real(conjg(fdis)*dphids(i)) * signz(is) * vthrat(is)* &
                     & vpgr(i,j,k,is) * ffun(ix,i) / tmp(ix,is)
                  if(ky_ene_evo) then
                    ene_f_landau(imod) = ene_f_landau(imod) + dum
                  end if
                  if(kykx_ene_evo) then
                    ene_f_landau_kykx(imod,ix) = &
                      & ene_f_landau_kykx(imod,ix) + dum
                  end if
                end if
                   
                
                ! The distribution contribution to the energy in the 
                ! drift term (VIII).
                ! This term appears in the ene_f equation.
                if((ky_ene_evo .and. ene_drift) .or. &
                  & (kykx_ene_evo .and. ene_drift)) then
                  dum =  tmp(ix,is) * de(ix,is) * d3X * d3v  &
                      & * real(conjg(fdis) * term08(iih)) &
                      & / fmaxwl(ix,i,j,k,is)
                  if(ky_ene_evo) then
                    ene_f_drift(imod) = ene_f_drift(imod) + dum
                  end if
                  if(kykx_ene_evo) then
                    ene_f_drift_kykx(imod,ix) = &
                      & ene_f_drift_kykx(imod,ix) + dum
                  end if
                end if
                
                
                ! The entropy source term (V)
                if(kykx_ene_evo .or. ky_ene_evo) then
                  dum = tmp(ix,is) * de(ix,is) * d3X * d3v &
                      & * real(conjg(fdis) * term05(iih)) &
                      & / fmaxwl(ix,i,j,k,is)
                  if(ky_ene_evo) then
                    ene_f_source(imod) = ene_f_source(imod) + dum
                  end if
                  if(kykx_ene_evo) then
                    ene_f_source_kykx(imod,ix) = &
                      & ene_f_source_kykx(imod,ix) + dum
                  end if
                  
                  ! The electromagnetic part of the entropy source term (V)
                  if(nlapar) then
                    dum = tmp(ix,is) * de(ix,is) * d3X * d3v  &
                        & * real(conjg(fdis) * term05_m(iih)) &
                        & / fmaxwl(ix,i,j,k,is)
                    if(ky_ene_evo) then
                      ene_f_source_m(imod) = ene_f_source_m(imod) + dum
                    end if
                    if(kykx_ene_evo) then
                      ene_f_source_m_kykx(imod,ix) = &
                        & ene_f_source_m_kykx(imod,ix) + dum
                    end if
                  end if
                end if
                
                
                ! transfer to twisted modes
                if(kykx_ene_evo .or. ky_ene_evo) then
                  
                  ! entropy transfer to twisted modes
                  ! (the minus in front of conjg(fdis) enters since term01
                  ! already contains a minus sign)
                  dum = -tmp(ix,is) * de(ix,is) * d3X * d3v &
                     & * (real(-conjg(fdis) * term01(iih)) &
                     & + vthrat(is) * vpgr(i,j,k,is) * fdis_sq &
                     & * ( mugr(j) * gfun(ix,i) * bn(ix,i) &
                     & - 0.5 * dcfen_ds(i,is))) / fmaxwl(ix,i,j,k,is)                  
                  if(ky_ene_evo) then
                    ene_f_twist(imod) = ene_f_twist(imod) + dum
                  end if
                  if(kykx_ene_evo) then
                    ene_f_twist_kykx(imod,ix) = &
                     & ene_f_twist_kykx(imod,ix) + dum
                  end if
                  
                  ! electrostatic field energy transfer to 
                  ! twisted modes (the minus in front of conjg(phi_ga)
                  ! enters since term01 already contains a minus sign)
                  dum = -tmp(ix,is) * de(ix,is) * d3X * d3v &
                     & * real(-conjg(phi_ga) * term01(iih) &
                     & + vthrat(is) * vpgr(i,j,k,is) * ffun(ix,i) & 
                     & * conjg(fdis) * dphids(i)) / tmp(ix,is) &
                     & * signz(is)
                  if(ky_ene_evo) then
                  ene_e_twist(imod) = ene_e_twist(imod) + dum
                  end if
                  if(kykx_ene_evo) then
                    ene_e_twist_kykx(imod,ix) = &
                     & ene_e_twist_kykx(imod,ix) + dum
                  end if
                
                end if
              
                
                ! electrostatic energy dissipation due to numerical 
                ! dissipation in the parallel coordinate
                dum = tmp(ix,is) * de(ix,is) * d3X * d3v &
                   & * real(conjg(phi_ga) * num_disp01(iih)) &
                   & * signz(is) / tmp(ix,is)
                ene_e_disp(imod) = ene_e_disp(imod) + dum
                if(kykx_ene_evo) then
                  ene_e_disp_kykx(imod,ix) = &
                    & ene_e_disp_kykx(imod,ix) + dum
                end if
                   
                   
                ! electrostatic energy dissipation due to numerical 
                ! dissipation in the parallel velocity coordinate
                dum = tmp(ix,is) * de(ix,is) * d3X * d3v &
                   & * real(conjg(phi_ga) * num_disp02(iih)) &
                   & * signz(is) / tmp(ix,is)
                ene_e_disp(imod) = ene_e_disp(imod) + dum
                if(kykx_ene_evo) then
                  ene_e_disp_kykx(imod,ix) = &
                    & ene_e_disp_kykx(imod,ix) + dum
                end if
                
                
                ! electrostatic energy dissipation due to numerical 
                ! dissipation in the perpendicular plane
                dum = tmp(ix,is) * de(ix,is) * d3X * d3v &
                   & * real(conjg(phi_ga) * num_disp03(iih)) &
                   & * signz(is) / tmp(ix,is)
                ene_e_disp(imod) = ene_e_disp(imod) + dum
                if(kykx_ene_evo) then
                  ene_e_disp_kykx(imod,ix) = &
                    & ene_e_disp_kykx(imod,ix) + dum
                end if
                
                
                ! electrostatic energy dissipation due to collisions
                if(lcollisions) then
                  dum = tmp(ix,is) * de(ix,is) * d3X * d3v &
                     & * real(conjg(phi_ga) * collisionop(iih)) &
                     & * signz(is) / tmp(ix,is)
                  ene_e_disp(imod) = ene_e_disp(imod) + dum
                  if(kykx_ene_evo) then
                    ene_e_disp_kykx(imod,ix) = &
                      & ene_e_disp_kykx(imod,ix) + dum
                  end if
                end if
                
              end do !vpar
            end do !mu
          end do !ns
        end do !nsp
      end do !nx
    end do !nmod


    ! mpi reduce 1D arrays:
    if(ky_ene_evo) then
      call mpireduce_sum_inplace(ene_e,shape(ene_e))
      call mpireduce_sum_inplace(ene_f,shape(ene_f))
      call mpireduce_sum_inplace(ene_m,shape(ene_m))
      
      call mpireduce_sum_inplace(last_ene_e,shape(last_ene_e))
      call mpireduce_sum_inplace(last_ene_f,shape(last_ene_f))
      call mpireduce_sum_inplace(last_ene_m,shape(last_ene_m))
      
      if(lcollisions) then
        call mpireduce_sum_inplace(ene_f_coll,shape(ene_f_coll))
      end if
      call mpireduce_sum_inplace(ene_f_num_dis,shape(ene_f_num_dis))
      call mpireduce_sum_inplace(ene_f_num_vp,shape(ene_f_num_vp))
      call mpireduce_sum_inplace(ene_f_num_perp,shape(ene_f_num_perp))
      
      if(ene_landau) then
        call mpireduce_sum_inplace(ene_f_landau,shape(ene_f_landau))
      end if
      if(ene_drift) then
        call mpireduce_sum_inplace(ene_f_drift,shape(ene_f_drift))
      end if
      call mpireduce_sum_inplace(ene_f_disp,shape(ene_f_disp))
      call mpireduce_sum_inplace(ene_e_disp,shape(ene_e_disp))
      call mpireduce_sum_inplace(ene_f_source,shape(ene_f_source))
      if(nlapar) then
        call mpireduce_sum_inplace(ene_f_source_m,shape(ene_f_source_m))
      end if
      
      call mpireduce_sum_inplace(ene_f_twist,shape(ene_f_twist))
      call mpireduce_sum_inplace(ene_e_twist,shape(ene_e_twist))
      
    end if !ky_ene_evo
    
    
    if(kykx_ene_evo) then
      
      call mpireduce_sum_inplace(ene_f_kykx,shape(ene_f_kykx), &
         & COMM_X_EQ)
      call mpireduce_sum_inplace(last_ene_f_kykx, &
        & shape(last_ene_f_kykx), COMM_X_EQ)
      
      call mpireduce_sum_inplace(ene_e_kykx, &
        & shape(ene_e_kykx), COMM_X_EQ)
      call mpireduce_sum_inplace(last_ene_e_kykx, &
        & shape(last_ene_e_kykx), COMM_X_EQ)
        
      if(nlapar) then
        call mpireduce_sum_inplace(ene_m_kykx, &
          & shape(ene_m_kykx), COMM_X_EQ)
        call mpireduce_sum_inplace(last_ene_m_kykx, &
          & shape(last_ene_m_kykx), COMM_X_EQ)
        call mpireduce_sum_inplace(ene_f_source_m_kykx, &
        & shape(ene_f_source_m_kykx), COMM_X_EQ)
      end if
      
      call mpireduce_sum_inplace(ene_f_twist_kykx, &
        & shape(ene_f_twist_kykx), COMM_X_EQ)
      call mpireduce_sum_inplace(ene_e_twist_kykx, &
        & shape(ene_e_twist_kykx), COMM_X_EQ)
        
      if(ene_landau) then
        call mpireduce_sum_inplace(ene_f_landau_kykx, &
        & shape(ene_f_landau_kykx), COMM_X_EQ)
      end if
      if(ene_drift) then
        call mpireduce_sum_inplace(ene_f_drift_kykx, &
        & shape(ene_f_drift_kykx), COMM_X_EQ)
      end if
        
      call mpireduce_sum_inplace(ene_f_source_kykx, &
        & shape(ene_f_source_kykx), COMM_X_EQ)
      call mpireduce_sum_inplace(ene_f_disp_kykx, &
        & shape(ene_f_disp_kykx), COMM_X_EQ)
      call mpireduce_sum_inplace(ene_e_disp_kykx, &
        & shape(ene_e_disp_kykx), COMM_X_EQ)
      
    end if ! kykx_ene_evo
    
    
    
    ! calculate the time derivatives
    if(root_processor) then
      do imod=1, nmod
        
        if (abs(ene_f(imod) - last_ene_f(imod)) < r_tiny) then
          dt_ene_f(imod) = 0.0
        else
          dt_ene_f(imod) = (ene_f(imod) - &
             & last_ene_f(imod))/delta_time_energetics
        end if
        
        if (abs(ene_e(imod) - last_ene_e(imod)) < r_tiny) then
          dt_ene_e(imod) = 0.0
        else
          dt_ene_e(imod) = (ene_e(imod) - &
             & last_ene_e(imod))/delta_time_energetics
        end if
        
        if (abs(ene_m(imod) - last_ene_m(imod)) < r_tiny) then
          dt_ene_m(imod) = 0.0
        else
          dt_ene_m(imod) = (ene_m(imod) - &
             & last_ene_m(imod))/delta_time_energetics
        end if
        
        ! time derivatives on the kykx-plane
        if(kykx_ene_evo) then
          do ix=1, nx
          
            ! distribution contribution to energy
            if (abs(ene_f_kykx(imod,ix) - &
               & last_ene_f_kykx(imod,ix)) < r_tiny) then
              dt_ene_f_kykx(imod,ix) = 0.0
            else
              dt_ene_f_kykx(imod,ix) = (ene_f_kykx(imod,ix) &
                & -last_ene_f_kykx(imod,ix)) &
                & /delta_time_energetics
            end if
            
            ! electric field contribution to energy
            if (abs(ene_e_kykx(imod,ix) - &
               & last_ene_e_kykx(imod,ix)) < r_tiny) then
              dt_ene_e_kykx(imod,ix) = 0.0
            else
              dt_ene_e_kykx(imod,ix) = (ene_e_kykx(imod,ix) &
                & -last_ene_e_kykx(imod,ix)) &
                & /delta_time_energetics
            end if
            
            ! magnetic field energy
            if(nlapar) then
              if (abs(ene_m_kykx(imod,ix) - &
                & last_ene_m_kykx(imod,ix)) < r_tiny) then
                  dt_ene_m_kykx(imod,ix) = 0.0
              else
                dt_ene_m_kykx(imod,ix) = (ene_m_kykx(imod,ix) &
                  & -last_ene_m_kykx(imod,ix)) &
                  & /delta_time_energetics
              end if
            end if
          end do
        end if

      end do

    end if

  end subroutine calc_largestep


  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !> The energy evolution subroutine calculates the time derivative
  !> of the energies. These have to be
  !> calculated and stored in the timestep that preceeds the one the
  !> subroutine is called in, as well as in the subroutine itself.
  !> This routine here is called from both places.
  !----------------------------------------------------------------------------
  subroutine calc_ene(ene_f, ene_e, ene_m, ene_f_kykx, &
             & ene_e_kykx, ene_m_kykx)
             
    use grid,           only : nmod, nx, ns, nvpar, nmu, nsp, parallel_s
    use grid,           only : proc_subset, gs
    use geom,           only : ints, bn
    use mode,           only : ixzero
    use velocitygrid,   only : intmu, intvp, vpgr
    use dist,           only : fdisi, fmaxwl, iphi
    use index_function, only : indx
    use matdat,         only : get_f_from_g
    use components,     only : signz, tmp, de, iadia, adiabatic_electrons
    use components,     only : vthrat
    use fields,         only : get_averaged_phi, get_averaged_apar
    use control,        only : zonal_adiabatic, spectral_radius
    use mpicomms,       only : COMM_S_NE
    use mpiinterface,   only : mpiallreduce_sum_inplace
    use rotation,       only : cfen
    use diagnos_generic, only : parseval_correction, xy_slice_ipar
    use diagnos_generic, only : get_tr_ps_mask
    use global,         only : r_tiny
    

    real, intent(out), optional :: ene_f_kykx(nmod,nx)
    real, intent(out), optional :: ene_f(nmod), ene_e(nmod), ene_m(nmod)
    real, intent(out), optional :: ene_e_kykx(nmod,nx)
    real, intent(out), optional :: ene_m_kykx(nmod,nx)
    
    integer :: i, j, k, is, imod, ix
    real :: d2X, d3X, dumint, d3v
    complex :: fdis, phi_ga, apar_ga
    real :: fdis_sq, dum
    complex :: dum_adia

    if(present(ene_f)) ene_f = 0.
    if(present(ene_e)) ene_e = 0.
    if(present(ene_m)) ene_m = 0.
    if(present(ene_f_kykx)) ene_f_kykx = 0.
    if(present(ene_e_kykx)) ene_e_kykx = 0.
    if(present(ene_m_kykx)) ene_m_kykx = 0.

    do imod=1,nmod
    
      d2X = 1.0
      
      do ix=1,nx

        ! sum over species must be inside s-integral
        do i=1,ns
        
          d3X = ints(i) * d2X
          
          do is=1,nsp
            if(de(ix,is) < r_tiny) cycle
            do j=1,nmu
            
              phi_ga  = get_averaged_phi(imod,ix,i,j,is,fdisi)
              
              ! works with nlapar=.false; get_averaged_apar returns zero
              ! then
              apar_ga  = get_averaged_apar(imod,ix,i,j,is,fdisi)
              
              dumint = intmu(j)*bn(ix,i)
              
              do k=1,nvpar
              
                fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)
                fdis_sq = abs(fdis)**2
                
                d3v = dumint * intvp(i,j,k,is)
              
                ! energy in the distribution
                dum = de(ix,is) * tmp(ix,is) * d3v * d3X & 
                   & * 0.5 * fdis_sq * tmp(ix,is) / fmaxwl(ix,i,j,k,is) 
                ene_f(imod) = ene_f(imod) + dum
                if(present(ene_f_kykx)) then
                  ene_f_kykx(imod,ix) = ene_f_kykx(imod,ix) &
                     & + dum
                end if
                
                ! energy in the electric field 
                dum =  0.5 * de(ix,is) * d3v * d3X &
                    & * real(fdis*conjg(phi_ga)) * signz(is)
                ene_e(imod) = ene_e(imod) + dum
                if(present(ene_e_kykx)) then
                  ene_e_kykx(imod,ix) = &
                    & ene_e_kykx(imod,ix) + dum
                end if
                
                ! energy in the magnetic field
                dum = de(ix,is) * vthrat(is) * d3v * d3X &
                   & * real(fdis*conjg(apar_ga))*vpgr(i,j,k,is)*signz(is)
                ene_m(imod) = ene_m(imod) + dum
                if(present(ene_m_kykx)) then
                  ene_m_kykx(imod,ix) = &
                    & ene_m_kykx(imod,ix) + dum
                end if
            
              end do !vpar
            end do !mu
          end do !nsp
        end do !ns
      end do !nx
    end do !nmod
  end subroutine calc_ene

  !--------------------------------------------------------------------
  !> The routine output() should do the output to files, using the
  !> routines provided by the io module.
  !--------------------------------------------------------------------
  subroutine output()
    use io,              only : append_chunk, xy_fmt, xy_fmt_long, ascii_fmt
    use mpiinterface,    only : root_processor
    use control,         only : nlapar, lcollisions
    use grid,            only : proc_subset, ns
    use diagnos_generic, only : kyx_output_array, xy_slice_ipar
    use source_time,     only : source_time_ampl
    use global,          only : r_tiny


    if (.not.lcalc_energy_evo) return

    ! calculation of the entropy balance terms
    if (lcalc_energy_evo) then
      call calc_largestep
    end if
    

    ! Write data
    if (root_processor) then

      ! ky-dependent data
      if(ky_ene_evo) then
        call append_chunk(i_ene_e, ene_e, xy_fmt, ascii_fmt)
        call append_chunk(i_ene_f, ene_f, xy_fmt, ascii_fmt)
        if (nlapar) then
          call append_chunk(i_ene_m, ene_m, xy_fmt, ascii_fmt)
        end if
        
        call append_chunk(i_dt_ene_e, dt_ene_e, xy_fmt, ascii_fmt)
        call append_chunk(i_dt_ene_f, dt_ene_f, xy_fmt, ascii_fmt)
        if (nlapar) then
          call append_chunk(i_dt_ene_m, dt_ene_m, xy_fmt, ascii_fmt)
        end if
        
        if(lcollisions) then
          call append_chunk(i_ene_f_coll, ene_f_coll, xy_fmt, ascii_fmt)
        end if
        call append_chunk(i_ene_f_num_dis, ene_f_num_dis, xy_fmt, ascii_fmt)
        call append_chunk(i_ene_f_num_vp, ene_f_num_vp, xy_fmt, ascii_fmt)
        call append_chunk(i_ene_f_num_perp, ene_f_num_perp, xy_fmt, ascii_fmt)
       
        if(ene_landau) then
          call append_chunk(i_ene_f_landau, ene_f_landau, xy_fmt, ascii_fmt)
        end if
        if(ene_drift) then
          call append_chunk(i_ene_f_drift, ene_f_drift, xy_fmt, ascii_fmt)
        end if
        
        call append_chunk(i_ene_f_twist, ene_f_twist, xy_fmt, ascii_fmt)
        call append_chunk(i_ene_e_twist, ene_e_twist, xy_fmt, ascii_fmt)
        
        call append_chunk(i_ene_f_source, ene_f_source, xy_fmt, ascii_fmt)
        call append_chunk(i_ene_f_disp, ene_f_disp, xy_fmt, ascii_fmt)
        call append_chunk(i_ene_e_disp, ene_e_disp, xy_fmt, ascii_fmt)
        if(nlapar) then
          call append_chunk(i_ene_f_source_m, ene_f_source_m, xy_fmt, ascii_fmt)
        end if
      end if ! ky_ene_evo
      
    end if
      
      
    ! ky- and x-dependent data
    if(kykx_ene_evo) then
    
      call kyx_output_array(ene_f_kykx, i_ene_f_kykx, &
           & proc_subset(0,1,1,1,1), .true.)
      call kyx_output_array(ene_e_kykx, &
        & i_ene_e_kykx, proc_subset(0,1,1,1,1), .true.)
      
      call kyx_output_array(dt_ene_f_kykx, &
        & i_dt_ene_f_kykx, proc_subset(0,1,1,1,1), .true.)
      call kyx_output_array(dt_ene_e_kykx, &
        & i_dt_ene_e_kykx, proc_subset(0,1,1,1,1), .true.)

      if(nlapar) then
        call kyx_output_array(ene_m_kykx, &
          & i_ene_m_kykx, proc_subset(0,1,1,1,1), .true.)
        call kyx_output_array(dt_ene_m_kykx, &
          & i_dt_ene_m_kykx, proc_subset(0,1,1,1,1), .true.)
        call kyx_output_array(ene_f_source_m_kykx, &
        & i_ene_f_source_m_kykx, proc_subset(0,1,1,1,1), .true.)
      end if
      
      call kyx_output_array(ene_f_twist_kykx, &
        & i_ene_f_twist_kykx, proc_subset(0,1,1,1,1), .true.)
      call kyx_output_array(ene_e_twist_kykx, &
        & i_ene_e_twist_kykx, proc_subset(0,1,1,1,1), .true.)
        
      if(ene_landau) then
        call kyx_output_array(ene_f_landau_kykx, &
        & i_ene_f_landau_kykx, proc_subset(0,1,1,1,1), .true.)
      end if
      if(ene_drift) then
        call kyx_output_array(ene_f_drift_kykx, &
        & i_ene_f_drift_kykx, proc_subset(0,1,1,1,1), .true.)
      end if
        
      call kyx_output_array(ene_f_source_kykx, &
        & i_ene_f_source_kykx, proc_subset(0,1,1,1,1), .true.)
      call kyx_output_array(ene_f_disp_kykx, &
        & i_ene_f_disp_kykx, proc_subset(0,1,1,1,1), .true.)
      call kyx_output_array(ene_e_disp_kykx, &
        & i_ene_e_disp_kykx, proc_subset(0,1,1,1,1), .true.)
        
    end if !kykx_ene_evo

  end subroutine output

end module diagnos_energy_evo
